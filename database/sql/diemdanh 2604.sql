-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2019 at 04:08 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diemdanh`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_groups`
--

CREATE TABLE `activity_groups` (
  `id` int(11) NOT NULL,
  `activitygroupcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '1',
  `path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_groups`
--

INSERT INTO `activity_groups` (`id`, `activitygroupcode`, `name`, `note`, `parent`, `depth`, `path`) VALUES
(1, 'NGOAINGU', 'Ngoại ngữ', '', 0, 1, '/1'),
(2, 'TOAN', 'Toán', NULL, 0, 1, '/2'),
(3, 'TIENGANH', 'Tiếng Anh', NULL, 1, 2, '/1/3'),
(4, 'A1', 'A1', NULL, 3, 3, '/1/3/4'),
(5, 'A2', 'A2', NULL, 3, 3, '/1/3/5'),
(6, 'TICHPHAN', 'Tích phân', 'ac', 2, 2, '/2/6'),
(7, 'CHINHTRI', 'Chính trị', NULL, 0, 1, '/7'),
(8, 'KHOA25', 'Khóa 25', NULL, 5, 4, '/1/3/5/8'),
(9, 'LUONGGIAC', 'Lượng giác', NULL, 2, 2, '/2/9'),
(10, 'VANHOC', 'Văn học', 'acvx', 0, 1, '/10');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `id` int(11) NOT NULL,
  `machinecode` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`id`, `machinecode`, `model`, `status`, `note`) VALUES
(1, 'M1', 'MODEL1', 1, ''),
(2, 'M2', 'MODL2', 1, ''),
(3, 'M3', 'MODEL3', 0, ''),
(4, 'M4', 'MODL4', 0, ''),
(6, 'M5', 'MODEL5', 0, 'GHI CHU 32'),
(7, 'M6', 'MODEL6', 0, '234');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `roomcode` varchar(100) DEFAULT NULL,
  `roomgroupid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `roomcode`, `roomgroupid`, `name`, `note`, `status`) VALUES
(1, 'A2201', 2, 'Phòng A2 201', 'Ghi chu', 1),
(2, 'A2202', 2, 'Phòng A2 202', NULL, 1),
(3, 'A3101', 1, 'Phòng A3 101', 'ghi chu 2', 1),
(4, 'A3102', 1, 'Phòng A3 102', 'ABC', 1),
(5, 'A3203', 1, 'Phòng A3 203', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `room_groups`
--

CREATE TABLE `room_groups` (
  `id` int(11) NOT NULL,
  `roomgroupcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_groups`
--

INSERT INTO `room_groups` (`id`, `roomgroupcode`, `name`, `note`) VALUES
(1, 'A3', 'Nhà A3', 'XX3'),
(2, 'A2', 'Nhà A2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_machines`
--

CREATE TABLE `room_machines` (
  `id` int(11) NOT NULL,
  `roomid` int(11) NOT NULL,
  `machineid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_machines`
--

INSERT INTO `room_machines` (`id`, `roomid`, `machineid`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 3, 3),
(6, 2, 4),
(7, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `season` int(11) NOT NULL DEFAULT '0',
  `datestartsummer` varchar(100) DEFAULT NULL,
  `datestartwinter` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `name`, `season`, `datestartsummer`, `datestartwinter`, `note`) VALUES
(4, 'SV Đại học', 1, '10/04', '16/10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shift_details`
--

CREATE TABLE `shift_details` (
  `id` int(11) NOT NULL,
  `shiftid` int(11) NOT NULL,
  `name_detail` varchar(100) DEFAULT NULL,
  `time_summer_start` varchar(11) DEFAULT NULL,
  `time_summer_end` varchar(11) DEFAULT NULL,
  `time_winter_start` varchar(11) DEFAULT NULL,
  `time_winter_end` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift_details`
--

INSERT INTO `shift_details` (`id`, `shiftid`, `name_detail`, `time_summer_start`, `time_summer_end`, `time_winter_start`, `time_winter_end`) VALUES
(4, 4, '1', '06:30', '07:20', '07:00', '07:50'),
(5, 4, '2', '07:25', '08:15', '07:55', '08:45'),
(6, 4, '3', '08:20', '09:10', '08:50', '09:40'),
(7, 4, '4', '09:15', '10:05', '09:45', '10:35'),
(8, 4, '5', '10:10', '11:00', '10:40', '11:30'),
(9, 4, '6', '13:30', '14:20', '13:00', '13:50'),
(10, 4, '7', '14:25', '15:15', '13:55', '14:45'),
(11, 4, '8', '15:20', '16:10', '14:50', '15:40'),
(12, 4, '9', '16:15', '17:05', '15:45', '16:35'),
(13, 4, '10', '17:10', '18:00', '16:40', '17:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `idnumber` varchar(100) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `token_reset_pass` varchar(100) DEFAULT NULL,
  `date_reset_pass` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `birthday` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `email_verified_at`, `firstname`, `lastname`, `idnumber`, `gender`, `avatar`, `token_reset_pass`, `date_reset_pass`, `address`, `status`, `birthday`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Q9aMGuOof7wo.SNtXFtLbOLc8hNqT2hexuQZSw2QG10JbBAEyeYL.', 'phongtrank55@gmail.com', NULL, 'System', 'Admin', 'admin', 1, 'avatar/1556189479/ZFtUyk5RPcycUSweVh2Z6riioUkMjzkhoAaLTEbP.jpeg', NULL, NULL, 'Phan Cong Tich', 1, '11/07/1996', '2019-04-25 17:16:32', '2019-04-25 10:16:32'),
(2, 'sangtv', '$2y$10$Z6TdQk2iJPOPlnaKT1nZYO.TOAn./MpR4ySz2S2TKOSFEojGqc9Li', 'xx@gmail.com', NULL, 'Sáng', 'Trần Văn', '1223', 1, 'avatar/1556189763/lrJnnLoAoBQ2XrDldPtkW8I2zwFYiC1pxxeJSLVq.jpeg', NULL, NULL, 'ads', 0, '11/06/1886', '2019-04-25 15:54:42', '2019-04-25 08:54:42'),
(5, 'abc', '$2y$10$Z86QoejNZsSFQaFI8VDi0O.aM9DZHRt48tJFnh0AQ6IBhsOhZjtK6', NULL, NULL, 'acx', 'abvc', 'abc', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 04:10:18', '2019-04-25 04:10:18'),
(6, 'xxx', '$2y$10$8mDLGr6zz/tiL2ILChKAI.Q9nZG0VpdYSTLRwVGH7yRtTMQK6Vhn6', NULL, NULL, 'xx', 'xx', 'xx', 1, 'avatar/1556192071/W817f41yUir3cq3pyRFRwVvvMHf8Ob7wdCjJqAmE.jpeg', NULL, NULL, NULL, 1, NULL, '2019-04-25 04:34:32', '2019-04-25 04:34:32'),
(7, 'fff', '$2y$10$RO/EVN/UXG7hQZOUoYyJ4uVmig/4l4BcY2L/K/6yIooaHR8r0MF9K', NULL, NULL, 'dd', 'fff', '122', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 19:27:45', '2019-04-25 12:27:45'),
(8, 'axx', '$2y$10$o4r/Mk8f6jN5nrVHip11LeHhdoGJUgzOBCEqyKWbxRG6itFKaJTBm', 'ptx@gmail.com', NULL, 'axx', 'axx', '123', 1, 'public/avatar/1556212487/3V6y63VO8PA9Kz8Ob3serEKvfTJGrDjebx0FNLjz.jpeg', NULL, NULL, 'Phan Cong Tich', 1, NULL, '2019-04-25 10:14:48', '2019-04-25 10:14:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_groups`
--
ALTER TABLE `activity_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomgroupid` (`roomgroupid`);

--
-- Indexes for table `room_groups`
--
ALTER TABLE `room_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomid` (`roomid`),
  ADD KEY `machineid` (`machineid`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shiftid` (`shiftid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_groups`
--
ALTER TABLE `activity_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_groups`
--
ALTER TABLE `room_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `room_machines`
--
ALTER TABLE `room_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shift_details`
--
ALTER TABLE `shift_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`roomgroupid`) REFERENCES `room_groups` (`id`);

--
-- Constraints for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD CONSTRAINT `room_machines_ibfk_1` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `room_machines_ibfk_2` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`);

--
-- Constraints for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD CONSTRAINT `shift_details_ibfk_1` FOREIGN KEY (`shiftid`) REFERENCES `shifts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
