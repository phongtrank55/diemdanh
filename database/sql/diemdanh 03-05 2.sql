-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2019 at 04:46 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diemdanh`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activitycode` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `activitycategoryid` int(11) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `shiftid` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `enable_in` int(11) DEFAULT NULL,
  `minute_before_in` int(11) DEFAULT NULL,
  `minute_after_in` int(11) DEFAULT NULL,
  `enable_attendance_in_between_shift` int(11) DEFAULT NULL,
  `enable_out` int(11) DEFAULT NULL,
  `minute_before_out` int(11) DEFAULT NULL,
  `minute_after_out` int(11) DEFAULT NULL,
  `enable_attendance_out_between_shift` int(11) DEFAULT NULL,
  `enable_late` int(11) DEFAULT NULL,
  `minute_late` int(11) DEFAULT NULL,
  `enable_self_enrol` int(11) DEFAULT NULL,
  `password_self_enrol` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activitycode`, `name`, `note`, `activitycategoryid`, `display`, `frequency`, `shiftid`, `start_date`, `end_date`, `enable_in`, `minute_before_in`, `minute_after_in`, `enable_attendance_in_between_shift`, `enable_out`, `minute_before_out`, `minute_after_out`, `enable_attendance_out_between_shift`, `enable_late`, `minute_late`, `enable_self_enrol`, `password_self_enrol`, `created_at`, `updated_at`) VALUES
(12, 'HD 10A', 'Lớp 10A', NULL, 1, 1, 2, 4, 1557002449, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, '2019-05-03 13:40:49', '2019-05-03 13:40:49'),
(13, 'CANBO', 'Nữ sinh thanh lịch', NULL, 1, 1, 3, NULL, 1557002579, 0, 1, 5, 5, 0, 0, NULL, NULL, NULL, 1, 15, 0, NULL, '2019-05-03 13:42:59', '2019-05-03 13:42:59'),
(14, 'CNPM', 'Công nghệ phần mềm', NULL, 13, 1, 1, 4, 1557006330, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, '2019-05-03 14:45:30', '2019-05-03 14:45:30');

-- --------------------------------------------------------

--
-- Table structure for table `activity_attendances`
--

CREATE TABLE `activity_attendances` (
  `id` int(11) NOT NULL,
  `activitycalendardetailid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `rawvalue` int(11) DEFAULT NULL,
  `finalvalue` int(11) DEFAULT NULL,
  `overridden` int(11) DEFAULT NULL,
  `useroveriden` int(11) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `activity_calendars`
--

CREATE TABLE `activity_calendars` (
  `id` int(11) NOT NULL,
  `start_date_calendar` int(11) DEFAULT NULL,
  `end_date_calendar` int(11) DEFAULT NULL,
  `time_start` varchar(6) DEFAULT NULL,
  `time_end` varchar(6) DEFAULT NULL,
  `roomid` int(11) DEFAULT NULL,
  `activityid` int(11) DEFAULT NULL,
  `shift_detail_start_id` int(11) DEFAULT NULL,
  `shift_detail_end_id` int(11) DEFAULT NULL,
  `day_of_week` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_calendars`
--

INSERT INTO `activity_calendars` (`id`, `start_date_calendar`, `end_date_calendar`, `time_start`, `time_end`, `roomid`, `activityid`, `shift_detail_start_id`, `shift_detail_end_id`, `day_of_week`) VALUES
(3, NULL, NULL, NULL, NULL, 3, 12, 4, 7, 2),
(4, NULL, NULL, NULL, NULL, 1, 12, 4, 8, 3),
(5, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 4),
(6, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 5),
(7, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 6),
(8, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 7),
(9, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 8),
(10, 1554842579, NULL, '19:30', '22:30', 1, 13, NULL, NULL, NULL),
(11, 1553550330, 1558907130, NULL, NULL, 1, 14, 6, 8, 3),
(12, 1549316730, 1552859130, NULL, NULL, 2, 14, 9, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `activity_calendar_details`
--

CREATE TABLE `activity_calendar_details` (
  `id` int(11) NOT NULL,
  `activityid` int(11) DEFAULT NULL,
  `roomid` int(11) DEFAULT NULL,
  `name` int(11) DEFAULT NULL,
  `time_start` int(11) DEFAULT NULL,
  `time_end` int(11) DEFAULT NULL,
  `date_attendance` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `activity_categories`
--

CREATE TABLE `activity_categories` (
  `id` int(11) NOT NULL,
  `activitycategorycode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '1',
  `path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_categories`
--

INSERT INTO `activity_categories` (`id`, `activitycategorycode`, `name`, `note`, `parent`, `depth`, `path`) VALUES
(1, 'NGOAINGU', 'Ngoại ngữ', '', 0, 1, '/1'),
(2, 'TOAN', 'Toán', NULL, 0, 1, '/2'),
(3, 'TIENGANH', 'Tiếng Anh', NULL, 1, 2, '/1/3'),
(4, 'A1', 'A1', NULL, 3, 3, '/1/3/4'),
(5, 'A2', 'A2', NULL, 3, 3, '/1/3/5'),
(6, 'TICHPHAN', 'Tích phân', 'ac', 2, 2, '/2/6'),
(7, 'CHINHTRI', 'Chính trị', NULL, 0, 1, '/7'),
(8, 'KHOA25', 'Khóa 25', NULL, 5, 4, '/1/3/5/8'),
(9, 'LUONGGIAC', 'Lượng giác', NULL, 2, 2, '/2/9'),
(10, 'VANHOC', 'Văn học', 'acvx', 0, 1, '/10'),
(11, 'B1', 'B1', NULL, 3, 3, '/1/3/11'),
(12, 'B2', 'B2', NULL, 3, 3, '/1/3/12'),
(13, 'IT', 'Tin học', NULL, 0, 1, '/13');

-- --------------------------------------------------------

--
-- Table structure for table `activity_enrols`
--

CREATE TABLE `activity_enrols` (
  `id` int(11) NOT NULL,
  `activityid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `timestart` int(11) DEFAULT NULL,
  `timeend` int(11) DEFAULT NULL,
  `method` int(11) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `module` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `module`, `name`, `value`) VALUES
(1, 'activity', 'enable_in_default', '1'),
(2, 'activity', 'enable_out_default', '1'),
(3, 'activity', 'minute_before_in_default', '5'),
(4, 'activity', 'minute_after_in_default', '5'),
(5, 'activity', 'minute_before_out_default', '5'),
(6, 'activity', 'minute_after_out_default', '5'),
(7, 'activity', 'enable_late_default', '1'),
(8, 'activity', 'minute_late_default', '15'),
(9, 'activity', 'enable_attendance_in_between_shift_default', '0'),
(10, 'activity', 'enable_attendance_out_between_shift_default', '0'),
(11, 'activity', 'enable_self_enrol_default', '0');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `id` int(11) NOT NULL,
  `machinecode` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `lastrecord` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`id`, `machinecode`, `model`, `lastrecord`, `status`, `note`) VALUES
(1, 'M1', 'MODEL1', NULL, 1, ''),
(2, 'M2', 'MODL2', NULL, 1, ''),
(3, 'M3', 'MODEL3', NULL, 0, ''),
(4, 'M4', 'MODL4', NULL, 0, ''),
(6, 'M5', 'MODEL5', NULL, 0, 'GHI CHU 32'),
(7, 'M6', 'MODEL6', NULL, 0, '234');

-- --------------------------------------------------------

--
-- Table structure for table `machine_records`
--

CREATE TABLE `machine_records` (
  `id` int(11) NOT NULL,
  `machineid` int(11) DEFAULT NULL,
  `usercode` varchar(100) DEFAULT NULL,
  `timeattendance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `roomcode` varchar(100) DEFAULT NULL,
  `roomgroupid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `roomcode`, `roomgroupid`, `name`, `note`, `status`) VALUES
(1, 'A2201', 2, 'Phòng A2 201', 'Ghi chu', 1),
(2, 'A2202', 2, 'Phòng A2 202', NULL, 1),
(3, 'A3101', 1, 'Phòng A3 101', 'ghi chu 2', 1),
(4, 'A3102', 1, 'Phòng A3 102', 'ABC', 1),
(5, 'A3203', 1, 'Phòng A3 203', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `room_groups`
--

CREATE TABLE `room_groups` (
  `id` int(11) NOT NULL,
  `roomgroupcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_groups`
--

INSERT INTO `room_groups` (`id`, `roomgroupcode`, `name`, `note`) VALUES
(1, 'A3', 'Nhà A3', 'XX3'),
(2, 'A2', 'Nhà A2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_machines`
--

CREATE TABLE `room_machines` (
  `id` int(11) NOT NULL,
  `roomid` int(11) NOT NULL,
  `machineid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_machines`
--

INSERT INTO `room_machines` (`id`, `roomid`, `machineid`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 3, 3),
(6, 2, 4),
(7, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `season` int(11) NOT NULL DEFAULT '0',
  `datestartsummer` varchar(100) DEFAULT NULL,
  `datestartwinter` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `name`, `season`, `datestartsummer`, `datestartwinter`, `note`) VALUES
(4, 'SV Đại học', 1, '16/04', '16/10', NULL),
(5, 'Ca co dinh', 0, NULL, NULL, 'ghi chu');

-- --------------------------------------------------------

--
-- Table structure for table `shift_details`
--

CREATE TABLE `shift_details` (
  `id` int(11) NOT NULL,
  `shiftid` int(11) NOT NULL,
  `name_detail` varchar(100) DEFAULT NULL,
  `time_summer_start` varchar(11) DEFAULT NULL,
  `time_summer_end` varchar(11) DEFAULT NULL,
  `time_winter_start` varchar(11) DEFAULT NULL,
  `time_winter_end` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift_details`
--

INSERT INTO `shift_details` (`id`, `shiftid`, `name_detail`, `time_summer_start`, `time_summer_end`, `time_winter_start`, `time_winter_end`) VALUES
(4, 4, '1', '06:30', '07:20', '07:00', '07:50'),
(5, 4, '2', '07:25', '08:15', '07:55', '08:45'),
(6, 4, '3', '08:20', '09:10', '08:50', '09:40'),
(7, 4, '4', '09:15', '10:05', '09:45', '10:35'),
(8, 4, '5', '10:10', '11:00', '10:40', '11:30'),
(9, 4, '6', '13:30', '14:20', '13:00', '13:50'),
(10, 4, '7', '14:25', '15:15', '13:55', '14:45'),
(11, 4, '8', '15:20', '16:10', '14:50', '15:40'),
(12, 4, '9', '16:15', '17:05', '15:45', '16:35'),
(13, 4, '10', '17:10', '18:00', '16:40', '17:30'),
(14, 5, 'SA', '10:20', '12:33', NULL, NULL),
(21, 5, 'CH', '13:29', '15:34', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `idnumber` varchar(100) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `token_reset_pass` varchar(100) DEFAULT NULL,
  `date_reset_pass` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `birthday` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `email_verified_at`, `remember_token`, `firstname`, `lastname`, `idnumber`, `gender`, `avatar`, `token_reset_pass`, `date_reset_pass`, `address`, `status`, `birthday`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Q9aMGuOof7wo.SNtXFtLbOLc8hNqT2hexuQZSw2QG10JbBAEyeYL.', 'phongtrank55@gmail.com', NULL, NULL, 'System', 'Admin', 'admin', 1, 'avatar/1556189479/ZFtUyk5RPcycUSweVh2Z6riioUkMjzkhoAaLTEbP.jpeg', NULL, NULL, 'Phan Cong Tich', 1, '11/07/1996', '2019-04-25 17:16:32', '2019-04-25 10:16:32'),
(2, 'sangtv', '$2y$10$Z6TdQk2iJPOPlnaKT1nZYO.TOAn./MpR4ySz2S2TKOSFEojGqc9Li', 'xx@gmail.com', NULL, NULL, 'Sáng', 'Trần Văn', '1223', 1, 'avatar/1556189763/lrJnnLoAoBQ2XrDldPtkW8I2zwFYiC1pxxeJSLVq.jpeg', NULL, NULL, 'ads', 0, '11/06/1886', '2019-04-25 15:54:42', '2019-04-25 08:54:42'),
(5, 'abc', '$2y$10$Z86QoejNZsSFQaFI8VDi0O.aM9DZHRt48tJFnh0AQ6IBhsOhZjtK6', NULL, NULL, NULL, 'acx', 'abvc', 'abc', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 04:10:18', '2019-04-25 04:10:18'),
(6, 'xxx', '$2y$10$8mDLGr6zz/tiL2ILChKAI.Q9nZG0VpdYSTLRwVGH7yRtTMQK6Vhn6', NULL, NULL, NULL, 'xx', 'xx', 'xx', 1, 'avatar/1556192071/W817f41yUir3cq3pyRFRwVvvMHf8Ob7wdCjJqAmE.jpeg', NULL, NULL, NULL, 1, NULL, '2019-04-25 04:34:32', '2019-04-25 04:34:32'),
(7, 'fff', '$2y$10$RO/EVN/UXG7hQZOUoYyJ4uVmig/4l4BcY2L/K/6yIooaHR8r0MF9K', NULL, NULL, NULL, 'dd', 'fff', '122', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 19:27:45', '2019-04-25 12:27:45'),
(8, 'axx', '$2y$10$o4r/Mk8f6jN5nrVHip11LeHhdoGJUgzOBCEqyKWbxRG6itFKaJTBm', 'ptx@gmail.com', NULL, NULL, 'axx', 'axx', '123', 1, 'public/avatar/1556212487/3V6y63VO8PA9Kz8Ob3serEKvfTJGrDjebx0FNLjz.jpeg', NULL, NULL, 'Phan Cong Tich', 1, NULL, '2019-04-25 10:14:48', '2019-04-25 10:14:48'),
(9, 'tth', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'Hung', 'Tran', 'tth', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-03 11:10:24', '2019-05-03 11:10:24'),
(10, 'ppt', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', 'ptxc@gmail.com', NULL, NULL, 'Phong', 'Van', 'ppt', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-03 18:16:10', '0000-00-00 00:00:00'),
(11, '1755234030100029', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'CHI', 'HOÀNG THỊ LINH', '1755234030100029', 1, NULL, NULL, NULL, NULL, 1, '20/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(12, '1755234030100036', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'CHUNG', 'TRẦN THỊ THÙY', '1755234030100036', 1, NULL, NULL, NULL, NULL, 1, '22/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(13, '1755234030100035', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'DUYÊN', 'LÊ THỊ PHƯƠNG', '1755234030100035', 1, NULL, NULL, NULL, NULL, 1, '12/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(14, '1755234030100021', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'DƯƠNG', 'TRƯƠNG THỊ', '1755234030100021', 1, NULL, NULL, NULL, NULL, 1, '23/10/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(15, '1755234030100030', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'GIANG', 'NGUYỄN THỊ', '1755234030100030', 1, NULL, NULL, NULL, NULL, 1, '07/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(16, '1755234030100025', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'HOÀI', 'NGUYỄN THỊ THU', '1755234030100025', 1, NULL, NULL, NULL, NULL, 1, '15/09/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(17, '1755234010100138', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'Hoàng', 'Võ Huy', '1755234010100138', 1, NULL, NULL, NULL, NULL, 1, '18/02/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(18, '1755234030100031', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'HÙNG', 'PHAN TUẤN', '1755234030100031', 1, NULL, NULL, NULL, NULL, 1, '08/11/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(19, '1755234030100519', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'MINH', 'HOÀNG MINH', '1755234030100519', 1, NULL, NULL, NULL, NULL, 1, '01/09/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(20, '1755234030100019', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NGỌC', 'TRẦN BÍCH', '1755234030100019', 1, NULL, NULL, NULL, NULL, 1, '14/06/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(21, '1755234030100023', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NGỌC', 'TRẦN THỊ HỒNG', '1755234030100023', 1, NULL, NULL, NULL, NULL, 1, '01/10/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(22, '1755234030100022', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NHUNG', 'NGUYỄN THỊ', '1755234030100022', 1, NULL, NULL, NULL, NULL, 1, '02/08/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(23, '1755234030100020', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'PHƯƠNG', 'TRẦN HÀ', '1755234030100020', 1, NULL, NULL, NULL, NULL, 1, '18/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(24, '1755234030100032', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'SEN', 'NGUYỄN THỊ', '1755234030100032', 1, NULL, NULL, NULL, NULL, 1, '24/04/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(25, '1755234030100024', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THÀNH', 'BÙI SỸ', '1755234030100024', 1, NULL, NULL, NULL, NULL, 1, '11/06/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(26, '1755234030100033', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THẢO', 'NGUYỄN PHƯƠNG', '1755234030100033', 1, NULL, NULL, NULL, NULL, 1, '03/05/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(27, '1755234030100034', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THUÝ', 'NGUYỄN THỊ', '1755234030100034', 1, NULL, NULL, NULL, NULL, 1, '13/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(28, '1755234030100026', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THÚY', 'LÊ THỊ', '1755234030100026', 1, NULL, NULL, NULL, NULL, 1, '17/07/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(29, '1755234030100027', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THƯƠNG', 'HOÀNG THỊ', '1755234030100027', 1, NULL, NULL, NULL, NULL, 1, '28/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(30, '1755234030100028', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'TIÊN', 'CAO THỊ THỦY', '1755234030100028', 1, NULL, NULL, NULL, NULL, 1, '02/11/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_machines`
--

CREATE TABLE `user_machines` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `machineid` int(11) DEFAULT NULL,
  `usercode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_machines`
--

INSERT INTO `user_machines` (`id`, `userid`, `machineid`, `usercode`) VALUES
(1, 1, 1, 'admin'),
(2, 2, 1, '1223'),
(3, 5, 1, 'abc'),
(4, 6, 1, 'xx'),
(5, 7, 1, '122'),
(6, 8, 1, '123'),
(7, 9, 1, 'tth'),
(8, 10, 1, 'ppt'),
(9, 11, 1, '1755234030100029'),
(10, 12, 1, '1755234030100036'),
(11, 13, 1, '1755234030100035'),
(12, 14, 1, '1755234030100021'),
(13, 15, 1, '1755234030100030'),
(14, 16, 1, '1755234030100025'),
(15, 17, 1, '1755234010100138'),
(16, 18, 1, '1755234030100031'),
(17, 19, 1, '1755234030100519'),
(18, 20, 1, '1755234030100019'),
(19, 21, 1, '1755234030100023'),
(20, 22, 1, '1755234030100022'),
(21, 23, 1, '1755234030100020'),
(22, 24, 1, '1755234030100032'),
(23, 25, 1, '1755234030100024'),
(24, 26, 1, '1755234030100033'),
(25, 27, 1, '1755234030100034'),
(26, 28, 1, '1755234030100026'),
(27, 29, 1, '1755234030100027'),
(28, 30, 1, '1755234030100028');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activitycategoryid` (`activitycategoryid`),
  ADD KEY `shiftid` (`shiftid`);

--
-- Indexes for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `activitycalendardetailid` (`activitycalendardetailid`);

--
-- Indexes for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activityid` (`activityid`),
  ADD KEY `shift_detail_end_id` (`shift_detail_end_id`),
  ADD KEY `shift_detail_start_id` (`shift_detail_start_id`),
  ADD KEY `roomid` (`roomid`);

--
-- Indexes for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activityid` (`activityid`),
  ADD KEY `roomid` (`roomid`);

--
-- Indexes for table `activity_categories`
--
ALTER TABLE `activity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `activityid` (`activityid`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_records`
--
ALTER TABLE `machine_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `machineid` (`machineid`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomgroupid` (`roomgroupid`);

--
-- Indexes for table `room_groups`
--
ALTER TABLE `room_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomid` (`roomid`),
  ADD KEY `machineid` (`machineid`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shiftid` (`shiftid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_machines`
--
ALTER TABLE `user_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `machineid` (`machineid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_categories`
--
ALTER TABLE `activity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `machine_records`
--
ALTER TABLE `machine_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_groups`
--
ALTER TABLE `room_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `room_machines`
--
ALTER TABLE `room_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shift_details`
--
ALTER TABLE `shift_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_machines`
--
ALTER TABLE `user_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`activitycategoryid`) REFERENCES `activity_categories` (`id`),
  ADD CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`shiftid`) REFERENCES `shifts` (`id`);

--
-- Constraints for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  ADD CONSTRAINT `activity_attendances_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `activity_attendances_ibfk_2` FOREIGN KEY (`activitycalendardetailid`) REFERENCES `activity_calendar_details` (`id`);

--
-- Constraints for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  ADD CONSTRAINT `activity_calendars_ibfk_1` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`),
  ADD CONSTRAINT `activity_calendars_ibfk_2` FOREIGN KEY (`shift_detail_end_id`) REFERENCES `shift_details` (`id`),
  ADD CONSTRAINT `activity_calendars_ibfk_3` FOREIGN KEY (`shift_detail_start_id`) REFERENCES `shift_details` (`id`),
  ADD CONSTRAINT `activity_calendars_ibfk_4` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  ADD CONSTRAINT `activity_calendar_details_ibfk_1` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`),
  ADD CONSTRAINT `activity_calendar_details_ibfk_2` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  ADD CONSTRAINT `activity_enrols_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `activity_enrols_ibfk_2` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`);

--
-- Constraints for table `machine_records`
--
ALTER TABLE `machine_records`
  ADD CONSTRAINT `machine_records_ibfk_1` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`roomgroupid`) REFERENCES `room_groups` (`id`);

--
-- Constraints for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD CONSTRAINT `room_machines_ibfk_1` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`),
  ADD CONSTRAINT `room_machines_ibfk_2` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`);

--
-- Constraints for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD CONSTRAINT `shift_details_ibfk_1` FOREIGN KEY (`shiftid`) REFERENCES `shifts` (`id`);

--
-- Constraints for table `user_machines`
--
ALTER TABLE `user_machines`
  ADD CONSTRAINT `user_machines_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_machines_ibfk_2` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
