-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2019 at 01:45 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diemdanh`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `clear_data_attendance` ()  begin
delete from activity_attendances;
DELETE from machine_records;
UPDATE machines set lastrecord = null;
update activity_calendar_details set completed = 0;

end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activitycode` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `activitycategoryid` int(11) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `shiftid` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `enable_in` int(11) DEFAULT NULL,
  `minute_before_in` int(11) DEFAULT NULL,
  `minute_after_in` int(11) DEFAULT NULL,
  `enable_attendance_in_between_shift` int(11) DEFAULT NULL,
  `enable_out` int(11) DEFAULT NULL,
  `minute_before_out` int(11) DEFAULT NULL,
  `minute_after_out` int(11) DEFAULT NULL,
  `enable_attendance_out_between_shift` int(11) DEFAULT NULL,
  `enable_late` int(11) DEFAULT NULL,
  `minute_late` int(11) DEFAULT NULL,
  `enable_self_enrol` int(11) DEFAULT NULL,
  `password_self_enrol` varchar(200) DEFAULT NULL,
  `expired_self_enrol` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activitycode`, `name`, `note`, `activitycategoryid`, `display`, `frequency`, `shiftid`, `start_date`, `end_date`, `enable_in`, `minute_before_in`, `minute_after_in`, `enable_attendance_in_between_shift`, `enable_out`, `minute_before_out`, `minute_after_out`, `enable_attendance_out_between_shift`, `enable_late`, `minute_late`, `enable_self_enrol`, `password_self_enrol`, `expired_self_enrol`, `created_at`, `updated_at`) VALUES
(12, 'HD 10A', 'Lớp 10A', NULL, 1, 1, 2, 4, 1557002449, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, NULL, '2019-05-03 13:40:49', '2019-05-03 13:40:49'),
(13, 'CANBO', 'Nữ sinh thanh lịch', NULL, 1, 1, 3, NULL, 1557002579, 0, 1, 5, 5, 0, 0, NULL, NULL, NULL, 1, 15, 0, NULL, NULL, '2019-05-03 13:42:59', '2019-05-03 13:42:59'),
(14, 'CNPM', 'Công nghệ phần mềm', NULL, 13, 1, 1, 4, 1557006330, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, NULL, '2019-05-07 09:08:12', '2019-05-03 14:45:30'),
(20, 'LTM', 'Lập trình mạng', NULL, 13, 1, 1, 4, 1557187200, 0, 1, 5, 5, 1, 1, 5, 5, 1, 1, 15, 1, '123456', 1557187200, '2019-05-07 02:39:38', '2019-05-07 02:39:38'),
(22, '1TIET', '1tiet', NULL, 13, 1, 1, 4, 1557187200, 0, 1, 5, 5, 1, 1, 5, 5, 1, 1, 15, 0, NULL, 0, '2019-05-07 02:49:35', '2019-05-07 02:49:35'),
(23, 'NO_MACHINE', 'NO_MACHINE', NULL, 13, 1, 1, 4, 1557334800, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, 0, '2019-05-09 16:32:32', '2019-05-09 16:32:32'),
(25, 'NO_ENABLE', 'NO_ENABLE', NULL, 13, 1, 1, 4, 1557421200, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, '2019-05-09 17:19:26', '2019-05-09 17:19:26'),
(31, 'NO CALENDAR', 'NO CALENDAR', NULL, 13, 1, 1, 4, 1557507600, 0, 1, 5, 5, 0, 1, 5, 5, 0, 1, 15, 0, NULL, 0, '2019-05-11 04:31:22', '2019-05-11 04:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `activity_attendances`
--

CREATE TABLE `activity_attendances` (
  `id` int(11) NOT NULL,
  `activitycalendardetailid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `timein` varchar(10) DEFAULT NULL,
  `timeout` varchar(10) DEFAULT NULL,
  `rawvalue` int(11) DEFAULT NULL,
  `percent_present_shift` varchar(10) DEFAULT NULL,
  `finalvalue` int(11) DEFAULT NULL,
  `overridden` int(11) NOT NULL DEFAULT '0',
  `useroverridden` int(11) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_attendances`
--

INSERT INTO `activity_attendances` (`id`, `activitycalendardetailid`, `userid`, `timein`, `timeout`, `rawvalue`, `percent_present_shift`, `finalvalue`, `overridden`, `useroverridden`, `note`) VALUES
(673, 248, 11, '12:58:00', '15:39:02', 1, '100', NULL, 0, NULL, NULL),
(674, 248, 12, '', '15:38:31', 3, '0', NULL, 0, NULL, NULL),
(675, 248, 14, '', '15:39:03', 3, '0', NULL, 0, NULL, NULL),
(676, 248, 15, '13:04:02', '15:39:04', 1, '100', NULL, 0, NULL, NULL),
(677, 248, 16, '13:10:00', '15:38:33', 2, '100', NULL, 0, NULL, NULL),
(678, 248, 17, '', '15:39:05', 3, '0', NULL, 0, NULL, NULL),
(679, 248, 19, '12:58:01', '15:38:34', 1, '100', NULL, 0, NULL, NULL),
(680, 248, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(681, 248, 21, '', '15:41:02', 3, '0', NULL, 0, NULL, NULL),
(682, 248, 22, '13:02:30', '15:41:03', 1, '100', NULL, 0, NULL, NULL),
(683, 248, 23, '13:04:03', '15:41:04', 1, '100', NULL, 0, NULL, NULL),
(684, 248, 24, '13:10:01', '15:41:05', 2, '100', NULL, 0, NULL, NULL),
(685, 248, 25, '', '15:41:06', 3, '0', NULL, 0, NULL, NULL),
(686, 248, 26, '12:58:02', '15:41:07', 1, '100', NULL, 0, NULL, NULL),
(687, 248, 27, '13:02:31', '15:41:10', 1, '100', NULL, 0, NULL, NULL),
(688, 248, 28, '', '15:41:09', 3, '0', NULL, 0, NULL, NULL),
(689, 248, 29, '', '15:41:08', 3, '0', NULL, 0, NULL, NULL),
(690, 248, 30, '13:14:09', '15:41:11', 2, '100', NULL, 0, NULL, NULL),
(691, 249, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(692, 249, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(693, 249, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(694, 249, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(695, 249, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(696, 249, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(697, 249, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(698, 249, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(699, 249, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(700, 249, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(701, 249, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(702, 249, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(703, 249, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(704, 249, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(705, 249, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(706, 249, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(707, 249, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(708, 249, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(709, 250, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(710, 250, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(711, 250, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(712, 250, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(713, 250, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(714, 250, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(715, 250, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(716, 250, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(717, 250, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(718, 250, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(719, 250, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(720, 250, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(721, 250, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(722, 250, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(723, 250, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(724, 250, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(725, 250, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(726, 250, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(727, 251, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(728, 251, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(729, 251, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(730, 251, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(731, 251, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(732, 251, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(733, 251, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(734, 251, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(735, 251, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(736, 251, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(737, 251, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(738, 251, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(739, 251, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(740, 251, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(741, 251, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(742, 251, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(743, 251, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(744, 251, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(745, 252, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(746, 252, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(747, 252, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(748, 252, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(749, 252, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(750, 252, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(751, 252, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(752, 252, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(753, 252, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(754, 252, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(755, 252, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(756, 252, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(757, 252, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(758, 252, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(759, 252, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(760, 252, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(761, 252, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(762, 252, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(763, 253, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(764, 253, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(765, 253, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(766, 253, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(767, 253, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(768, 253, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(769, 253, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(770, 253, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(771, 253, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(772, 253, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(773, 253, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(774, 253, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(775, 253, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(776, 253, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(777, 253, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(778, 253, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(779, 253, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(780, 253, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(781, 254, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(782, 254, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(783, 254, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(784, 254, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(785, 254, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(786, 254, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(787, 254, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(788, 254, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(789, 254, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(790, 254, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(791, 254, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(792, 254, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(793, 254, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(794, 254, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(795, 254, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(796, 254, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(797, 254, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(798, 254, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(799, 254, 18, '', '', 3, '0', NULL, 0, NULL, NULL),
(800, 255, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(801, 255, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(802, 255, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(803, 255, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(804, 255, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(805, 255, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(806, 255, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(807, 255, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(808, 255, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(809, 255, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(810, 255, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(811, 255, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(812, 255, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(813, 255, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(814, 255, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(815, 255, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(816, 255, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(817, 255, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(818, 255, 18, '', '', 3, '0', NULL, 0, NULL, NULL),
(819, 256, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(820, 256, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(821, 256, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(822, 256, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(823, 256, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(824, 256, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(825, 256, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(826, 256, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(827, 256, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(828, 256, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(829, 256, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(830, 256, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(831, 256, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(832, 256, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(833, 256, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(834, 256, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(835, 256, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(836, 256, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(837, 256, 18, '', '', 3, '0', NULL, 0, NULL, NULL),
(838, 257, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(839, 257, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(840, 257, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(841, 257, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(842, 257, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(843, 257, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(844, 257, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(845, 257, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(846, 257, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(847, 257, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(848, 257, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(849, 257, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(850, 257, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(851, 257, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(852, 257, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(853, 257, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(854, 257, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(855, 257, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(856, 257, 18, '', '', 3, '0', NULL, 0, NULL, NULL),
(857, 258, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(858, 258, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(859, 258, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(860, 258, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(861, 258, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(862, 258, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(863, 258, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(864, 258, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(865, 258, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(866, 258, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(867, 258, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(868, 258, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(869, 258, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(870, 258, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(871, 258, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(872, 258, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(873, 258, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(874, 258, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(875, 258, 18, '', '', 3, '0', NULL, 0, NULL, NULL),
(876, 263, 11, '', '', 3, '0', NULL, 0, NULL, NULL),
(877, 263, 12, '', '', 3, '0', NULL, 0, NULL, NULL),
(878, 263, 14, '', '', 3, '0', NULL, 0, NULL, NULL),
(879, 263, 15, '', '', 3, '0', NULL, 0, NULL, NULL),
(880, 263, 16, '', '', 3, '0', NULL, 0, NULL, NULL),
(881, 263, 17, '', '', 3, '0', NULL, 0, NULL, NULL),
(882, 263, 19, '', '', 3, '0', NULL, 0, NULL, NULL),
(883, 263, 20, '', '', 3, '0', NULL, 0, NULL, NULL),
(884, 263, 21, '', '', 3, '0', NULL, 0, NULL, NULL),
(885, 263, 22, '', '', 3, '0', NULL, 0, NULL, NULL),
(886, 263, 23, '', '', 3, '0', NULL, 0, NULL, NULL),
(887, 263, 24, '', '', 3, '0', NULL, 0, NULL, NULL),
(888, 263, 25, '', '', 3, '0', NULL, 0, NULL, NULL),
(889, 263, 26, '', '', 3, '0', NULL, 0, NULL, NULL),
(890, 263, 27, '', '', 3, '0', NULL, 0, NULL, NULL),
(891, 263, 28, '', '', 3, '0', NULL, 0, NULL, NULL),
(892, 263, 29, '', '', 3, '0', NULL, 0, NULL, NULL),
(893, 263, 30, '', '', 3, '0', NULL, 0, NULL, NULL),
(894, 263, 18, '', '', 3, '0', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `activity_calendars`
--

CREATE TABLE `activity_calendars` (
  `id` int(11) NOT NULL,
  `start_date_calendar` int(11) DEFAULT NULL,
  `end_date_calendar` int(11) DEFAULT NULL,
  `time_start` varchar(6) DEFAULT NULL,
  `time_end` varchar(6) DEFAULT NULL,
  `roomid` int(11) DEFAULT NULL,
  `activityid` int(11) DEFAULT NULL,
  `shift_detail_start_id` int(11) DEFAULT NULL,
  `shift_detail_end_id` int(11) DEFAULT NULL,
  `day_of_week` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_calendars`
--

INSERT INTO `activity_calendars` (`id`, `start_date_calendar`, `end_date_calendar`, `time_start`, `time_end`, `roomid`, `activityid`, `shift_detail_start_id`, `shift_detail_end_id`, `day_of_week`) VALUES
(3, NULL, NULL, NULL, NULL, 3, 12, 4, 7, 2),
(4, NULL, NULL, NULL, NULL, 1, 12, 4, 8, 3),
(5, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 4),
(6, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 5),
(7, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 6),
(8, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 7),
(9, NULL, NULL, NULL, NULL, 1, 12, 4, 4, 8),
(10, 1554842579, NULL, '19:30', '22:30', 1, 13, NULL, NULL, NULL),
(11, 1553550330, 1558907130, NULL, NULL, 1, 14, 6, 8, 3),
(12, 1549316730, 1552859130, NULL, NULL, 2, 14, 9, 11, 2),
(18, 1550966400, 1558742400, NULL, NULL, 1, 20, 4, 6, 2),
(20, 1557705600, 1558224000, NULL, NULL, 1, 22, 4, 4, 2),
(21, 1551632400, 1558198800, NULL, NULL, 5, 23, 10, 13, 2),
(23, 1556470800, 1556989200, NULL, NULL, 1, 25, 6, 8, 6);

-- --------------------------------------------------------

--
-- Table structure for table `activity_calendar_details`
--

CREATE TABLE `activity_calendar_details` (
  `id` int(11) NOT NULL,
  `activityid` int(11) DEFAULT NULL,
  `holidayid` int(11) DEFAULT NULL,
  `roomid` int(11) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `shortname` varchar(100) DEFAULT NULL,
  `time_start` varchar(11) DEFAULT NULL,
  `time_end` varchar(11) DEFAULT NULL,
  `date_attendance` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `modified` int(11) NOT NULL DEFAULT '0',
  `completed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_calendar_details`
--

INSERT INTO `activity_calendar_details` (`id`, `activityid`, `holidayid`, `roomid`, `fullname`, `shortname`, `time_start`, `time_end`, `date_attendance`, `status`, `modified`, `completed`) VALUES
(248, 14, NULL, 2, 'Buổi 1', 'B1', '13:00', '15:40', 1549238400, 1, 0, 0),
(249, 14, NULL, 2, 'Buổi 2', 'B2', '13:00', '15:40', 1549843200, 1, 0, 0),
(250, 14, NULL, 2, 'Buổi 3', 'B3', '13:00', '15:40', 1550448000, 1, 0, 0),
(251, 14, NULL, 2, 'Buổi 4', 'B4', '13:00', '15:40', 1551052800, 1, 0, 0),
(252, 14, NULL, 2, 'Buổi 5', 'B5', '13:00', '15:40', 1551657600, 1, 0, 0),
(253, 14, NULL, 2, 'Buổi 6', 'B6', '13:00', '15:40', 1552262400, 1, 0, 0),
(254, 14, NULL, 1, 'Buổi 7', 'B7', '08:50', '11:30', 1553558400, 1, 0, 0),
(255, 14, NULL, 1, 'Buổi 8', 'B8', '08:50', '11:30', 1554163200, 1, 0, 0),
(256, 14, NULL, 1, 'Buổi 9', 'B9', '08:50', '11:30', 1554768000, 1, 0, 0),
(257, 14, NULL, 1, 'Buổi 10', 'B10', '08:20', '11:00', 1555372800, 1, 0, 0),
(258, 14, NULL, 1, 'Buổi 11', 'B11', '08:20', '11:00', 1555977600, 1, 0, 0),
(259, 14, 5, 1, 'Buổi 12', 'B12', '08:20', '11:00', 1556582400, 0, 0, 0),
(260, 14, 3, 1, 'Buổi 13', 'B13', '08:20', '11:00', 1557187200, 2, 0, 0),
(261, 14, 3, 1, 'Buổi 14', 'B14', '08:20', '11:00', 1557792000, 1, 0, 0),
(262, 14, 3, 1, 'Buổi 15', 'B15', '08:20', '11:00', 1558396800, 2, 0, 0),
(263, 14, NULL, 1, 'Buổi 19', 'b19', '13:00', '17:00', 1557334800, 1, 1, 0),
(264, 23, NULL, 5, 'Buổi 1', 'B1', '13:55', '17:30', 1551718800, 1, 0, 0),
(265, 23, NULL, 5, 'Buổi 2', 'B2', '13:55', '17:30', 1552323600, 1, 0, 0),
(266, 23, NULL, 5, 'Buổi 3', 'B3', '13:55', '17:30', 1552928400, 1, 0, 0),
(267, 23, NULL, 5, 'Buổi 4', 'B4', '13:55', '17:30', 1553533200, 1, 0, 0),
(268, 23, NULL, 5, 'Buổi 5', 'B5', '13:55', '17:30', 1554138000, 1, 0, 0),
(269, 23, NULL, 5, 'Buổi 6', 'B6', '13:55', '17:30', 1554742800, 1, 0, 0),
(270, 23, NULL, 5, 'Buổi 7', 'B7', '14:25', '18:00', 1555347600, 1, 0, 0),
(271, 23, NULL, 5, 'Buổi 8', 'B8', '14:25', '18:00', 1555952400, 1, 0, 0),
(272, 23, 5, 5, 'Buổi 9', 'B9', '14:25', '18:00', 1556557200, 2, 0, 0),
(273, 23, 3, 5, 'Buổi 10', 'B10', '14:25', '18:00', 1557162000, 2, 0, 0),
(274, 23, 3, 5, 'Buổi 11', 'B11', '14:25', '18:00', 1557766800, 2, 0, 0),
(275, 25, NULL, 1, 'Buổi 1', 'B1', '08:20', '11:00', 1556902800, 1, 0, 0),
(276, 20, NULL, 1, 'Buổi 1', 'B1', '07:00', '09:40', 1551052800, 1, 0, 1),
(277, 20, NULL, 1, 'Buổi 2', 'B2', '07:00', '09:40', 1551657600, 1, 0, 1),
(278, 20, NULL, 1, 'Buổi 3', 'B3', '07:00', '09:40', 1552262400, 1, 0, 1),
(279, 20, NULL, 1, 'Buổi 4', 'B4', '07:00', '09:40', 1552867200, 1, 0, 1),
(280, 20, NULL, 1, 'Buổi 5', 'B5', '07:00', '09:40', 1553472000, 1, 0, 1),
(281, 20, NULL, 1, 'Buổi 6', 'B6', '07:00', '09:40', 1554076800, 1, 0, 1),
(282, 20, NULL, 1, 'Buổi 7', 'B7', '07:00', '09:40', 1554681600, 1, 0, 1),
(283, 20, NULL, 1, 'Buổi 8', 'B8', '07:00', '09:40', 1555286400, 1, 0, 1),
(284, 20, NULL, 1, 'Buổi 9', 'B9', '06:30', '09:10', 1555891200, 1, 0, 1),
(285, 20, NULL, 1, 'Buổi 10', 'B10', '06:30', '09:10', 1556496000, 1, 0, 1),
(286, 20, 3, 1, 'Buổi 11', 'B11', '06:30', '09:10', 1557100800, 2, 0, 0),
(287, 20, 3, 1, 'Buổi 12', 'B12', '06:30', '09:10', 1557705600, 2, 0, 0),
(288, 20, 3, 1, 'Buổi 13', 'B13', '06:30', '09:10', 1558310400, 2, 0, 0),
(289, 31, NULL, 3, 'Buổi 1', 'b1', '11:30', '11:30', 1558285200, 1, 1, 0),
(290, 22, 3, 1, 'Buổi 1', 'B1', '06:30', '07:20', 1557705600, 2, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `activity_calendar_detail_shifts`
--

CREATE TABLE `activity_calendar_detail_shifts` (
  `id` int(11) NOT NULL,
  `activitycalendardetailid` int(11) DEFAULT NULL,
  `timestart` varchar(10) DEFAULT NULL,
  `timeend` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_calendar_detail_shifts`
--

INSERT INTO `activity_calendar_detail_shifts` (`id`, `activitycalendardetailid`, `timestart`, `timeend`) VALUES
(1, 276, '07:00', '07:50'),
(2, 276, '07:55', '08:45'),
(3, 276, '08:50', '09:40'),
(4, 277, '07:00', '07:50'),
(5, 277, '07:55', '08:45'),
(6, 277, '08:50', '09:40'),
(7, 278, '07:00', '07:50'),
(8, 278, '07:55', '08:45'),
(9, 278, '08:50', '09:40'),
(10, 279, '07:00', '07:50'),
(11, 279, '07:55', '08:45'),
(12, 279, '08:50', '09:40'),
(13, 280, '07:00', '07:50'),
(14, 280, '07:55', '08:45'),
(15, 280, '08:50', '09:40'),
(16, 281, '07:00', '07:50'),
(17, 281, '07:55', '08:45'),
(18, 281, '08:50', '09:40'),
(19, 282, '07:00', '07:50'),
(20, 282, '07:55', '08:45'),
(21, 282, '08:50', '09:40'),
(22, 283, '07:00', '07:50'),
(23, 283, '07:55', '08:45'),
(24, 283, '08:50', '09:40'),
(25, 284, '06:30', '07:20'),
(26, 284, '07:25', '08:15'),
(27, 284, '08:20', '09:10'),
(28, 285, '06:30', '07:20'),
(29, 285, '07:25', '08:15'),
(30, 285, '08:20', '09:10'),
(31, 286, '06:30', '07:20'),
(32, 286, '07:25', '08:15'),
(33, 286, '08:20', '09:10'),
(34, 287, '06:30', '07:20'),
(35, 287, '07:25', '08:15'),
(36, 287, '08:20', '09:10'),
(37, 288, '06:30', '07:20'),
(38, 288, '07:25', '08:15'),
(39, 288, '08:20', '09:10'),
(40, 290, '06:30', '07:20');

-- --------------------------------------------------------

--
-- Table structure for table `activity_categories`
--

CREATE TABLE `activity_categories` (
  `id` int(11) NOT NULL,
  `activitycategorycode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `depth` int(11) NOT NULL DEFAULT '1',
  `path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_categories`
--

INSERT INTO `activity_categories` (`id`, `activitycategorycode`, `name`, `note`, `parent`, `depth`, `path`) VALUES
(1, 'NGOAINGU', 'Ngoại ngữ', '', 0, 1, '/1'),
(2, 'TOAN', 'Toán', NULL, 0, 1, '/2'),
(3, 'TIENGANH', 'Tiếng Anh', NULL, 1, 2, '/1/3'),
(4, 'A1', 'A1', NULL, 3, 3, '/1/3/4'),
(5, 'A2', 'A2', NULL, 3, 3, '/1/3/5'),
(6, 'TICHPHAN', 'Tích phân', 'ac', 2, 2, '/2/6'),
(7, 'CHINHTRI', 'Chính trị', NULL, 0, 1, '/7'),
(8, 'KHOA25', 'Khóa 25', NULL, 5, 4, '/1/3/5/8'),
(9, 'LUONGGIAC', 'Lượng giác', NULL, 2, 2, '/2/9'),
(10, 'VANHOC', 'Văn học', 'acvx', 0, 1, '/10'),
(11, 'B1', 'B1', NULL, 3, 3, '/1/3/11'),
(12, 'B2', 'B2', NULL, 3, 3, '/1/3/12'),
(13, 'IT', 'Tin học', NULL, 0, 1, '/13');

-- --------------------------------------------------------

--
-- Table structure for table `activity_enrols`
--

CREATE TABLE `activity_enrols` (
  `id` int(11) NOT NULL,
  `activityid` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `timestart` int(11) DEFAULT NULL,
  `timeend` int(11) DEFAULT NULL,
  `method` int(11) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `activity_enrols`
--

INSERT INTO `activity_enrols` (`id`, `activityid`, `userid`, `timestart`, `timeend`, `method`, `note`) VALUES
(12, 14, 11, 1549192040, NULL, 1, NULL),
(13, 14, 12, 1549192040, NULL, 1, NULL),
(14, 14, 13, 1549192040, NULL, 1, NULL),
(15, 14, 14, 1549192040, NULL, 1, NULL),
(16, 14, 15, 1549192040, NULL, 1, NULL),
(17, 14, 16, 1549192040, NULL, 1, NULL),
(18, 14, 17, 1549192040, NULL, 1, NULL),
(20, 14, 19, 1549192040, NULL, 1, NULL),
(21, 14, 20, 1549192040, NULL, 1, NULL),
(22, 14, 21, 1549192040, NULL, 1, NULL),
(23, 14, 22, 1549192040, NULL, 1, NULL),
(24, 14, 23, 1549192040, NULL, 1, NULL),
(25, 14, 24, 1549192040, NULL, 1, NULL),
(26, 14, 25, 1549192040, NULL, 1, NULL),
(27, 14, 26, 1549192040, NULL, 1, NULL),
(28, 14, 27, 1549192040, NULL, 1, NULL),
(29, 14, 28, 1549192040, NULL, 1, NULL),
(30, 14, 29, 1549192040, NULL, 1, NULL),
(31, 14, 30, 1549192040, NULL, 1, NULL),
(32, 14, 18, 1553446800, NULL, 1, NULL),
(33, 23, 12, 1551546000, NULL, 1, NULL),
(34, 25, 5, 1557421200, NULL, 1, NULL),
(35, 25, 8, 1550336400, NULL, 1, NULL),
(36, 23, 13, 1548522000, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `capabilities`
--

CREATE TABLE `capabilities` (
  `capabilitycode` varchar(60) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `contextlevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `capabilities`
--

INSERT INTO `capabilities` (`capabilitycode`, `name`, `contextlevel`) VALUES
('activity:edit', 'Chỉnh sửa các hoạt động', 10),
('activity:enrol_manual', 'Đăng ký thủ công hoạt động', 100),
('activity:overridden', 'Ghi đè kết quả điểm danh', 100),
('activity:update_calendar_detail', 'Cập nhật lịch cụ thể', 100),
('activity:view_report', 'Xem báo cáo thống kê', 100),
('activity:view_result', 'Xem kết quả điểm danh', 100);

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `module` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `module`, `name`, `value`) VALUES
(1, 'activity', 'enable_in_default', '1'),
(2, 'activity', 'enable_out_default', '1'),
(3, 'activity', 'minute_before_in_default', '5'),
(4, 'activity', 'minute_after_in_default', '5'),
(5, 'activity', 'minute_before_out_default', '5'),
(6, 'activity', 'minute_after_out_default', '5'),
(7, 'activity', 'enable_late_default', '1'),
(8, 'activity', 'minute_late_default', '15'),
(9, 'activity', 'enable_attendance_in_between_shift_default', '0'),
(10, 'activity', 'enable_attendance_out_between_shift_default', '0'),
(11, 'activity', 'enable_self_enrol_default', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contexts`
--

CREATE TABLE `contexts` (
  `id` int(11) NOT NULL,
  `contextlevel` int(11) DEFAULT NULL,
  `instanceid` int(11) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `shortname` varchar(100) DEFAULT NULL,
  `startdate` int(11) DEFAULT NULL,
  `enddate` int(11) DEFAULT NULL,
  `dayeveryyear` int(11) DEFAULT NULL,
  `montheveryyear` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `fullname`, `shortname`, `startdate`, `enddate`, `dayeveryyear`, `montheveryyear`, `type`, `note`) VALUES
(3, 'test ngay cu thể', 'spec', 1557100800, 1559174400, NULL, NULL, 0, NULL),
(5, 'Giải phóng miền nam', 'GPMN', 0, 0, 30, 4, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `id` int(11) NOT NULL,
  `machinecode` varchar(100) DEFAULT NULL,
  `filename` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `lastrecord` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`id`, `machinecode`, `filename`, `model`, `lastrecord`, `status`, `note`) VALUES
(1, 'M1', 'M1.txt', 'MODEL1', 1549269665, 1, NULL),
(2, 'M2', 'M2.txt', 'MODL2', 1549269960, 1, NULL),
(3, 'M3', 'M3.txt', 'MODEL3', NULL, 0, NULL),
(4, 'M4', 'M4.txt', 'MODL4', NULL, 0, ''),
(6, 'M5', 'M5.txt', 'MODEL5', NULL, 0, 'GHI CHU 32'),
(7, 'M6', 'M6.txt', 'MODEL6', NULL, 0, '234');

-- --------------------------------------------------------

--
-- Table structure for table `machine_records`
--

CREATE TABLE `machine_records` (
  `id` int(11) NOT NULL,
  `machineid` int(11) DEFAULT NULL,
  `usercode` varchar(100) DEFAULT NULL,
  `timeattendance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `machine_records`
--

INSERT INTO `machine_records` (`id`, `machineid`, `usercode`, `timeattendance`) VALUES
(129, 1, '1755234030100024', 1549260961),
(130, 1, '1755234030100036', 1549269511),
(131, 1, '1755234030100035', 1549269512),
(132, 1, '1755234030100025', 1549269513),
(133, 1, '1755234030100519', 1549269514),
(134, 1, '1755234030100029', 1549269542),
(135, 1, '1755234030100021', 1549269543),
(136, 1, '1755234030100030', 1549269544),
(137, 1, '1755234010100138', 1549269545),
(138, 1, '1755234030100023', 1549269662),
(139, 1, '1755234030100022', 1549269663),
(140, 1, '1755234030100020', 1549269664),
(141, 1, '1755234030100032', 1549269665),
(142, 2, '1755234030100021', 1549259400),
(143, 2, '1755234030100023', 1549259401),
(144, 2, '1755234030100026', 1549259402),
(145, 2, '1755234030100036', 1549259640),
(146, 2, '1755234030100019', 1549259641),
(147, 2, '1755234030100027', 1549259642),
(148, 2, '1755234030100029', 1549259880),
(149, 2, '1755234030100519', 1549259881),
(150, 2, '1755234030100033', 1549259882),
(151, 2, '1755234030100035', 1549260149),
(152, 2, '1755234030100022', 1549260150),
(153, 2, '1755234030100034', 1549260151),
(154, 2, '1755234030100030', 1549260242),
(155, 2, '1755234030100020', 1549260243),
(156, 2, '1755234030100025', 1549260600),
(157, 2, '1755234030100032', 1549260601),
(158, 2, '1755234030100028', 1549260849),
(159, 2, '1755234010100138', 1549260960),
(160, 2, '1755234030100024', 1549269666),
(161, 2, '1755234030100033', 1549269667),
(162, 2, '1755234030100027', 1549269668),
(163, 2, '1755234030100026', 1549269669),
(164, 2, '1755234030100034', 1549269670),
(165, 2, '1755234030100028', 1549269671),
(166, 2, '1755234030100019', 1549269960);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rolecode` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `rolecode`, `name`, `description`) VALUES
(4, 'member', 'Thành viên tham dự', NULL),
(5, 'staff', 'Cán bộ phụ trách', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_allow_assigns`
--

CREATE TABLE `role_allow_assigns` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `roleidassign` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_allow_assigns`
--

INSERT INTO `role_allow_assigns` (`id`, `roleid`, `roleidassign`) VALUES
(2, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `role_capabilities`
--

CREATE TABLE `role_capabilities` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `capabilitycode` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_capabilities`
--

INSERT INTO `role_capabilities` (`id`, `roleid`, `capabilitycode`) VALUES
(9, 4, 'activity:view_report'),
(10, 4, 'activity:view_result'),
(11, 5, 'activity:enrol_manual'),
(12, 5, 'activity:overridden'),
(13, 5, 'activity:update_calendar_detail'),
(14, 5, 'activity:view_report'),
(15, 5, 'activity:view_result');

-- --------------------------------------------------------

--
-- Table structure for table `role_context_levels`
--

CREATE TABLE `role_context_levels` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `contextlevel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_context_levels`
--

INSERT INTO `role_context_levels` (`id`, `roleid`, `contextlevel`) VALUES
(3, 4, 100),
(4, 5, 100);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `roomcode` varchar(100) DEFAULT NULL,
  `roomgroupid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `roomcode`, `roomgroupid`, `name`, `note`, `status`) VALUES
(1, 'A2201', 2, 'Phòng A2 201', 'Ghi chu', 1),
(2, 'A2202', 2, 'Phòng A2 202', NULL, 1),
(3, 'A3101', 1, 'Phòng A3 101', 'ghi chu 2', 1),
(4, 'A3102', 1, 'Phòng A3 102', 'ABC', 1),
(5, 'A3203', 1, 'Phòng A3 203', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `room_groups`
--

CREATE TABLE `room_groups` (
  `id` int(11) NOT NULL,
  `roomgroupcode` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `note` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_groups`
--

INSERT INTO `room_groups` (`id`, `roomgroupcode`, `name`, `note`) VALUES
(1, 'A3', 'Nhà A3', 'XX3'),
(2, 'A2', 'Nhà A2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_machines`
--

CREATE TABLE `room_machines` (
  `id` int(11) NOT NULL,
  `roomid` int(11) NOT NULL,
  `machineid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room_machines`
--

INSERT INTO `room_machines` (`id`, `roomid`, `machineid`) VALUES
(1, 1, 1),
(4, 3, 3),
(8, 2, 2),
(10, 1, 3),
(11, 4, 4),
(12, 2, 1),
(13, 2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `season` int(11) NOT NULL DEFAULT '0',
  `datestartsummer` varchar(100) DEFAULT NULL,
  `datestartwinter` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `name`, `season`, `datestartsummer`, `datestartwinter`, `note`) VALUES
(4, 'SV Đại học', 1, '16/04', '16/10', NULL),
(5, 'Ca co dinh', 0, NULL, NULL, 'ghi chu');

-- --------------------------------------------------------

--
-- Table structure for table `shift_details`
--

CREATE TABLE `shift_details` (
  `id` int(11) NOT NULL,
  `shiftid` int(11) NOT NULL,
  `name_detail` varchar(100) DEFAULT NULL,
  `time_summer_start` varchar(11) DEFAULT NULL,
  `time_summer_end` varchar(11) DEFAULT NULL,
  `time_winter_start` varchar(11) DEFAULT NULL,
  `time_winter_end` varchar(11) DEFAULT NULL,
  `sortorder` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift_details`
--

INSERT INTO `shift_details` (`id`, `shiftid`, `name_detail`, `time_summer_start`, `time_summer_end`, `time_winter_start`, `time_winter_end`, `sortorder`) VALUES
(4, 4, 'Tiết 1', '06:30', '07:20', '07:00', '07:50', 1),
(5, 4, 'Tiết 2', '07:25', '08:15', '07:55', '08:45', 2),
(6, 4, 'Tiết 3', '08:20', '09:10', '08:50', '09:40', 3),
(7, 4, 'Tiết 4', '09:15', '10:05', '09:45', '10:35', 4),
(8, 4, 'Tiết 5', '10:10', '11:00', '10:40', '11:30', 5),
(9, 4, 'Tiết 6', '13:30', '14:20', '13:00', '13:50', 6),
(10, 4, 'Tiết 7', '14:25', '15:15', '13:55', '14:45', 7),
(11, 4, 'Tiết 8', '15:20', '16:10', '14:50', '15:40', 8),
(12, 4, 'Tiết 9', '16:15', '17:05', '15:45', '16:35', 9),
(13, 4, 'Tiết 10', '17:10', '18:00', '16:40', '17:30', 10),
(14, 5, 'SA', '06:20', '11:33', NULL, NULL, 1),
(21, 5, 'CH', '13:29', '15:34', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `idnumber` varchar(100) DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `token_reset_pass` varchar(100) DEFAULT NULL,
  `date_reset_pass` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `birthday` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `email_verified_at`, `remember_token`, `firstname`, `lastname`, `idnumber`, `gender`, `avatar`, `token_reset_pass`, `date_reset_pass`, `address`, `status`, `birthday`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Q9aMGuOof7wo.SNtXFtLbOLc8hNqT2hexuQZSw2QG10JbBAEyeYL.', 'phongtrank55@gmail.com', NULL, NULL, 'System', 'Admin', 'admin', 1, 'avatar/1556189479/ZFtUyk5RPcycUSweVh2Z6riioUkMjzkhoAaLTEbP.jpeg', NULL, NULL, 'Phan Cong Tich', 1, '11/07/1996', '2019-04-25 17:16:32', '2019-04-25 10:16:32'),
(2, 'sangtv', '$2y$10$Z6TdQk2iJPOPlnaKT1nZYO.TOAn./MpR4ySz2S2TKOSFEojGqc9Li', 'xx@gmail.com', NULL, NULL, 'Sáng', 'Trần Văn', '1223', 1, 'avatar/1556189763/lrJnnLoAoBQ2XrDldPtkW8I2zwFYiC1pxxeJSLVq.jpeg', NULL, NULL, 'ads', 0, '11/06/1886', '2019-04-25 15:54:42', '2019-04-25 08:54:42'),
(5, 'abc', '$2y$10$Z86QoejNZsSFQaFI8VDi0O.aM9DZHRt48tJFnh0AQ6IBhsOhZjtK6', NULL, NULL, NULL, 'acx', 'abvc', 'abc', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 04:10:18', '2019-04-25 04:10:18'),
(6, 'xxx', '$2y$10$8mDLGr6zz/tiL2ILChKAI.Q9nZG0VpdYSTLRwVGH7yRtTMQK6Vhn6', NULL, NULL, NULL, 'xx', 'xx', 'xx', 1, 'avatar/1556192071/W817f41yUir3cq3pyRFRwVvvMHf8Ob7wdCjJqAmE.jpeg', NULL, NULL, NULL, 1, NULL, '2019-04-25 04:34:32', '2019-04-25 04:34:32'),
(7, 'fff', '$2y$10$RO/EVN/UXG7hQZOUoYyJ4uVmig/4l4BcY2L/K/6yIooaHR8r0MF9K', NULL, NULL, NULL, 'dd', 'fff', '122', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-04-25 19:27:45', '2019-04-25 12:27:45'),
(8, 'axx', '$2y$10$o4r/Mk8f6jN5nrVHip11LeHhdoGJUgzOBCEqyKWbxRG6itFKaJTBm', 'ptx@gmail.com', NULL, NULL, 'axx', 'axx', '123', 1, 'public/avatar/1556212487/3V6y63VO8PA9Kz8Ob3serEKvfTJGrDjebx0FNLjz.jpeg', NULL, NULL, 'Phan Cong Tich', 1, NULL, '2019-04-25 10:14:48', '2019-04-25 10:14:48'),
(9, 'tth', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'Hung', 'Tran', 'tth', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-03 11:10:24', '2019-05-03 11:10:24'),
(10, 'ppt', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', 'ptxc@gmail.com', NULL, NULL, 'Phong', 'Van', 'ppt', 1, NULL, NULL, NULL, NULL, 1, NULL, '2019-05-03 18:16:10', '0000-00-00 00:00:00'),
(11, '1755234030100029', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'CHI', 'HOÀNG THỊ LINH', '1755234030100029', 1, NULL, NULL, NULL, NULL, 1, '20/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(12, '1755234030100036', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'CHUNG', 'TRẦN THỊ THÙY', '1755234030100036', 1, NULL, NULL, NULL, NULL, 1, '22/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(13, '1755234030100035', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'DUYÊN', 'LÊ THỊ PHƯƠNG', '1755234030100035', 1, NULL, NULL, NULL, NULL, 1, '12/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(14, '1755234030100021', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'DƯƠNG', 'TRƯƠNG THỊ', '1755234030100021', 1, NULL, NULL, NULL, NULL, 1, '23/10/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(15, '1755234030100030', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'GIANG', 'NGUYỄN THỊ', '1755234030100030', 1, NULL, NULL, NULL, NULL, 1, '07/03/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(16, '1755234030100025', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'HOÀI', 'NGUYỄN THỊ THU', '1755234030100025', 1, NULL, NULL, NULL, NULL, 1, '15/09/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(17, '1755234010100138', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'Hoàng', 'Võ Huy', '1755234010100138', 1, NULL, NULL, NULL, NULL, 1, '18/02/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(18, '1755234030100031', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'HÙNG', 'PHAN TUẤN', '1755234030100031', 1, NULL, NULL, NULL, NULL, 1, '08/11/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(19, '1755234030100519', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'MINH', 'HOÀNG MINH', '1755234030100519', 1, NULL, NULL, NULL, NULL, 1, '01/09/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(20, '1755234030100019', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NGỌC', 'TRẦN BÍCH', '1755234030100019', 1, NULL, NULL, NULL, NULL, 1, '14/06/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(21, '1755234030100023', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NGỌC', 'TRẦN THỊ HỒNG', '1755234030100023', 1, NULL, NULL, NULL, NULL, 1, '01/10/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(22, '1755234030100022', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'NHUNG', 'NGUYỄN THỊ', '1755234030100022', 1, NULL, NULL, NULL, NULL, 1, '02/08/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(23, '1755234030100020', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'PHƯƠNG', 'TRẦN HÀ', '1755234030100020', 1, NULL, NULL, NULL, NULL, 1, '18/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(24, '1755234030100032', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'SEN', 'NGUYỄN THỊ', '1755234030100032', 1, NULL, NULL, NULL, NULL, 1, '24/04/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(25, '1755234030100024', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THÀNH', 'BÙI SỸ', '1755234030100024', 1, NULL, NULL, NULL, NULL, 1, '11/06/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(26, '1755234030100033', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THẢO', 'NGUYỄN PHƯƠNG', '1755234030100033', 1, NULL, NULL, NULL, NULL, 1, '03/05/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(27, '1755234030100034', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THUÝ', 'NGUYỄN THỊ', '1755234030100034', 1, NULL, NULL, NULL, NULL, 1, '13/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(28, '1755234030100026', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THÚY', 'LÊ THỊ', '1755234030100026', 1, NULL, NULL, NULL, NULL, 1, '17/07/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(29, '1755234030100027', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'THƯƠNG', 'HOÀNG THỊ', '1755234030100027', 1, NULL, NULL, NULL, NULL, 1, '28/01/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00'),
(30, '1755234030100028', '$2y$10$tEhI1VcRg/3uhHlau3t.1.sVDTS8DgnmicKDuBh9DUlLKq2duCgHq', NULL, NULL, NULL, 'TIÊN', 'CAO THỊ THỦY', '1755234030100028', 1, NULL, NULL, NULL, NULL, 1, '02/11/1999', '2019-05-03 18:41:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_machines`
--

CREATE TABLE `user_machines` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `machineid` int(11) DEFAULT NULL,
  `usercode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_machines`
--

INSERT INTO `user_machines` (`id`, `userid`, `machineid`, `usercode`) VALUES
(1, 1, 2, 'admin'),
(2, 2, 2, '1223'),
(3, 5, 2, 'abc'),
(4, 6, 2, 'xx'),
(5, 7, 2, '122'),
(6, 8, 2, '123'),
(7, 9, 2, 'tth'),
(8, 10, 2, 'ppt'),
(10, 12, 2, '1755234030100036'),
(12, 14, 2, '1755234030100021'),
(13, 15, 2, '1755234030100030'),
(14, 16, 2, '1755234030100025'),
(15, 17, 2, '1755234010100138'),
(16, 18, 2, '1755234030100031'),
(17, 19, 2, '1755234030100519'),
(18, 20, 2, '1755234030100019'),
(19, 21, 2, '1755234030100023'),
(20, 22, 2, '1755234030100022'),
(21, 23, 2, '1755234030100020'),
(22, 24, 2, '1755234030100032'),
(23, 25, 2, '1755234030100024'),
(24, 26, 2, '1755234030100033'),
(25, 27, 2, '1755234030100034'),
(26, 28, 2, '1755234030100026'),
(27, 29, 2, '1755234030100027'),
(28, 30, 2, '1755234030100028'),
(29, 11, 2, '1755234030100029'),
(31, 2, 1, '122345'),
(32, 5, 1, 'abcxxx'),
(34, 7, 1, '122'),
(36, 9, 1, 'tth'),
(37, 10, 1, 'ppt'),
(38, 12, 1, '1755234030100036'),
(40, 14, 1, '1755234030100021'),
(41, 15, 1, '1755234030100030'),
(42, 16, 1, '1755234030100025'),
(43, 17, 1, '1755234010100138'),
(44, 18, 1, '1755234030100031'),
(45, 19, 1, '1755234030100519'),
(46, 20, 1, '1755234030100019'),
(47, 21, 1, '1755234030100023'),
(48, 22, 1, '1755234030100022'),
(49, 23, 1, '1755234030100020'),
(50, 24, 1, '1755234030100032'),
(51, 25, 1, '1755234030100024'),
(52, 26, 1, '1755234030100033'),
(53, 27, 1, '1755234030100034'),
(54, 28, 1, '1755234030100026'),
(55, 29, 1, '1755234030100027'),
(56, 30, 1, '1755234030100028'),
(57, 11, 1, '1755234030100029'),
(58, 8, 1, '123');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_attendance`
-- (See below for the actual view)
--
CREATE TABLE `v_attendance` (
`id` int(11)
,`userid` int(11)
,`lastname` varchar(100)
,`firstname` varchar(100)
,`idnumber` varchar(100)
,`birthday` varchar(100)
,`email` varchar(100)
,`timestartenrol` int(11)
,`timeendenrol` int(11)
,`activitycalendardetailid` int(11)
,`timein` varchar(10)
,`timeout` varchar(10)
,`percent_present_shift` varchar(10)
,`rawvalue` int(11)
,`finalvalue` int(11)
,`overridden` int(11)
,`useroverridden` int(11)
,`note` varchar(200)
,`date_attendance` int(11)
,`modified` int(11)
,`completed` int(11)
,`status` int(11)
,`activityid` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_raw_records`
-- (See below for the actual view)
--
CREATE TABLE `v_raw_records` (
`machineid` int(11)
,`userid` int(11)
,`timeattendance` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_attendance`
--
DROP TABLE IF EXISTS `v_attendance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_attendance`  AS  select `aa`.`id` AS `id`,`ae`.`userid` AS `userid`,`u`.`lastname` AS `lastname`,`u`.`firstname` AS `firstname`,`u`.`idnumber` AS `idnumber`,`u`.`birthday` AS `birthday`,`u`.`email` AS `email`,`ae`.`timestart` AS `timestartenrol`,`ae`.`timeend` AS `timeendenrol`,`acd`.`id` AS `activitycalendardetailid`,`aa`.`timein` AS `timein`,`aa`.`timeout` AS `timeout`,`aa`.`percent_present_shift` AS `percent_present_shift`,`aa`.`rawvalue` AS `rawvalue`,`aa`.`finalvalue` AS `finalvalue`,`aa`.`overridden` AS `overridden`,`aa`.`useroverridden` AS `useroverridden`,`aa`.`note` AS `note`,`acd`.`date_attendance` AS `date_attendance`,`acd`.`modified` AS `modified`,`acd`.`completed` AS `completed`,`acd`.`status` AS `status`,`acd`.`activityid` AS `activityid` from (((`users` `u` join `activity_enrols` `ae` on((`ae`.`userid` = `u`.`id`))) join `activity_calendar_details` `acd` on((`acd`.`activityid` = `ae`.`activityid`))) left join `activity_attendances` `aa` on(((`aa`.`activitycalendardetailid` = `acd`.`id`) and (`aa`.`userid` = `ae`.`userid`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_raw_records`
--
DROP TABLE IF EXISTS `v_raw_records`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_raw_records`  AS  select `mr`.`machineid` AS `machineid`,`um`.`userid` AS `userid`,`mr`.`timeattendance` AS `timeattendance` from (`machine_records` `mr` join `user_machines` `um` on(((`um`.`machineid` = `mr`.`machineid`) and (`mr`.`usercode` = `um`.`usercode`)))) order by `mr`.`timeattendance` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activitycategoryid` (`activitycategoryid`),
  ADD KEY `shiftid` (`shiftid`);

--
-- Indexes for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `activitycalendardetailid` (`activitycalendardetailid`);

--
-- Indexes for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activityid` (`activityid`),
  ADD KEY `shift_detail_end_id` (`shift_detail_end_id`),
  ADD KEY `shift_detail_start_id` (`shift_detail_start_id`),
  ADD KEY `roomid` (`roomid`);

--
-- Indexes for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activityid` (`activityid`),
  ADD KEY `roomid` (`roomid`),
  ADD KEY `holidayid` (`holidayid`);

--
-- Indexes for table `activity_calendar_detail_shifts`
--
ALTER TABLE `activity_calendar_detail_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activitycalendardetailid` (`activitycalendardetailid`);

--
-- Indexes for table `activity_categories`
--
ALTER TABLE `activity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `activityid` (`activityid`);

--
-- Indexes for table `capabilities`
--
ALTER TABLE `capabilities`
  ADD PRIMARY KEY (`capabilitycode`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contexts`
--
ALTER TABLE `contexts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_records`
--
ALTER TABLE `machine_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `machineid` (`machineid`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_allow_assigns`
--
ALTER TABLE `role_allow_assigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roleid` (`roleid`),
  ADD KEY `roleidassign` (`roleidassign`);

--
-- Indexes for table `role_capabilities`
--
ALTER TABLE `role_capabilities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roleid` (`roleid`),
  ADD KEY `capabilitycode` (`capabilitycode`);

--
-- Indexes for table `role_context_levels`
--
ALTER TABLE `role_context_levels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roleid` (`roleid`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomgroupid` (`roomgroupid`);

--
-- Indexes for table `room_groups`
--
ALTER TABLE `room_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roomid` (`roomid`),
  ADD KEY `machineid` (`machineid`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shiftid` (`shiftid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_machines`
--
ALTER TABLE `user_machines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `machineid` (`machineid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=895;

--
-- AUTO_INCREMENT for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=291;

--
-- AUTO_INCREMENT for table `activity_calendar_detail_shifts`
--
ALTER TABLE `activity_calendar_detail_shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `activity_categories`
--
ALTER TABLE `activity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contexts`
--
ALTER TABLE `contexts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `machines`
--
ALTER TABLE `machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `machine_records`
--
ALTER TABLE `machine_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role_allow_assigns`
--
ALTER TABLE `role_allow_assigns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role_capabilities`
--
ALTER TABLE `role_capabilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `role_context_levels`
--
ALTER TABLE `role_context_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `room_groups`
--
ALTER TABLE `room_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `room_machines`
--
ALTER TABLE `room_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `shift_details`
--
ALTER TABLE `shift_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `user_machines`
--
ALTER TABLE `user_machines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`activitycategoryid`) REFERENCES `activity_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`shiftid`) REFERENCES `shifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity_attendances`
--
ALTER TABLE `activity_attendances`
  ADD CONSTRAINT `activity_attendances_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_attendances_ibfk_2` FOREIGN KEY (`activitycalendardetailid`) REFERENCES `activity_calendar_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity_calendars`
--
ALTER TABLE `activity_calendars`
  ADD CONSTRAINT `activity_calendars_ibfk_1` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_calendars_ibfk_2` FOREIGN KEY (`shift_detail_end_id`) REFERENCES `shift_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_calendars_ibfk_3` FOREIGN KEY (`shift_detail_start_id`) REFERENCES `shift_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_calendars_ibfk_4` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity_calendar_details`
--
ALTER TABLE `activity_calendar_details`
  ADD CONSTRAINT `activity_calendar_details_ibfk_1` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_calendar_details_ibfk_2` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_calendar_details_ibfk_3` FOREIGN KEY (`holidayid`) REFERENCES `holidays` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity_calendar_detail_shifts`
--
ALTER TABLE `activity_calendar_detail_shifts`
  ADD CONSTRAINT `activity_calendar_detail_shifts_ibfk_1` FOREIGN KEY (`activitycalendardetailid`) REFERENCES `activity_calendar_details` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `activity_enrols`
--
ALTER TABLE `activity_enrols`
  ADD CONSTRAINT `activity_enrols_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_enrols_ibfk_2` FOREIGN KEY (`activityid`) REFERENCES `activities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `machine_records`
--
ALTER TABLE `machine_records`
  ADD CONSTRAINT `machine_records_ibfk_1` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_allow_assigns`
--
ALTER TABLE `role_allow_assigns`
  ADD CONSTRAINT `role_allow_assigns_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_allow_assigns_ibfk_2` FOREIGN KEY (`roleidassign`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_capabilities`
--
ALTER TABLE `role_capabilities`
  ADD CONSTRAINT `role_capabilities_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_capabilities_ibfk_2` FOREIGN KEY (`capabilitycode`) REFERENCES `capabilities` (`capabilitycode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_context_levels`
--
ALTER TABLE `role_context_levels`
  ADD CONSTRAINT `role_context_levels_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`roomgroupid`) REFERENCES `room_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `room_machines`
--
ALTER TABLE `room_machines`
  ADD CONSTRAINT `room_machines_ibfk_1` FOREIGN KEY (`roomid`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_machines_ibfk_2` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `shift_details`
--
ALTER TABLE `shift_details`
  ADD CONSTRAINT `shift_details_ibfk_1` FOREIGN KEY (`shiftid`) REFERENCES `shifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_machines`
--
ALTER TABLE `user_machines`
  ADD CONSTRAINT `user_machines_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_machines_ibfk_2` FOREIGN KEY (`machineid`) REFERENCES `machines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
