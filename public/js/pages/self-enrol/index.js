

$('#modal-enrol').on('show.bs.modal', function (event) {
        
    $('#notification-enrol').hide();
    var button = $(event.relatedTarget) // Button that triggered the modal
    var title = button.data('title'); // Extract info from data-* attributes
    
    var id = button.data('id');
    var title = button.data('title');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);

    modal.find('.modal-body #modal-enrol-id').val(id);
    modal.find('.modal-body #modal-enrol-title').text(title);
    
});

$('#btn-submit-enrol').click(function(e){
    // alert('ok');
    var modal = $(this).closest('.modal');
    var activityid = modal.find('.modal-body input[name="activityid"]').val();
    var password = modal.find('.modal-body input[name="password"]').val();
    var token = modal.find('input[name="_token"]').val();
    // alert(token);
    // var url = curUrl + '/store-enrol-user';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
    $.post( url,
        {
            activityid : activityid,
            password : password
        }, function(result){
            console.log(result);
            if(!result.success){
                modal.find('#notification-enrol').show();
                modal.find('#notification-enrol #message').text(result.message);
            }else{
                // console.log('ok');
                window.location.href = result.url;
            }
        }, 'json');
});
