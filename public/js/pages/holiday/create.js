$(function(){
    setHideIf($('select[name="type"]'), 'value', 1, $('#div-enddate'));

    $('form#form-department').validate({ // initialize the plugin

        rules: {
            fullname: {
                required: true
            },
            shortname: {
                required: true
            },
            startdate: {
                required: true
            },
            enddate: {
                required: true
            }
        },
        messages: {
            fullname: "Tên viết tắt không thể để trống!",
            shortname: "Tên viết tắt không thể để trống!",
            startdate: "Ngày bắt đầu không thể để trống",
            enddate: "Ngày kết thúc không thể để trống"
        }
    
    });
    
    // 
    // alert('ok');
    var firstload = true;

    $('select#type-holiday').change(function(){
        var format = $(this).val() == 0 ? 'dd/mm/yyyy' : 'dd/mm';
        // alert(format);
        $('.input-daterange').datepicker('destroy');
        $('.input-daterange').datepicker({
            autoclose: true,
            format: format,
            language: 'vi',
            // todayHighlight: true
        });
        
        //check validate
        if($(this).val() == 0)
            $('input[name="enddate"]' ).rules( "add", {
                required: true,
                messages: {
                    required: "Không thể để trống!"
                }
            });
        else
            $('input[name="enddate"]' ).rules('remove');

        //load 

        if(firstload)
            firstload = false;
        else // tránh mới hiển thị đã xóa
            $('.input-daterange input[type="text"]').val('');
        // $('.input-daterange').datepicker('update');   
        
    });
    $('select#type-holiday').trigger('change');
    
});