$(function(){

    $.validator.addMethod(
        "dateFormatWithoutYear",
        function(value, element) {
            var check = false;
            var re = /^\d{1,2}\/\d{1,2}$/;
                if( re.test(value)){
                    var adata = value.split('/');
                    var dd = parseInt(adata[0],10);
                    var mm = parseInt(adata[1],10);
                    // var yyyy = parseInt(adata[2],10);
                    var xdata = new Date(2019,mm-1,dd);
                    if (( xdata.getMonth () === mm - 1 ) && ( xdata.getDate() === dd ) ) {
                    check = true;
                }
                else {
                    check = false;
                }
            } else {
            check = false;
            }
            return this.optional(element) || check;
        },
        "Wrong date format"
    );

    $('form#form-department').validate({ // initialize the plugin
        rules: {
            name: {
                required: true
            },
        },
        messages: {
            name: "Tên không thể để trống!",
        }
        
        });
        

$('#modal-delete').on('show.bs.modal', function (event) {
		
    var button = $(event.relatedTarget) // Button that triggered the modal
    // var title = button.data('title'); // Extract info from data-* attributes
    currentDeleteRow = button.closest('tr');
    // console.log(currentDeleteRow.html());    
    // var id = button.data('id');
    // var title = button.data('title');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    // var modal = $(this);

    // modal.find('.modal-body #modal-delete-id').val(id);
    // modal.find('.modal-body #modal-delete-name').text(title);
    
});

var currentDeleteRow = null;


$('input[name="season"]').on('click', function(){
    // alert($(this).is(":checked"));
    handleSeason();
});

function handleSeason(){
    var season = $('input[name="season"]').is(":checked");
    if(season){
        $('#calBySeason').show();
        $('table#detail-table .time-winter').show();
        $('input[name="datestartsummer"]' ).rules( "add", {
            required: true,
            dateFormatWithoutYear:true, 
            messages: {
                required: "Không thể để trống!",
                dateFormatWithoutYear: "Định dạng không hợp lệ"
            }
        });
        $('input[name="datestartwinter"]' ).rules( "add", {
            required: true,
            dateFormatWithoutYear:true, 
            messages: {
                required: "Không thể để trống!",
                dateFormatWithoutYear: "Định dạng không hợp lệ"
            }
        });
    }else{
        $('#calBySeason').hide();
        $('table#detail-table .time-winter').hide();
        $('input[name="datestartsummer"]' ).rules( "remove");
        $('input[name="datestartwinter"]' ).rules( "remove");
    }
}

handleSeason();


$('table#detail-table').on('click', '#btnAddRow', function(){
    // alert('ok');
    var cur_tr_class = $(this).closest('tr').attr('class');
    $(this).closest('tr').removeClass('last-row');
    var rowHtml = '<tr class="'+cur_tr_class+'">' + $(this).closest('tr').html() +'</tr>';
    $(this).closest('tbody').append(rowHtml);
    $(this).closest('td').html('');
    // console.log(rowHtml);
    $('tr.last-row input').val('');
});
// $('table#detail-table').on('click', '#btnDeleteRow', function(){
$('#btnDeleteRow').click(function(){
    if(currentDeleteRow.hasClass('last-row'))
        currentDeleteRow.find('input').val('');
    else
        currentDeleteRow.remove();
});

});

