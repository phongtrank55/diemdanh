$('form#form-department').validate({ // initialize the plugin

    rules: {
        roomgroupcode: {
            required: true
        },

        name: {
            required: true,
            
        },

  },
    messages: {
        roomgroupcode: "Mã nhóm phòng không thể để trống!",
        name: "Tên nhóm phòng không thể để trống!",
    }

});