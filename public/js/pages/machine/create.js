$('form#form-department').validate({ // initialize the plugin

    rules: {
        machinecode: {
            required: true
        },

        model: {
            required: true,    
        },

        filename: {
            required: true,    
        },
    },
    messages: {
        machinecode: "Mã máy không thể để trống!",
        model: "Model không thể để trống!",
        model: "Tên tệp không thể để trống!",
    }

});

