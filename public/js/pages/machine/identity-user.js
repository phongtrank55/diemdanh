$(function(){
    
    $('#modal-delete').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        
        var id = button.data('id');
        var title = button.data('title');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
    
        modal.find('.modal-body #modal-delete-id').val(id);
        modal.find('.modal-body #modal-delete-name').text(title);
        
    });

    $('#modal-edit-identity').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        
        var id = button.data('id');
        var title = button.data('title');
        var usercode = button.data('usercode');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
    
        modal.find('.modal-body #modal-id').val(id);
        modal.find('.modal-body #modal-edit-name').text(title);
        modal.find('.modal-body #modal-usercode').val(usercode);
        modal.find('#notification-identity').hide();
        
    });

    $('.btn-submit-identity').click(function(e){

        var modal = $(this).closest('.modal');
        var usercode = modal.find('.modal-body input[name="usercode"]').val();
        var userid = modal.find('.modal-body #modal-id').val();
        var token = modal.find('input[name="_token"]').val();
        // alert(token);
        var url = curUrl + '/store-identity-user';
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });
        $.post( url,
            {
                usercode : usercode,
                userid : userid
            }, function(result){
                // console.log(result);
                if(!result.success){
                    modal.find('#notification-identity').show();
                    modal.find('#notification-identity #message').text(result.message);
                }else{
                    // console.log('ok');
                    window.location.href = curUrl;
                }
            }, 'json');
    });

    $('#modal-add-identity').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        // alert('ok');
        modal.find('#notification-identity').hide();
        var selUser = modal.find('.modal-body #modal-id');
        selUser.empty();
        var url = curUrl + '/get-potential-user';
        $.get(url, function(data){
            // console.log(data);
            $.each(data, function (index, user) { 
                //  console.log(user.id);
                var item = '<option value="'+user.id+'">'+ user.lastname + ' ' +
                         user.firstname + ' - ' + user.idnumber + '</option>';
                selUser.append(item);
            });
            
        },'json');
        
    });
});