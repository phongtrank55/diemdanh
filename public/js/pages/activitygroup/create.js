$('form#form-department').validate({ // initialize the plugin

    rules: {
        activitycategorycode: {
            required: true
        },

        name: {
            required: true,
            
        },

  },
    messages: {
        activitycategorycode: "Mã danh mục hoạt động không thể để trống!",
        name: "Tên danh mục hoạt động không thể để trống!",
    }

});