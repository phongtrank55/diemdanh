$(function(){
    $('#btn-eye').click(function(){

        type = $('input[name="password"]').attr('type');
        if(type=='text'){
            $('input[name="password"]').attr('type', 'password');
            $('#btn-eye i').attr('class', 'fa fa-eye');
        }else{
            $('input[name="password"]').attr('type', 'text');
            $('#btn-eye i').attr('class', 'fa fa-eye-slash');
        }
    });

    $('form#form-department').validate({ // initialize the plugin

        rules: {
            username: {
                required: true
            },
        
            password: {
                required: true,
              
            },
             idunumber: {
                required: true,
              
            },
        
            avatar: {
                extension: "jpeg|png|jpg"
            },
            lastname: {
                required: true
            },
        
            firstname: {
                required: true
            },
        
            birthday: {
                // required: true
            },
            email: {
                // required: true,
                email:true
            }
        },
        messages: {
            firstname: "Tên không thể để trống!",
            lastname: "Họ không thể để trống!",
            idunumber: "Mã thành viên không thể để trống!",
            birthday: "Ngày sinh không thể để trống!",
            username: "Tên đăng nhập không thể để trống!",
            password: "Mật khẩu không thể để trống!",
            email: {
                required: "Email không được để trống!",
                 email: "Không đúng định dạng email!"
            },
            avatar: "Chỉ cho phép định dạng JPG/PNG !"
        }
        
        });
        
});