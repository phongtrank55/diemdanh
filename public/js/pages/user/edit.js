$(function(){
    $('#btn-eye').click(function(){

        type = $('input[name="password"]').attr('type');
        if(type=='text'){
            $('input[name="password"]').attr('type', 'password');
            $('#btn-eye i').attr('class', 'fa fa-eye');
        }else{
            $('input[name="password"]').attr('type', 'text');
            $('#btn-eye i').attr('class', 'fa fa-eye-slash');
        }
    });

    $('a#enableChangePassword').click(function(e){
        e.preventDefault();
        // alert('ok');
        var change = $('input[name="changePassword"]').val();
        if(change == 1){ //khong doi pass
            $('#inputPassword').show();
            $('input[name="password"]' ).rules( "remove");
            $('#textRequiredPassword').hide();
            $('input[name="changePassword"]').val('0');
            $(this).text('Thay đổi mật khẩu');
        }else{
            $('#inputPassword').show();
            $('input[name="password"]' ).rules( "add", {
                required: true,
                messages: {
                    required: "Mật khẩu không thể để trống!",
                }
            });
            $('#textRequiredPassword').show();
            $('input[name="changePassword"]').val('1');
            $(this).text('Hủy thay đổi mật khẩu');
        }
    });
    $('form#form-department').validate({ // initialize the plugin

        rules: {
            username: {
                required: true
            },
        
            // password: {
                // required: true,      
            // },
             idunumber: {
                required: true,
              
            },
        
            avatar: {
                extension: "jpeg|png|jpg"
            },
            lastname: {
                required: true
            },
        
            firstname: {
                required: true
            },
        
            birthday: {
                // required: true
            },
            email: {
                // required: true,
                email:true
            }
        },
        messages: {
            firstname: "Tên không thể để trống!",
            lastname: "Họ không thể để trống!",
            idunumber: "Mã thành viên không thể để trống!",
            birthday: "Ngày sinh không thể để trống!",
            username: "Tên đăng nhập không thể để trống!",
            // password: "Mật khẩu không thể để trống!",
            email: {
                required: "Email không được để trống!",
                 email: "Không đúng định dạng email!"
            },
            avatar: "Chỉ cho phép định dạng JPG/PNG !"
        }
        
        });
        
});