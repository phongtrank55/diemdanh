$(function(){
    $('form#form-department').validate({ // initialize the plugin

        rules: {
            rolecode: {
                required: true
            },
            name: {
                required: true
            }
        },
        messages: {
            rolecode: "Mã vai trò không thể để trống!",
            name: "Tên vai trò không thể để trống!",
            
        }
    
    });
    
});