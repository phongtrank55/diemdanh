$('form#form-department').validate({ // initialize the plugin

    rules: {
        roomcode: {
            required: true
        },

        name: {
            required: true,
            
        },

  },
    messages: {
        roomcode: "Mã phòng không thể để trống!",
        name: "Tên phòng không thể để trống!",
    }

});