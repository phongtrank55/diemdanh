$(function(){
    $('.actual_range').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'vi',
        todayHighlight: true
    });
    $(".actual_range").datepicker().on('show.bs.modal', function(event) {
        // prevent datepicker from firing bootstrap modal "show.bs.modal"
        event.stopPropagation(); 
    });
    $( ".actual_range" ).datepicker( "setDate", new Date());


// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//     }
// });


//  $.ajax({
//     url: "{{ url('/grocery/post') }}",
//     method: 'post',
//     data: {
//        name: jQuery('#name').val(),
//        type: jQuery('#type').val(),
//        price: jQuery('#price').val()
//     },
//     success: function(result){
//        console.log(result);
//     }});


    $('#modal-delete').on('show.bs.modal', function (event) {
            
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        
        var id = button.data('id');
        var title = button.data('title');
        // alert(id);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
    
        modal.find('.modal-body #modal-delete-id').val(id);
        modal.find('.modal-body #modal-delete-name').text(title);
        
    });

    $('#modal-enrol').on('show.bs.modal', function (event) {
        $('#potential-user').empty();
        $.get('potential-users/'+$('#modal-ennol-id').val(), function(data){
            // alert(data);
            // console.log(data);
            $.each(data, function (indexInArray, user) { 
                //  console.log(user.id);
                var item = '<option value="'+user.id+'">'+ user.lastname + ' ' +
                         user.firstname + ' - ' + user.idnumber + '</option>';
                $('#potential-user').append(item);
            });
            
        },'json');
        
    });
});