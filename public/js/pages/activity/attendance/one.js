$(function(){
    function load_date_time_picker(){
        $('#datepicker').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'vi',
            todayHighlight: true
        });
    }

    $('#calendar-detail').change(function(){

        $('#date-attendance').val($( "#calendar-detail option:selected" ).data('date'));
        var url = 'result-by-detail/' + $(this).val();
        $('.loader').show();
        $( "div#result").html('');
        $( "div#result" ).load(url, function() {
            // console.log('ok');
            $('.loader').hide();
        });

    });

    load_date_time_picker();

    $('#calendar-detail').trigger('change');
    
});