$(function(){
    function load_date_time_picker(){
        $('.actual_range').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: 'vi',
            todayHighlight: true
        });
        
        $('.timepicker').timepicker({
            showInputs: false,
            showMeridian: false
        }); 
    }

    load_date_time_picker();

    // $('div#work-calendar table').find('input[type="text"]').attr('required', true);
      //Delete and add row table
    $('div#work-calendar').on('click', '#btnAddRow', function(){
        // alert('ok');
        var cur_tr_class = $(this).closest('tr').attr('class');
        $(this).closest('tr').removeClass('last-row');
        var rowHtml = '<tr class="'+cur_tr_class+'">' + $(this).closest('tr').html() +'</tr>';
        $(this).closest('tbody').append(rowHtml);
        // $(this).closest('tr').find('input[type="text"]').prop('required', true);
        $(this).closest('td').html('');
        // console.log(rowHtml);
        $('tr.last-row input').val('');

        // $(this).closest('tbody').find('input[type="text"]').prop('required', true);
        load_date_time_picker();
    });

    $('#btnDeleteRow').click(function(){
        if(currentDeleteRow.hasClass('last-row'))
            currentDeleteRow.find('input').val('');
        else
            currentDeleteRow.remove();
    });
    $('#modal-delete').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        currentDeleteRow = button.closest('tr');        
    });
    
    var currentDeleteRow = null;
   
});