$(function(){
    $('a.treeview').on('click', function(e){
        e.preventDefault();
        var classOpen = 'fa fa-plus';
        var classClose = 'fa fa-minus';
        var iconExpand = $(this).find('i:first');
        var currentClass = iconExpand.attr('class');
        if(classOpen==currentClass){
            iconExpand.attr('class', classClose);
            $(this).parent().find('.treeview-menu:first').show();
        } else {
            $(this).parent().find('.treeview-menu:first').hide();
            iconExpand.attr('class', classOpen);
        }
    });


    $('#modal-delete-activitycategory').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        
        var id = button.data('id');
        var title = button.data('title');
        // alert(id);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
    
        modal.find('.modal-body #modal-delete-id').val(id);
        modal.find('.modal-body #modal-delete-name').text(title);
        
    });
    
    $('#modal-delete-activity').on('show.bs.modal', function (event) {
            
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        
        var id = button.data('id');
        var title = button.data('title');
        // alert(id);
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
    
        modal.find('.modal-body #modal-delete-id').val(id);
        modal.find('.modal-body #modal-delete-name').text(title);
        
    });
});


