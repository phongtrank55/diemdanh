$(function(){
    $('form#form-department').validate({ // initialize the plugin

        rules: {
            activitycode: {
                required: true
            },
    
            name: {
                required: true,
                
            },
    
      },
        messages: {
            activitycode: "Mã hoạt động không thể để trống!",
            name: "Tên hoạt động không thể để trống!",
        }
    
    });

    setDisableIf($('input[type="checkbox"][name="enable_late"]'), 'checked', false, $('input[name="minute_late"]'));
    setDisableIf($('input[type="checkbox"][name="enable_in"]'), 'checked', false, $('input[name="minute_before_in"]'));
    setDisableIf($('input[type="checkbox"][name="enable_in"]'), 'checked', false, $('input[name="minute_after_in"]'));
    setDisableIf($('input[type="checkbox"][name="enable_out"]'), 'checked', false, $('input[name="minute_before_out"]'));
    setDisableIf($('input[type="checkbox"][name="enable_out"]'), 'checked', false, $('input[name="minute_after_out"]'));
    setDisableIf($('input[type="checkbox"][name="enable_end_date"]'), 'checked', false, $('input[name="end_date"]'));
    setDisableIf($('input[type="checkbox"][name="enable_in"]'), 'checked', false, $('input[name="enable_attendance_in_between_shift"]'));
    setDisableIf($('input[type="checkbox"][name="enable_out"]'), 'checked', false, $('input[name="enable_attendance_out_between_shift"]'));
    setDisableIf($('input[type="checkbox"][name="enable_self_enrol"]'), 'checked', false, $('input[name="password_self_enrol"]'));
    setDisableIf($('input[type="checkbox"][name="enable_self_enrol"]'), 'checked', false, $('input[name="expired_self_enrol"]'));
    setDisableIf($('input[type="checkbox"][name="enable_self_enrol"]'), 'checked', false, $('input[name="enable_expired_self_enrol"]'));
    setDisableIf($('input[type="checkbox"][name="enable_expired_self_enrol"]'), 'checked', false, $('input[name="expired_self_enrol"]'));
    setDisableIf($('select[name="frequency"]'), 'value', 3, $('#shift'));
    //Date picker
    // $('#start_date_attendance').datepicker().on('changeDate', function(e) {
    //     $('#end_date_attendance').datepicker('setStartDate', $(this).val());
    // });
    // $('#btn').click(function(){
    //     alert($('input[name="start_date_attendance"]').val());
    // });

    // $('.input-daterange input').datepicker({
    //     autoclose: true,
    //     format: 'dd/mm/yyyy',
    //     language: 'vi',
    //     todayHighlight: true
    // });
    // $('.input-daterange input').each(function() {
    //     $(this).datepicker();
    // });
// $('body').on('click', function(){
// $('div#work-calendar').ready(function(){
    // alert('ready');
function load_date_time_picker(){
    $('.input-daterange').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'vi',
        // todayHighlight: true
    });

    $('.actual_range').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'vi',
        todayHighlight: true
    });

    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false
    });
    
    $('input[name="enable_end_date"]').datepicker('destroy');
};

load_date_time_picker();

    //Delete and add row table
    $('div#work-calendar').on('click', '#btnAddRow', function(){
        // alert('ok');
        var cur_tr_class = $(this).closest('tr').attr('class');
        $(this).closest('tr').removeClass('last-row');
        var rowHtml = '<tr class="'+cur_tr_class+'">' + $(this).closest('tr').html() +'</tr>';
        $(this).closest('tbody').append(rowHtml);
        // $(this).closest('tr').find('input[type="text"]').prop('required', true);
        $(this).closest('td').html('');
        // console.log(rowHtml);
        $('tr.last-row input').val('');

        load_date_time_picker();
    });
    $('#btnDeleteRow').click(function(){
        if(currentDeleteRow.hasClass('last-row'))
            currentDeleteRow.find('input').val('');
        else
            currentDeleteRow.remove();
    });
    $('#modal-delete').on('show.bs.modal', function (event) {
		
        var button = $(event.relatedTarget) // Button that triggered the modal
        currentDeleteRow = button.closest('tr');        
    });
    
    var currentDeleteRow = null;
   
    
    //xử lý ajax
    $('select#frequency').change(function(){
        // alert('ok');
        
        var url = '/doan/public/admin/activity/subview-table-frequency/' + $(this).val();
        if($(this).val() != 3)
            url = url + '/'+$('select#shift').val();
        if(typeof(param) != 'undefined'){
            url = url +'?activity='+param;
        }
    
        $( "div#work-calendar" ).load(url, function() {
            // alert( "Load was performed." );
            load_date_time_picker();
        });
        
    });
    $('select#shift').change(function(){
        // alert('ok');
        $('select#frequency').trigger('change');
    });
    $('select#frequency').trigger('change');
});
