
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Đăng nhập | Hệ thống điểm danh</title>
  
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/AdminLTE.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/mystyle.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/pages/login.css')}}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body"> 
    <p align="center">  <img src="{{asset('img/vinhuni.png')}}" alt="logo"> </p>
    <p class="login-box-msg" class="fix_log"> <a href="#"> <strong>HỆ THỐNG ĐIỂM DANH TỰ ĐỘNG </strong></a></p>

    @if($errors->any())
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <div class="text-center">
        Bạn nhập sai tên hoăc mật khẩu hoặc tài khoản của bạn đã bị khóa!</div>
      </div>
    @endif
    <form action="{{route('login')}}" method="post">
      @csrf
      <div class="form-group has-feedback">
        <input type="text" value="{{old('username')}}" class="form-control" name="username" placeholder="Tên Đăng Nhập..">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Mật Khẩu..">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        <div class="checkbox">
            <label>
              <input name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} type="checkbox"> Ghi nhớ tài khoản
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat fix_login"> Đăng nhập </button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links">
       <a href="{{ route('password.request') }}">Quyên mật khẩu ?</a><br>
    </div>
    <!-- /.social-auth-links -->

   
    

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

</body>

<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>