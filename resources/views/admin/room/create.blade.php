@extends('layouts.master')

@section('title', 'Thêm phòng')

@section ('content')

@if(empty($roomGroups))

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý!</strong> Chưa có nhóm phòng nên không thể thêm phòng!
</div>

@else
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm phòng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.room.store')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mã phòng:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="roomcode" value="{{old('roomcode')}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên phòng: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="name" class="form-control" value="{{old('name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Nhóm phòng:
                </label>
                <div class="col-md-3">
                <select name="roomgroupid" class="form-control">
                    <?php
                        $currentGroup = isset($roomGroupCurrent) ? $roomGroupCurrent : old('roomgroupid');
                    ?>
                    @foreach($roomGroups as $key => $value)
                        <option {{$currentGroup == $key ? 'selected':''}} value="{{$key}}">{{$value}}</option>        
                    @endforeach
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{old('note')}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@endif

@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/room/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('roomcode'))
				$('form#form-department').validate().showErrors({
				 	"roomcode": "{{$errors->first('roomcode')}}"
				});
          	@endif
          
		});

	</script>
@stop

