@extends('layouts.master')
@section('title', 'Quản lý phòng')
@section('content')

<!-- Danh sach may dien danh -->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Quản lý phòng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
   <!-- box-body -->
    <div class="box-body">
    <!-- room group table -->
    <div class="col-md-4 col-sm-12">
        <h4 class="text-center box-title">Danh sách nhóm phòng</h4>
        <div class="box-header">
            <a href="{{route('admin.roomgroup.create')}}" class="btn btn-sm btn-info pull-right"> 
                <i class="fa fa-plus"></i> Thêm nhóm phòng
            </a>
        </div>
        <div class="clearfix">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Mã nhóm phòng</th>
                        <th>Tên nhóm phòng</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php $index=1; ?>
                @foreach($roomGroups as $roomGroup)
                <tr>
                    <td class="text-center">{{$index++}}</td>
                    <td><a href="{{route('admin.room.index', ['roomgroup' => $roomGroup->id])}}"> {{$roomGroup->roomgroupcode}}</a> </td>
                    <td> {{$roomGroup->name}}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-primary btn-sm btn-flat dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-cog"></i> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="{{route('admin.roomgroup.edit', ['id'=>$roomGroup->id])}}">
                                    <i class="fa fa-pencil"></i> Sửa
                                </a></li>
                                <li><a data-toggle="modal" href="#modal-delete-roomgroup"
                                        data-id = "{{$roomGroup->id}}" 
                                        data-title = "{{$roomGroup->roomgroupcode}}"> 
                                    <i class="fa fa-trash"></i> Xóa
                                </a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- End room group table -->
    <!-- Room table -->
    <div class="col-md-8 col-sm-12">
        <h4 class="box-title text-center">Danh sách phòng làm việc</h4>
        <!-- form filter -->
        <div class="box-header">
            <form action="{{route('admin.room.index')}}" method="GET" class="form-inline" role="form">
                <div class="search-form col-md-10">
                    <div class="form-group">
                        <select name="roomgroup" class="form-control">
                            <option value="0">--- Nhóm phòng ---</option>        
                            @foreach($roomGroups as $roomGroup)
                            <option {{$roomGroupCurrent == $roomGroup->id ? 'selected': ''}} value="{{$roomGroup->id}}">
                                {{$roomGroup->roomgroupcode}}
                            </option>        
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="status" class="form-control">
                            <option value="-1">--- Tình trạng ---</option>
                            <option {{$status == 1 ? 'selected': ''}} value="1">Đã gán máy điểm danh</option>
                            <option {{$status == 0 ? 'selected': ''}} value="0">Chưa gán máy điểm danh</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-flat">
                        <i class="fa fa-filter"></i> Lọc
                    </button>
                </div> 
                <div class="col-md-2">
                    <button type="submit" formaction="{{route('admin.room.create')}}" class="btn btn-sm btn-info "> 
                        <i class="fa fa-plus"></i> Thêm phòng
                    </button>
                </div>
            </form> 
        </div>
        <div class="clearfix"></div>
        <!-- end form filter -->
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Mã phòng</th>
                        <th>Tên phòng</th>
                        <th>Máy điểm danh</th>
                        <th>Ghi chú</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $index=1; ?>
                    @foreach($rooms as $room)
                    <tr>
                        <td class="text-center">{{$index++}}</td>
                        <td>{{$room->roomcode}} </td>
                        <td> {{$room->name}}</td>
                        <td>
                        @foreach($room->machines as $machine)
                            <a href="{{route('admin.machine.edit', ['id' => $machine['id']])}}">
                                <label class="label {{$machine['status'] ? 'label-success' : 'label-danger'}}">
                                    {{$machine['machinecode']}} 
                                </label>
                            </a>
                            &nbsp;
                        @endforeach
                        </td>
                        <td> {{$room->note}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary btn-sm btn-flat dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1"  style="right: 0; left: auto;">
                                    <li><a href="{{route('admin.room.edit', ['id'=>$room->id])}}">
                                        <i class="fa fa-pencil"></i> Sửa
                                    </a></li>
                                    <li><a data-toggle="modal" href="#modal-delete-room"
                                            data-id = "{{$room->id}}" 
                                            data-title = "{{$room->roomcode}}"> 
                                        <i class="fa fa-trash"></i> Xóa
                                    </a></li>
                                    <li><a href="{{route('admin.room.assignmachine', ['id'=>$room->id])}}">
                                        <i class="fa fa-gavel"></i> Gán máy điểm danh
                                    </a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            <!-- box footer -->
            @if(!empty($rooms))
            <div class="box-footer">
                <label class="control-label"><span class="label-success">&nbsp;&nbsp;&nbsp;&nbsp;</span> Hoạt động </label>
                &nbsp; &nbsp;
                <label class="control-label"><span class="label-danger">&nbsp;&nbsp;&nbsp;&nbsp;</span> Không hoạt động </label>
            </div>
            @endif
            <!-- end box footer -->
        </div>
    <!-- End room table -->
    </div>
   <!-- End boxbody -->
    
</div>

<!-- Modal Delete Room Group-->
<div class="modal fade" id="modal-delete-roomgroup">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="">
                {{csrf_field()}}
                
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="id">
                    Bạn có chắc chắn xóa nhóm phòng <strong id="modal-delete-name"></strong> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End modal Delete Room Group-->
<!-- Modal Delete Room -->
<div class="modal fade" id="modal-delete-room">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="">
                {{csrf_field()}}
                
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="id">
                    Bạn có chắc chắn xóa phòng <strong id="modal-delete-name"></strong> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End modal Delete Room-->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/room/index.js')}}"></script>
@endsection