@extends('layouts.master')

@section('title', 'Sửa phòng')

@section ('content')

@if(empty($roomGroups))

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý!</strong> Chưa có nhóm phòng nên không thể sửa phòng!
</div>

@else
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Sửa phòng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.room.update')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <input type="hidden" name="id" value="{{$room->id}}">
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mã phòng:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="roomcode" value="{{$errors->any() ? old('roomcode') : $room->roomcode}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên phòng: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="name" class="form-control" value="{{$errors->any() ? old('name') : $room->name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Nhóm phòng:
                </label>
                <div class="col-md-3">
                <select name="roomgroupid" class="form-control">
                    <?php
                        $currentGroup = $errors->any() ? old('roomgroupid') : $room->roomgroupid;
                    ?>
                    @foreach($roomGroups as $key => $value)
                        <option {{$currentGroup == $key ? 'selected':''}} value="{{$key}}">{{$value}}</option>        
                    @endforeach
                </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{$errors->any() ? old('note') : $room->note }}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@endif

@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/room/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('roomcode'))
				$('form#form-department').validate().showErrors({
				 	"roomcode": "{{$errors->first('roomcode')}}"
				});
          	@endif
          
		});

	</script>
@stop

