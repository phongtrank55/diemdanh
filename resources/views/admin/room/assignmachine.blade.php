@extends('layouts.master')

@section('title', 'Gán máy điểm danh')

@section ('content')

@if(empty($potentialMachines))

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý!</strong> Chưa có máy điểm danh nên không gán được
</div>

@else
    @if(session()->has('messageAssignMachine'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Thông báo!</strong> {{session()->get('messageAssignMachine')}}
        </div>

    @endif
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Gán máy điểm danh cho {{$room->name}} ({{$room->roomcode}})</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <div class="col-sm-12 col-md-12">
            <form id="form1" action="{{route('admin.room.storeassign')}}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{$room->id}}">
                <div class="box-header">
                    <button class="btn btn-sm btn-flat btn-primary pull-right" type="submit">Lưu</button>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-sm-12 form-group col-md-offset-1 col-md-4">
                    <label for="" class="control-label text-center">Các máy đang sử dụng</label>
                        <select class="form-control" id="selUseMachines" name="usemachines[]" size="10" multiple>
                        @foreach($usedOwnRoomMachines as $machine)    
                        <option value="{{$machine->id}}">
                            {{$machine->machinecode}} {{!$machine->status ? '(Không hoạt động)':''}}
                        </option>
                        @endforeach     
                        </select>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <br><br><br>    
                        <button id="btnLeft" type="button" class="btn-success btn btn-flat col-md-12">
                            <i class="fa fa-angle-double-left"></i> 
                        </button>
                        &nbsp;&nbsp;
                        <button id="btnRight" type="button" class="btn btn-danger btn-flat col-md-12">
                            <i class="fa fa-angle-double-right"></i> 
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-4 form-group">
                        <label for="" class="control-label text-center">Các máy tiềm năng</label>
                        <select id="selPotentialMachines" class="form-control" size="10" multiple>
                            @foreach($potentialMachines as $machine)    
                            <option value="{{$machine->id}}">
                                {{$machine->machinecode}} {{ !$machine->status ? '(Không hoạt động)':''}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-sm btn-flat btn-primary pull-right" type="submit">Lưu</button>        
                <div class="clearfix"></div>               
            </form>
        </div>
    </div>

    </div>
    <!-- box body -->
</div>
@endif

@stop

@section('scripts')

    <script type="text/javascript" src="{{ asset('js/pages/room/assign-machine.js') }}"></script>
	
@stop

