@extends('layouts.master')

@section('title', 'Thêm thành viên')

@section ('content')

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm thành viên</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.user.store')}}" id="form-department" method="POST" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-header col-md-12">
                    Thông tin tài khoản
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Tên đăng nhập: 
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-3">
                        <input type="text" name="username" class="form-control" value="{{old('username')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Mật khẩu: 
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input type="password" name="password" class="form-control" value="{{old('password')}}">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" id="btn-eye">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Trạng thái:
                    </label>
                    <div class="col-md-3">
                        <select name="status" class="form-control">
                            <option {{old('status') === '1' ? 'selected' : '' }} value="1">Kích hoạt</option>
                            <option {{old('status') === '0' ? 'selected' : '' }} value="0">Tạm ngưng</option>
                        </select>
                    </div>
                </div>

                <div class="form-header col-md-12">
                    Thông tin cá nhân
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Họ:
                        <span 
                        class="require-field">(*)</span>
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="lastname" class="form-control"  value="{{old('lastname')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Tên:
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="firstname" class="form-control"  value="{{old('firstname')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Mã thành viên:
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="idnumber" class="form-control"  value="{{old('idnumber')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Ngày sinh:
                    </label>
                    <div class="col-md-5">
                        <input type="text" name="birthday" class="form-control"  value="{{old('birthday')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Giới tính:
                    </label>
                    <div class="col-md-2">
                        <?php $currentGender = old('gender'); ?>
                        <select name="gender" class="form-control">
                            <option {{$currentGender === '1' ? 'selected' : '' }} value="1">Nam</option>
                            <option {{$currentGender === '0' ? 'selected' : '' }} value="0">Nữ</option>
                            <option {{$currentGender === '2' ? 'selected' : '' }} value="2">Khác</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Email:
                        <!-- <span class="require-field">(*)</span> -->
                    </label>
                    <div class="col-md-6">
                        <input type="text" name="email" class="form-control" value="{{old('email')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Địa chỉ:
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="address" class="form-control" value = "{{old('address')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Ảnh:
                    </label>
                    <div class="col-md-10">
                        <input type="file" name="avatar">
                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary">Lưu</button>
                    </div>
                </div>
        </form>
    </div>
    <!-- box body -->
</div>
@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/user/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('idnumber'))
				$('form#form-department').validate().showErrors({
				 	"idnumber": "{{$errors->first('idnumber')}}"
				});
          	@endif
            @if($errors->has('username'))
				$('form#form-department').validate().showErrors({
				 	"username": "{{$errors->first('username')}}"
				});
          	@endif
          	@if($errors->has('email'))
				$('form#form-department').validate().showErrors({
				 	"email": "{{$errors->first('email')}}"
				});
              @endif
           
		});

	</script>
@stop