@extends('layouts.master')
@section('title', 'Thành viên')
@section('content')

<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#">Thành viên</a></li>
    <!-- <li><a href="#tab_2" data-toggle="tab">Tab 2</a></li> -->
    <li><a href="{{route('admin.cohort.index')}}">Nhóm thành viên</a></li>
    
    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
    <!-- Danh sach thanh vien -->
      <div class="box-header">
        <h3 class="box-title">Danh sách thành viên</h3>

      </div>
      <!-- /.box-header -->
      <div class="box-header">
          <div class="search-form col-md-8 col-sm-12">
              <form action="" method="GET" class="form-inline" role="form">
                  <div class="form-group">
                      <input type="text" name="username" class="form-control" id="" placeholder="Tên đăng nhập" value="{{$username}}">
                  </div>
                  <div class="form-group">
                      <input type="text" name="fullname" class="form-control" id="" placeholder="Họ tên" value="{{$fullname}}">
                  </div>
                  <div class="form-group">
                      <select name="status" class="form-control">
                          <option value="-1">--- Tình trạng ---</option>
                          <option {{$status==1 ? 'selected' : ''}} value ="1"> Đang hoạt động</option>
                          <option {{$status==0 ? 'selected' : ''}}  value ="0"> Không hoạt động</option>
                      </select>
                  </div>
                  <button type="submit" class="btn btn-primary btn-flat">
                      <i class="fa fa-filter"></i> Lọc
                  </button>
              </form>                 
          </div> 
          <a href="{{route('admin.user.create')}}" class="btn btn-sm btn-info btn-flat pull-right"> 
              <i class="fa fa-plus"></i> Thêm thành viên
          </a>
      </div>
      <!-- /.box-footer -->

      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>#</th>
              <th>Tên đăng nhập</th>
              <th>Họ tên</th>
              <th>Mã thành viên</th>
              <th>Email</th>
              <th>Ngày sinh</th>
              <th>Trạng thái</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            <?php 
              $currentPage = $users->currentPage();
              $perPage = $users->perPage();
              //$currentPage=1;
              $index = $perPage*($currentPage-1)+1; 
            ?>
              @foreach($users as $user)
                <tr>
                  <td class="text-center">{{$index++}}</td>
                  <td>{{$user->username}} </td>
                  <td><a href=""> {{$user->getFullname() }}</a></td>
                  <td>{{$user->idnumber}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->birthday}}</td>
                  
                  <td class="text-center">
                    <span class="label {{$user->status ? 'label-success':'label-danger'}} ">
                      {{$user->status ? 'Hoạt động': 'Không hoạt động'}}
                    </span>
                  </td>
                  <td>
                    <a href="{{route('admin.user.edit', ['id'=>$user->id])}}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Sửa</a>
                    <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                      data-id = "{{$user->id}}" 
                      data-title = "{{$user->getFullname() }}"> 
                      <i class="fa fa-trash"></i> Xóa </span></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
      <div class="box-header clearfix">
        {{ $users->links() }}              
      </div>
      <!-- /.box-footer -->

      <!-- End Danh sach Thanh vien -->

        </div>
  </div>
  <!-- /.tab-content -->
</div>
<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<form method="post" action="{{route('admin.machine.delete')}}">
					{{csrf_field()}}
					
					<div class="modal-body">
						<input type="hidden" id="modal-delete-id" name="id">
						Bạn có chắc chắn xóa thành viên  <strong id="modal-delete-name"></strong> này cùng với các đăng ký liên quan ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="submit" class="btn btn-primary">Có</button>
					</div>
				</form>
			</div>
		</div>
	</div>	
<!-- End modal Delete  -->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/user/index.js')}}"></script>
@endsection