@extends('layouts.master')

@section('title', 'Sửa danh mục hoạt động')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Sửa danh mục hoạt động</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.activitycategory.update')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <input type="hidden" name="id" value="{{$activityCategory->id}}">
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mục cha:
                </label>
                <div class="col-md-8">
                    <select name="parent" id="" class="form-control">
                    <?php
                        $currentGroup = old('parent') ? old('parent') : $activityCategory->parent;
                    ?>
                        @foreach($parents as $key => $value)
                            <option {{$currentGroup == $key ? 'selected':''}} value="{{$key}}">{{$value}}</option>        
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mã danh mục hoạt động:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="activitycategorycode" value="{{old('activitycategorycode') ? old('activitycategorycode') : $activityCategory->activitycategorycode}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên danh mục hoạt động: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="name" class="form-control" value="{{old('name') ? old('name') : $activityCategory->name}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{old('note') ? old('note') : $activityCategory->note}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/activitycategory/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('activitycategorycode'))
				$('form#form-department').validate().showErrors({
				 	"activitycategorycode": "{{$errors->first('activitycategorycode')}}"
				});
          	@endif
          
		});

	</script>
@stop