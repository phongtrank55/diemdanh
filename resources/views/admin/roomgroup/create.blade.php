@extends('layouts.master')

@section('title', 'Thêm nhóm phòng')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm nhóm phòng</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.roomgroup.store')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mã nhóm phòng:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="roomgroupcode" value="{{old('roomgroupcode')}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên nhóm phòng: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="name" class="form-control" value="{{old('name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{old('note')}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/roomgroup/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('roomgroupcode'))
				$('form#form-department').validate().showErrors({
				 	"roomgroupcode": "{{$errors->first('roomgroupcode')}}"
				});
          	@endif
          
		});

	</script>
@stop