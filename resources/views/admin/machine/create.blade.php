@extends('layouts.master')

@section('title', 'Thêm máy điểm danh')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm máy điểm danh</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.machine.store')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <div class="form-group">
                <label class="control-label col-md-2">
                    Mã máy:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="machinecode" value="{{old('machinecode')}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Model: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="model" class="form-control" value="{{old('model')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên tệp: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="filename" class="form-control" value="{{old('filename')}}">
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tình trạng: 
                </label>
                <div class="col-md-3">
                    <select class="form-control" name="status" id="">
                        <option {{old('status')==1 ? 'selected':''}} value="1"> Đang hoạt động</option>
                        <option {{old('status')==0 ? 'selected':''}}  value="0"> Không hoạt động</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{old('note')}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/machine/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('machinecode'))
				$('form#form-department').validate().showErrors({
				 	"machinecode": "{{$errors->first('machinecode')}}"
				});
            @endif
            @if($errors->has('filename'))
				$('form#form-department').validate().showErrors({
				 	"filename": "{{$errors->first('filename')}}"
				});
          	@endif
          
		});

	</script>
@stop