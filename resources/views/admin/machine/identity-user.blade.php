@extends('layouts.master')

@section('title', 'Cập nhật mã định danh')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Cập nhật mã định danh trên máy {{$machine->machinecode}}</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
     <!-- /.box-header -->
     <div class="box-header">
          <div class="search-form col-md-8 col-sm-12">
              <form action="" method="GET" class="form-inline" role="form">
                  <div class="form-group">
                      <input type="text" name="idnumber" class="form-control" id="" placeholder="Mã thành viên" value="{{$idnumber}}">
                  </div>
                  <div class="form-group">
                      <input type="text" name="fullname" class="form-control" id="" placeholder="Họ tên" value="{{$fullname}}">
                  </div>
                  <button type="submit" class="btn btn-primary btn-flat">
                      <i class="fa fa-filter"></i> Lọc
                  </button>
              </form>                 
          </div> 
          <a class="btn btn-sm btn-primary pull-right" data-toggle="modal" href="#modal-add-identity">
              <i class="fa fa-plus"></i> Thêm mã định danh
          </a>
      </div>
      <!-- /.box-footer -->

      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>#</th>
              <th>Họ tên</th>
              <th>Mã thành viên</th>
              <th>Ngày sinh</th>
              <th>Mã định danh</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            <?php 
              $currentPage = $userMachines->currentPage();
              $perPage = $userMachines->perPage();
              $index = $perPage*($currentPage-1)+1; 
            ?>
              @foreach($userMachines as $userMachine)
                <tr>
                  <td class="text-center">{{$index++}}</td>
                  <td nowrap><a href=""> {{$userMachine->user->getFullname() }}</a></td>
                  <td>{{$userMachine->user->idnumber}}</td>
                  <td>{{$userMachine->user->birthday}}</td>
                  <td>{{$userMachine->usercode}}</td>                  
                  <td>
                  <a class="btn btn-sm btn-warning" data-toggle="modal" href="#modal-edit-identity"
                      data-id = "{{$userMachine->user->id}}" 
                      data-title = "{{$userMachine->user->getFullname(). ' - '.$userMachine->user->idnumber }}"
                      data-usercode = "{{$userMachine->usercode}}"> 
                      <i class="fa fa-pencil"></i> Sửa </span></a>
                    <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                      data-id = "{{$userMachine->id}}" 
                      data-title = "{{$userMachine->user->getFullname() }}"> 
                      <i class="fa fa-trash"></i> Xóa </span></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
    <!-- box body -->
    <div class="box-footer">
        {{ $userMachines->links() }}        
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="{{route('admin.machine.delete-identity-user')}}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="id">
                        Bạn có chắc chắn xóa mã định danh của <strong id="modal-delete-name"></strong> trên máy {{$machine->machinecode}} ?
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" id="btnDeleteRow" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End Modal Delete  -->


<!-- Modal edit identity -->
<div class="modal fade" id="modal-edit-identity">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cập nhật mã định danh</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="notification-identity">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Lỗi!</strong>  <span id="message">qAFĐ</span>
                    </div>
                    
            
                @csrf
                <input type="hidden" id="modal-id" value="" name="userid">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Thành viên:
                            </label>
                            <label style="text-align:left" class="control-label col-md-8" id="modal-edit-name">
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Mã định danh:
                            </label>
                            <div class="col-md-7">
                                <input type="text" name="usercode" id="modal-usercode" required class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-submit-identity">OK</button>    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End Modal edit identity  -->
<!-- Modal edit identity -->
<div class="modal fade" id="modal-add-identity">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Thêm mã định danh</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="notification-identity">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Lỗi!</strong>  <span id="message">qAFĐ</span>
                    </div>
                    
            
                @csrf
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Thành viên:
                            </label>
                            <div class="col-md-7">
                                <select name="userid" id="modal-id" class="form-control">

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Mã định danh:
                            </label>
                            <div class="col-md-7">
                                <input type="text" name="usercode" id="modal-usercode" required class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-submit-identity">OK</button>    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End Modal edit identity  -->


@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/pages/machine/identity-user.js') }}"></script>
    <script>
        var curUrl = "{{route('admin.machine.update-identity-user', ['id'=>$machine->id])}}";
    </script>
@stop

