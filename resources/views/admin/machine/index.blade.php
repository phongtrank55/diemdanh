@extends('layouts.master')
@section('title', 'Máy điểm danh')
@section('content')

<!-- Danh sach may dien danh -->
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Danh sách máy điểm danh</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-header">
                <div class="search-form col-md-6">
                    <form action="" method="GET" class="form-inline" role="form">
                        <div class="form-group">
                            <input type="text" name="keyword" class="form-control" id="" placeholder="Mã máy hoặc model" value="{{$keyword}}">
                        </div>
                        <div class="form-group">
                            <select name="status" class="form-control">
                                <option value="-1">--- Tình trạng ---</option>
                                <option {{$status==1 ? 'selected' : ''}} value ="1"> Đang hoạt động</option>
                                <option {{$status==0 ? 'selected' : ''}}  value ="0"> Không hoạt động</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-flat">
                            <i class="fa fa-filter"></i> Lọc
                        </button>
                    </form>                 
                </div> 
                <a href="{{route('admin.machine.create')}}" class="btn btn-sm btn-info btn-flat pull-right"> 
                    <i class="fa fa-plus"></i> Thêm máy điểm danh
                </a>
            </div>
            <!-- /.box-footer -->
       
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Mã máy</th>
                    <th>Model</th>
                    <th>Tên tệp</th>
                    <th>Phòng</th>
                    <th>Tình trạng</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
					$currentPage = $machines->currentPage();
					$perPage = $machines->perPage();
					//$currentPage=1;
					$index = $perPage*($currentPage-1)+1; 
				?>
        @foreach($machines as $machine)
                  <tr>
                    <td class="text-center">{{$index++}}</td>
                    <td>{{$machine->machinecode}}</td>
                    <td>{{$machine->model}}</td>
                    <td>{{$machine->filename}}</td>
                    <?php
                      $textRooms = [];
                      foreach($machine->rooms as $roomMachine)
                        $textRooms[] = $roomMachine->room->roomcode;
                      $textRooms = implode(', ', $textRooms);
                    ?>
                    <td>{{$textRooms}}</td>
                    <td class="text-center">
                      <span class="label {{$machine->status ? 'label-success':'label-danger'}} ">
                        {{$machine->status ? 'Hoạt động': 'Không hoạt động'}}
                      </span>
                    </td>
                    <td class="text-center">
                    <a href="{{route('admin.machine.edit', ['id'=>$machine->id])}}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Sửa</a>
                    <a href="{{route('admin.machine.update-identity-user', ['id'=>$machine->id])}}" class="btn btn-sm btn-primary"> <i class="fa fa-user-plus"></i> Cập nhật mã định danh</a>
                      <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                        data-id = "{{$machine->id}}" 
                        data-title = "{{$machine->machinecode}}"> 
                <i class="fa fa-trash"></i> Xóa </span></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-header clearfix">
              {{ $machines->links() }}            
            </div>
            <!-- /.box-footer -->
       
          </div>
<!-- End Danh sach may diem danh -->

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<form method="post" action="{{route('admin.machine.delete')}}">
					{{csrf_field()}}
					
					<div class="modal-body">
						<input type="hidden" id="modal-delete-id" name="id">
						Bạn có chắc chắn xóa máy này cùng với các đăng ký các phòng của máy  <strong id="modal-delete-name"></strong> ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="submit" class="btn btn-primary">Có</button>
					</div>
				</form>
			</div>
		</div>
	</div>	
<!-- End modal Delete  -->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/machine/index.js')}}"></script>
@endsection