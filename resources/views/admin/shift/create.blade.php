@extends('layouts.master')

@section('title', 'Thêm ca làm việc')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm ca làm việc</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <form action="{{route('admin.shift.store')}}" id="form-department" method="POST" class="form-horizontal" >
        <div class="box-body">
            @csrf
            <div class="form-header col-md-12">
                    Thông tin ca làm việc
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="name" value="{{old('name')}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <div class="checkbox">
                        <label>
                            <input value="1" name="season" checked type="checkbox"> Làm việc theo mùa
                        </label>
                    </div>
                </div>
            </div>
            <div id="calBySeason">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Ngày bắt đầu mùa hè: 
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-5">
                        <input placeholder="Định dạng DD/MM" type="text" name="datestartsummer"  class="form-control" value="{{old('datestartsummer')}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Ngày bắt đầu mùa đông: 
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-5">
                        <input placeholder="Định dạng DD/MM" type="text" name="datestartwinter" class="form-control" value="{{old('datestartwinter')}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{old('note')}}" class="form-control">
                </div>
            </div>
            <div class="form-header col-md-12">
                Chi tiết ca làm việc
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table no-margin text-center" id="detail-table">
                        <thead>
                            <tr class="time-winter">
                                <th></th>
                                <th></th>
                                <th colspan="2" id="title-summer" >Thời gian mùa hè</th>
                                <th colspan="2" id="title-winter">Thời gian mùa đông</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>Tên</th>
                                <th>Giờ vào</th>
                                <th>Giờ ra</th>
                                <th class="time-winter">Giờ vào</th>
                                <th class="time-winter">Giờ ra</th>
                                <th ></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class ="form-inline last-row">
                                <td>
                                    <button type="button" id="btnAddRow" class="btn btn-sm btn-primary">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </td>
                                <td>
                                    <input style="width: 9em;" type="text"  name="name_detail[]" class="form-control text-center" id=""> 
                                </td>
                                <td>
                                    <input type="number" min="0" max="23" name="hour_summer_start[]" class="form-control text-center" id=""> 
                                    <strong>:</strong>
                                    <input type="number" min="0" max="59" name="minute_summer_start[]" class="form-control text-center" id="">
                                </td>
                                <td>
                                    <input type="number" min="0" max="23" name="hour_summer_end[]" class="form-control text-center" id=""> 
                                    <strong>:</strong>
                                    <input type="number" min="0" max="59" name="minute_summer_end[]" class="form-control text-center" id="">
                                </td>
                                <td class="time-winter">
                                    <input type="number" min="0" max="23" name="hour_winter_start[]" class="form-control text-center" id=""> 
                                    <strong>:</strong>
                                    <input type="number" min="0" max="59" name="minute_winter_start[]" class="form-control text-center" id="">
                                </td>
                                <td class="time-winter">
                                    <input type="number" min="0" max="23" name="hour_winter_end[]" class="form-control text-center" id=""> 
                                    <strong>:</strong>
                                    <input type="number" min="0" max="59" name="minute_winter_end[]" class="form-control text-center" id="">
                                </td>
                                <td>
                                    <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        
        </div>
        <!-- box body -->
        <div class="box-footer"> 
            <div class="form-group">
                <div class="col-md-12">
                    <button type="submit" class="pull-right col-md-2 btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
                    
					<div class="modal-body">
                        Bạn có chắc chắn xóa thời gian này ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="button" id="btnDeleteRow" data-dismiss="modal" class="btn btn-primary">Có</button>
					</div>
			</div>
		</div>
	</div>	
<!-- End modal Delete  -->
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/pages/shift/create.css')}}">
@stop

@section('scripts')

	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/shift/create.js') }}"></script>
	<!-- <script type="text/javascript">
		$(function(){
			@if($errors->has('machinecode'))
				$('form#form-department').validate().showErrors({
				 	"machinecode": "{{$errors->first('machinecode')}}"
				});
          	@endif
          
		});

	</script> -->
@stop