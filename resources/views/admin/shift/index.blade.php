@extends('layouts.master')
@section('title', 'Ca làm việc')
@section('content')


@isset($curShift)
<!-- chi tiet ca lam viec -->
<div class="box box-info" id ="table-detail">
    <div class="box-header with-border">
      <h3 class="box-title">Chi tiết ca làm việc {{$curShift->name}}</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-header">
        <a href="{{route('admin.shift.edit', ['id'=>$curShift->id])}}" class="btn btn-sm btn-info btn-flat pull-right"> 
            <i class="fa fa-pencil"></i> Chỉnh sửa
        </a>
    </div>
    @if($curShift->season == Constraint::CALCULATE_BY_SEASON)
    <div class="box-header">
        <p> Ngày bất đầu mùa hè: <strong>{{$curShift->datestartsummer}}</strong></p>
        <p> Ngày bất đầu mùa đông: <strong>{{$curShift->datestartwinter}}</strong></p>
    </div>
    <!-- /.box-footer -->
    @endif

    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin text-center">
        <thead>
        @if($curShift->season == Constraint::CALCULATE_BY_SEASON)
        <tr class="time-winter">
              <th></th>
              <th colspan="2" id="title-summer" >Thời gian mùa hè</th>
              <th colspan="2" id="title-winter">Thời gian mùa đông</th>
          </tr>
        @endif
          <tr>
              <th>Tên</th>
              <th>Giờ vào</th>
              <th>Giờ ra</th>
              @if($curShift->season == Constraint::CALCULATE_BY_SEASON)
              <th class="time-winter">Giờ vào</th>
              <th class="time-winter">Giờ ra</th>
              @endif
          </tr>
      </thead> 
        <tbody>
          @foreach($shiftDetails as $detail)
          <tr>
              <td>{{$detail->name_detail}}</td>
              <td>{{$detail->time_summer_start}}</td>
              <td>{{$detail->time_summer_end}}</td>
              @if($curShift->season == Constraint::CALCULATE_BY_SEASON)
              <td class="time-winter">{{$detail->time_winter_start}}</td>
              <td class="time-winter">{{$detail->time_winter_end}}</td>
              @endif
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
</div>
<!-- end chi tiet ca lam viec -->
@endisset

<!-- Danh sach ca lam viec -->
<div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Danh sách ca làm việc</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-header">
        <a href="{{route('admin.shift.create')}}" class="btn btn-sm btn-info btn-flat pull-right"> 
            <i class="fa fa-plus"></i> Thêm ca làm việc
        </a>
    </div>
    <!-- /.box-footer -->

    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
          <tr>
            <th>#</th>
            <th>Tên ca làm việc</th>
            <th>Thời gian phân mùa</th>
            <th>Ghi chú</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          <?php 
              $currentPage = $shifts->currentPage();
              $perPage = $shifts->perPage();
              //$currentPage=1;
              $index = $perPage*($currentPage-1)+1; 
            ?>
          @foreach($shifts as $shift)
          <tr>
            <td class="text-center">{{$index++}}</td>
            <td>
            <?php $isCurrent = isset($curShift) && $curShift->id == $shift->id; ?>
              <a id="{{$isCurrent ? 'cur-ref': ''}}" href="{{$isCurrent ? '#table-detail' : route('admin.shift.index', ['id'=>$shift->id])}}">
                {{$shift->name}}
              </a>
            </td>
            <td>
            @if($shift->season == Constraint::CALCULATE_BY_SEASON)
              <p>Mùa hè: <strong>{{$shift->datestartsummer}}</strong></p>
              <p>Mùa đông: <strong>{{$shift->datestartwinter}}</strong></p>
            @else
              Không tính theo mùa
            @endif
            </td>
            <td>{{$shift->note}}</td>
            <td>
              <a href="{{route('admin.shift.edit', ['id'=>$shift->id])}}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Sửa</a>
              <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                data-id = "{{$shift->id}}" 
                data-title = "{{$shift->name}}"> 
                <i class="fa fa-trash"></i> Xóa </span></a>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-header clearfix">
      {{ $shifts->links() }}            
    </div>
    <!-- /.box-footer -->

</div>
<!-- End Danh sach ca lam viec -->
<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<form method="post" action="{{route('admin.shift.delete')}}">
					{{csrf_field()}}
					
					<div class="modal-body">
						<input type="hidden" id="modal-delete-id" name="id">
						Bạn có chắc chắn xóa ca làm việc này <strong id="modal-delete-name"></strong>  ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="submit" class="btn btn-primary">Có</button>
					</div>
				</form>
			</div>
		</div>
  </div>	
<!-- End modal Delete  -->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/shift/index.js')}}"></script>
@endsection