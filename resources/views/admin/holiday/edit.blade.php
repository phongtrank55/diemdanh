@extends('layouts.master')

@section('title', 'Sửa ngày lễ')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Sửa ngày lễ</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.holiday.update')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <input type="hidden" name="id" value="{{$holiday->id}}">
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên đầy đủ:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="fullname" value="{{$errors->any() ? old('fullname') : $holiday->fullname}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Tên viết tắt:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-2">
                    <input type="text" name="shortname" value="{{$errors->any() ? old('shortname') : $holiday->shortname}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-2">
                    Loại: 
                </label>
                <div class="col-md-3">
                    <select class="form-control" name="type" id="type-holiday">
                        <?php $type = $errors->any() ? old('type') : $holiday->type; ?>
                        <option {{$type==Constraint::HOLIDAY_TYPE_SPECIFIC_DAY ? 'selected':''}} value="{{Constraint::HOLIDAY_TYPE_SPECIFIC_DAY}}">Ngày cụ thể</option>
                        <option {{$type==Constraint::HOLIDAY_TYPE_SOLAR ? 'selected':''}} value="{{Constraint::HOLIDAY_TYPE_SOLAR}}">Dương lịch hằng năm</option>
                        <!-- <option {{$type==Constraint::HOLIDAY_TYPE_LUNAR ? 'selected':''}} value="{{Constraint::HOLIDAY_TYPE_LUNAR}}">Âm lịch hàng năm</option> -->
                    </select>
                </div>
            </div>
            <?php
                $fullDate = $type==Constraint::HOLIDAY_TYPE_SPECIFIC_DAY;
                $startDate = $fullDate ? date('d/m/Y', $holiday->startdate) : $holiday->dayeveryyear.'/'.$holiday->montheveryyear;
            ?>
            <div class="input-daterange">
                <div class="form-group">
                    <label class="control-label col-md-2">
                        Ngày bắt đầu:
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="startdate" class="form-control pull-right" id="startdate" value="{{$errors->any() ? old('startdate') : $startDate}}">
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="div-enddate">
                    <label class="control-label col-md-2">
                        Ngày kết thúc:
                        <span class="require-field">(*)</span>
                    </label>
                    <div class="col-md-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="enddate" class="form-control pull-right" id="enddate" value="{{$errors->any() ? old('enddate') : date('d/m/Y', $holiday->enddate)}}">
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="form-group">
                <label class="control-label col-md-2">
                    Ghi chú:
                </label>
                <div class="col-md-10">
                    <input type="text" name="note" value = "{{$errors->any() ? old('note') : $holiday->note}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.min.css') }}">

@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.vi.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-timepicker.min.js') }}"></script>
    
	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/holiday/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			@if($errors->has('shortname'))
				$('form#form-department').validate().showErrors({
				 	"shortname": "{{$errors->first('shortname')}}"
                });
                
              @endif
              @if($errors->has('start'))
				$('form#form-department').validate().showErrors({
				 	"start": "{{$errors->first('start')}}"
                });
                
              @endif
              @if($errors->has('enddate'))
				$('form#form-department').validate().showErrors({
				 	"enddate": "{{$errors->first('enddate')}}"
                });
                
              @endif
              
            @if(!$errors->any())
                // $('#startdate').datepicker('setDate', new Date());
            @endif
          
		});

	</script>
@stop