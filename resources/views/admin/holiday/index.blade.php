@extends('layouts.master')
@section('title', 'Nghỉ lễ')
@section('content')

<!-- Danh sach may dien danh -->
<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Danh sách ngày nghỉ lễ</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-header">
                <div class="pull-right">
                    <a href="{{route('admin.holiday.create')}}" class="btn btn-sm btn-primary">
                        <i class="fa fa-plus"></i> Thêm ngày lễ
                    </a>
                </div>
            </div>
            <!-- /.box-footer -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Tên đầy đủ</th>
                    <th>Tên viết tắt</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Loại</th>
                    <th>Ghi chú</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
					$currentPage = $holidays->currentPage();
					$perPage = $holidays->perPage();
					//$currentPage=1;
					$index = $perPage*($currentPage-1)+1; 
				?>
                @foreach($holidays as $holiday)
                  <tr>
                    <td class="text-center">{{$index++}}</td>
                    <td>{{$holiday->fullname}}</td>
                    <td>{{$holiday->shortname}}</td>
                    @if($holiday->type == Constraint::HOLIDAY_TYPE_SOLAR)
                    <td colspan="2">
                      {{$holiday->dayeveryyear}}/{{$holiday->montheveryyear}}
                    </td> 
                    <td>
                        <span class="label label-success">
                          Dương lịch hàng năm
                        </span>
                    </td>
                    @elseif($holiday->type == Constraint::HOLIDAY_TYPE_SPECIFIC_DAY)
                    <td>{{date('d/m/Y', $holiday->startdate)}}</td> 
                    <td>{{date('d/m/Y', $holiday->enddate)}}</td> 
                    <td>
                        <span class="label label-primary">
                          Ngày cụ thể
                        </span>
                    </td>
                    @endif
                    <td>{{$holiday->note}}</td>
                    
                    <td>
                      <a href="{{route('admin.holiday.edit', ['id'=>$holiday->id])}}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Sửa</a>
                      <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                        data-id = "{{$holiday->id}}" 
                        data-title = "{{$holiday->fullname}}"> 
                <i class="fa fa-trash"></i> Xóa </span></a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-header clearfix">
              {{ $holidays->links() }}            
            </div>
            <!-- /.box-footer -->
       
          </div>
<!-- End Danh sach may diem danh -->

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<form method="post" action="{{route('admin.holiday.delete')}}">
					{{csrf_field()}}
					
					<div class="modal-body">
						<input type="hidden" id="modal-delete-id" name="id">
						Bạn có chắc chắn xóa ngày lễ  <strong id="modal-delete-name"></strong> ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="submit" class="btn btn-primary">Có</button>
					</div>
				</form>
			</div>
		</div>
	</div>	
<!-- End modal Delete  -->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/holiday/index.js')}}"></script>
@endsection