@extends('layouts.master')
@section('title', 'Thiết lập hệ thống')

@section('content')

<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="{{$section == Constraint::CONFIG_SECTION_ACTIVITY ? 'active' : ''}}">
      <a href="{{$section != Constraint::CONFIG_SECTION_ACTIVITY ? route('admin.config.index', ['section' => Constraint::CONFIG_SECTION_ACTIVITY]) : '#'}}">
        Hoạt động
      </a>
    </li>
    <li class="{{$section == Constraint::CONFIG_SECTION_MACHINE ? 'active' : ''}}">
      <a href="{{$section != Constraint::CONFIG_SECTION_MACHINE ? route('admin.config.index', ['section' => Constraint::CONFIG_SECTION_MACHINE]) : '#'}}">
        Máy điểm danh
      </a>
    </li>
    <li class="{{$section == Constraint::CONFIG_SECTION_ROLE ? 'active' : ''}}">
      <a href="{{route('admin.config.index', ['section' => Constraint::CONFIG_SECTION_ROLE])}}">
        Phân quyền
      </a>
    </li>
    <li class="{{$section == Constraint::CONFIG_SECTION_SYSTEM ? 'active' : ''}}">
      <a href="{{$section != Constraint::CONFIG_SECTION_SYSTEM ? route('admin.config.index', ['section' => Constraint::CONFIG_SECTION_SYSTEM]) : '#'}}">
        Hệ thống
      </a>
    </li>
    <li class="{{$section == Constraint::CONFIG_SECTION_EMAIL ? 'active' : ''}}">
      <a href="{{$section != Constraint::CONFIG_SECTION_EMAIL ? route('admin.config.index', ['section' => Constraint::CONFIG_SECTION_EMAIL]) : '#'}}">
        Email
      </a>
    </li>
    
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">

      
         <div class="box-body">
        @if(session()->has('messageConfig'))
          <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Thông báo!</strong> {{session()->get('messageConfig')}}
          </div>
        @endif                
        @yield('sub-content')
    
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /. box-footer -->
      </form>
    </div>

  </div>
  <!-- /.tab-content -->
</div>

@endsection
@section('scripts')
  @yield('sub-scripts')
@endsection