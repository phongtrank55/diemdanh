@extends('admin.config.index')
@section('sub-content')
<div class="box-header">
    <div class="form-header col-md-12">
        Thêm vai trò
    </div>
</div>

<form action="{{route('admin.config.store-role')}}" method="POST" class="form-horizontal" role="form" id="form-department">
    @csrf
    <div class="box-body">
        <div class="form-group">
            <label for="" class="control-label col-md-3">Tên vai trò:<span class="require-field">(*)</span></label>
            <div class="col-md-4">
                <input type="text" name="name" value="{{old('name')}}" id="" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3">Mã vai trò:<span class="require-field">(*)</span></label>
            <div class="col-md-3">
                <input type="text" name="rolecode" value="{{old('rolecode')}}" id="" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3">Mô tả:</label>
            <div class="col-md-8">
                <input type="text" name="description" value="{{old('description')}}" id="" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3">Các bối cảnh được bổ nhiệm:</label>
            <div class="col-md-8">
                <div class="checkbox">
                    <label for="">
                        <input type="checkbox" name="contextlevel10" value="1" {{old('contextlevel10') ? 'checked' : '' }} id="">
                        Hệ thống
                    </label> <br>
                    <label for="">
                        <input type="checkbox" name="contextlevel100" value="1" {{old('contextlevel100') ? 'checked' : '' }} id="">
                        Hoạt động
                    </label>
                </div>
                
            </div>
        </div>
        <div class="form-group">
            <label for="" class="control-label col-md-3">Cho phép bổ nhiệm vai trò:</label>
            <div class="col-md-4">
                <select name="allowassign[]" id="" class="form-control" multiple="multiple">
                    @foreach($roles as $role)
                    <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
       <label for="" class="control-label col-md-3">Các quyền:</label>
    </div>
    <div class="table-responsive col-md-10 col-md-offset-1">
        <table class="table no-margin">
            <thead>
                <tr>
                    <th>Quyền</th>
                    <th>Cho phép</th>
                </tr>
            </thead>
            <tbody>
            @foreach($capabilities as $capability)
            <tr>
                <td>
                   <strong>{{$capability->name}}</strong> 
                   <br>
                   <span class="text-muted">{{$capability->capabilitycode}}</span>
                </td>
                <td class="text-center">
                    <input type="checkbox" name="capabilties[]" value="{{$capability->capabilitycode}}" id="">
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- Table các quyền -->

    <!-- End table các quyền -->
    <!-- /.box-body -->
    
    <div class="clearfix">
    
    </div>
    
    <div class="box-footer">
        <div class="form-group">
                <button type="submit" class="btn btn-primary pull-right col-md-2">Lưu</button>
        </div>
    </div>
</form>
@stop
@section('sub-scripts')
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script src="{{asset('js/pages/role/create.js')}}"></script>
    <script>
        $(function(){
            @if($errors->has('rolecode'))
				$('form#form-department').validate().showErrors({
				 	"rolecode": "{{$errors->first('rolecode')}}"
				});
            @endif
        });
    </script>
@stop