@extends('admin.config.index')
@section('sub-content')
<div class="form-header col-md-12">
    Danh sách các vai trò
</div>
<div class="box-header">
    <div class="pull-right">
        <a href="{{route('admin.config.create-role')}}" class="btn btn-sm btn-primary">
            <i class="fa fa-plus"></i> Thêm vai trò
        </a>
    </div>
</div>
<!-- /.box-footer -->
<div class="box-body">
    <div class="table-responsive">
    <table class="table no-margin">
        <thead>
        <tr>
        <th>#</th>
        <th>Tên vai trò</th>
        <th>Mã vai trò</th>
        <th>Mô tả</th>
        
        <th></th>
        </tr>
        </thead>
        <tbody>
        <?php 
        $currentPage = $roles->currentPage();
        $perPage = $roles->perPage();
        //$currentPage=1;
        $index = $perPage*($currentPage-1)+1; 
    ?>
    @foreach($roles as $role)
        <tr>
        <td class="text-center">{{$index++}}</td>
        <td>{{$role->name}}</td>
        <td class="text-center">{{$role->rolecode}}</td>
        <td>{{$role->description}}</td>
        
        <td>
            <a href="{{route('admin.config.edit-role', ['id'=>$role->id])}}" class="btn btn-sm btn-warning"> <i class="fa fa-pencil"></i> Sửa</a>
            <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                data-id = "{{$role->id}}" 
                data-title = "{{$role->name}}"> 
                    <i class="fa fa-trash"></i> Xóa </span>
            </a>
        </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    <!-- /.table-responsive -->
</div>
<!-- /.box-body -->
<div class="box-header clearfix">
    {{ $roles->links() }}            
</div>
<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Cảnh báo</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<form method="post" action="{{route('admin.config.delete-role')}}">
					{{csrf_field()}}
					
					<div class="modal-body">
						<input type="hidden" id="modal-delete-id" name="id">
						Bạn có chắc chắn xóa vai trò <strong id="modal-delete-name"></strong> ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
						<button type="submit" class="btn btn-primary">Có</button>
					</div>
				</form>
			</div>
		</div>
	</div>	
<!-- End modal Delete  -->
@stop
@section('sub-scripts')
    <script src="{{asset('js/pages/role/index.js')}}"></script>
@stop