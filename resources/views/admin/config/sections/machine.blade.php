@extends('admin.config.index')
@section('sub-content')
<form action="{{route('admin.config.update', compact('section'))}}" id="form-department" method="POST" class="form-horizontal"  enctype="multipart/form-data">
        @csrf
     
<div class="form-header col-md-12">
    Máy điểm danh
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Số lượng phòng tối đa có thể gán cho một máy:
        <br>
        <span class="text-muted">machine | max_room_per_machine</span>
    </label>
    <div class="col-md-2">
        <?php $curCheck = $errors->any() ? old('max_room_per_machine') : $configs['max_room_per_machine']; ?>
        <select name="max_room_per_machine" class="form-control" id="">
            <option {{$curCheck == 0 ? 'selected': ''}} value="0">Không giới hạn</option>
            @for($i=1; $i<=10; $i++)
            <option {{$curCheck == $i ? 'selected':''}} value="{{$i}}">{{$i}}</option>
            @endfor
        </select>
    </div>
</div>
<div class="form-group">
        <div class="col-sm-10 col-sm-offset-4">
            <button type="submit" class="btn btn-primary">Lưu</button>
        </div>
        </div>

</form>
@stop
