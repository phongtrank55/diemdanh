@extends('admin.config.index')
@section('sub-content')
<form action="{{route('admin.config.update', compact('section'))}}" id="form-department" method="POST" class="form-horizontal"  enctype="multipart/form-data">
        @csrf
     
<div class="form-header col-md-12">
    Tham số mặc định đầu vào
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Cho phép điểm danh vào:
        <br>
        <span class="text-muted">activity | enable_in_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_in_default') : $configs['enable_in_default']; ?>
                <input type="hidden" name="enable_in_default" value="0"/>
                <input type="checkbox" name="enable_in_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: có  </span>
            </label>
        </div>
  </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Số phút trước khi điểm danh vào:
        <br>
        <span class="text-muted">activity | minute_before_in_default</span>
    </label>
    <div class="col-md-8">
    <div class="form-inline">
        <input style="width:80px" min="0" max="20" type="number" name="minute_before_in_default" class="form-control text-center" value="{{$errors->any() ? old('minute_before_in_default') : $configs['minute_before_in_default']}}">
        <small>Số phút cho phép điểm danh là "vào" trước khi tới giờ làm việc</small>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Số phút sau khi điểm danh vào:
        <br>
        <span class="text-muted">activity | minute_before_in_default</span>
    </label>
    <div class="col-md-8">
    <div class="form-inline">
        <input style="width:80px" min="0" max="20" type="number" name="minute_after_in_default" class="form-control text-center" value="{{$errors->any() ? old('minute_after_in_default') : $configs['minute_after_in_default']}}">
        <small>Số phút cho phép điểm danh là "vào" sau khi tới giờ làm việc</small>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
            Điểm danh vào giữa các ca:
        <br>
        <span class="text-muted">activity | enable_attendance_in_between_shift_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_attendance_in_between_shift_default') : $configs['enable_attendance_in_between_shift_default']; ?>
                <input type="hidden" name="enable_attendance_in_between_shift_default" value="0"/>
                <input type="checkbox" name="enable_attendance_in_between_shift_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: không  </span>
            </label>
        </div>
  </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Cho phép điểm danh ra:
        <br>
        <span class="text-muted">activity | enable_out_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_out_default') : $configs['enable_out_default']; ?>
                <input type="hidden" name="enable_out_default" value="0"/>
                <input type="checkbox" name="enable_out_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: có  </span>
            </label>
        </div>
  </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">
        Số phút trước khi điểm danh ra:
        <br>
        <span class="text-muted">activity | minute_before_out_default</span>
    </label>
    <div class="col-md-8">
    <div class="form-inline">
        <input style="width:80px" min="0" max="20" type="number" name="minute_before_out_default" class="form-control text-center" value="{{$errors->any() ? old('minute_before_out_default') : $configs['minute_before_out_default']}}">
        <small>Số phút cho phép điểm danh là "ra" trước khi hết giờ làm việc</small>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Số phút sau khi điểm danh ra:
        <br>
        <span class="text-muted">activity | minute_after_out_default</span>
    </label>
    <div class="col-md-8">
    <div class="form-inline">
        <input style="width:80px" min="0" max="20" type="number" name="minute_after_out_default" class="form-control text-center" value="{{$errors->any() ? old('minute_after_out_default') : $configs['minute_after_out_default']}}">
        <small>Số phút cho phép điểm danh là "ra" sau khi hết giờ làm việc</small>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
            Điểm danh ra giữa các ca:
        <br>
        <span class="text-muted">activity | enable_attendance_out_between_shift_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_attendance_out_between_shift_default') : $configs['enable_attendance_out_between_shift_default']; ?>
                <input type="hidden" name="enable_attendance_out_between_shift_default" value="0"/>
                <input type="checkbox" name="enable_attendance_out_between_shift_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: không  </span>
            </label>
        </div>
  </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">
        Cho phép đi muộn:
        <br>
        <span class="text-muted">activity | enable_late_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_late_default') : $configs['enable_late_default']; ?>
                <input type="hidden" name="enable_late_default" value="0"/>
                <input type="checkbox" name="enable_late_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: có  </span>
            </label>
        </div>
  </div>
</div>

<div class="form-group">
    <label class="control-label col-md-4">
        Số phút cho phép đi muộn:
        <br>
        <span class="text-muted">activity | minute_late_default</span>
    </label>
    <div class="col-md-8">
    <div class="form-inline">
        <input style="width:80px" min="0" type="number" name="minute_late_default" class="form-control text-center" value="{{$errors->any() ? old('minute_late_default') : $configs['minute_late_default']}}">
        <small>Sau khoảng thời gian này tính từ thời điểm vào sẽ tính là "muộn"</small>
        </div>
    </div>
</div>
<div class="form-header col-md-12">
    Đăng ký hoạt động
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Cho phép tự đăng ký:
        <br>
        <span class="text-muted">activity | enable_self_enrol_default</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <label>
                <?php $curCheck = $errors->any() ? old('enable_self_enrol_default') : $configs['enable_self_enrol_default']; ?>
                <input type="hidden" name="enable_self_enrol_default" value="0"/>
                <input type="checkbox" name="enable_self_enrol_default" value="1" {{$curCheck ? 'checked' :''}} />
                <span class="text-muted">Mặc định: không  </span>
            </label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Vai trò được bổ nhiệm khi tự đăng ký:
        <br>
        <span class="text-muted">activity | role_self_enrol</span>
    </label>
    <div class="col-md-3">
        <?php $curCheck = $errors->any() ? old('role_self_enrol') : $configs['role_self_enrol']; ?>
        <select name="role_self_enrol" class="form-control" id="">
            @foreach($roles as $role)
            <option {{$curCheck == $role->id ? 'selected':''}} value="{{$role->id}}">{{$role->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-header col-md-12">
    Vai trò được xét điểm danh
</div>
<div class="form-group">
    <label class="control-label col-md-4">
        Vai trò được xét điểm danh:
        <br>
        <span class="text-muted">activity | attendance_role</span>
    </label>
    <div class="col-md-8">
        <div class="checkbox">
            <?php $currentRoles = explode(Constraint::CONFIG_DELIMITER, $configs['attendance_role']); ?>
            @foreach($roles as $role)
            <label for="">
                <input type="checkbox" name="attendance_role[]" value="{{$role->id}}" {{ in_array($role->id, $currentRoles) ? 'checked' : '' }} id="">
                {{ $role->name }}
            </label> <br>
            @endforeach
        </div>        
    </div>

</div>

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-4">
            <button type="submit" class="btn btn-primary">Lưu</button>
        </div>
        </div>
    </form>
@stop
@section('sub-scripts')
    <script src="{{asset('js/pages/config/activity.js')}}"></script>
@stop