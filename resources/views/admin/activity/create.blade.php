@extends('layouts.master')

@section('title', 'Thêm hoạt động')

@section ('content')

@if(empty($activityCategories))

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý!</strong> Chưa có danh mục hoạt động nên không thể thêm hoạt động!
</div>
@elseif(!$hasRoom)

<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý!</strong> Chưa có phòng nên không thể thêm hoạt động!
</div>

@else

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm hoạt động</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
    <div class="box-body">
        <form action="{{route('admin.activity.store')}}" id="form-department" method="POST" class="form-horizontal" >
            @csrf
            <!-- Thông tin chung -->
            <div class="form-header col-md-12">
                Thông tin chung
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Mã hoạt động:
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-3">
                    <input type="text" name="activitycode" value="{{old('activitycode')}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Tên hoạt động: 
                    <span class="require-field">(*)</span>
                </label>
                <div class="col-md-5">
                    <input type="text" name="name" class="form-control" value="{{old('name')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Danh mục hoạt động:
                </label>
                <div class="col-md-8">
                    <select name="activitycategoryid" class="form-control">
                        <?php
                            $currentGroup = isset($activityCategoryCurrent) ? $activityCategoryCurrent : old('activitycategoryid');
                        ?>
                        @foreach($activityCategories as $key => $value)
                            <option {{$currentGroup == $key ? 'selected':''}} value="{{$key}}">{{$value}}</option>        
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Hiển thi hoạt động:
                </label>
                <div class="col-md-2">
                    <select name="display" class="form-control">
                        <option {{old('display') == Constraint::ACTIVITY_DISPLAY_OPEN ? 'selected' : '' }} value="{{Constraint::ACTIVITY_DISPLAY_OPEN}}">Mở</option>
                        <option {{old('display') == Constraint::ACTIVITY_DISPLAY_CLOSE ? 'selected' : '' }} value="{{Constraint::ACTIVITY_DISPLAY_CLOSE}}">Đóng</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Ghi chú:
                </label>
                <div class="col-md-8">
                    <input type="text" name="note" value = "{{old('note')}}" class="form-control">
                </div>
            </div>
            <!-- End thông tin chung -->
            <!-- Lich hoat dong -->
            <div class="form-header col-md-12">
                Lịch hoạt động
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" >Tần suất:</label>
                <div class="col-md-3">
                    <select class="form-control" name="frequency" id="frequency">
                        @if(!empty($shifts))    
                        <option {{old('frequency') == Constraint::ACTIVITY_FREQUENCY_SOME_DAY ? 'selected' : '' }} value="{{Constraint::ACTIVITY_FREQUENCY_SOME_DAY}}">Vài buổi trong tuần</option>
                        <option {{old('frequency') == Constraint::ACTIVITY_FREQUENCY_EVERY_DAY ? 'selected' : '' }} value="{{Constraint::ACTIVITY_FREQUENCY_EVERY_DAY}}">Cả tuần</option>
                        @endif
                        <option {{old('frequency') == Constraint::ACTIVITY_FREQUENCY_FREE ? 'selected' : '' }} value="{{Constraint::ACTIVITY_FREQUENCY_FREE}}">Tự do</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4" >Loại ca làm việc:</label>
                <div class="col-md-4">
                    <select class="form-control" name="shiftid" id="shift">
                        @foreach($shifts as $shift)
                        <option {{old('shiftid') == $shift->id ? 'checked' : ''}} value="{{$shift->id}}">{{$shift->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
           <div class="input-daterange">
                <div class="form-group">
                    <label class="control-label col-md-4">
                        Ngày bắt đầu hoạt động:
                    </label>
                    <div class="col-md-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="start_date" value="{{old('start_date')}}"class="actual_range form-control pull-right" id="start_date" required>
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-md-4">
                        Ngày kết thúc hoạt động:
                    </label>
                    <div class="col-md-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="end_date" value="{{old('end_date')}}" class="actual_range form-control pull-right" id="end_date">
                        </div>
                    </div>
                    <div class="checkbox">
                        <label for="">
                            <input type="checkbox" {{old('enable_end_date') ? 'checked' :'' }} name="enable_end_date" value="1"> Mở
                        </label>
                    </div>
                </div>
            </div>
            <!-- Work calendar  -->
            <div id="work-calendar">
                <!-- load here -->
            </div>
            <!-- End Work calendar -->
 
            <!-- End lich hoat dong -->
            <!-- Tham số đầu vào -->
            <div class="form-header col-md-12">
                Tham số đầu vào
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    
                </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_in') : $configs['enable_in_default']; ?>
                            <input type="hidden" name="enable_in" value="0"/>
                            <input type="checkbox" name="enable_in" value="1" {{$curCheck ? 'checked' :''}} />
                            Cho phép điểm danh vào 
                        </label>
                    </div>
            </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Số phút trước khi điểm danh vào:
                </label>
                <div class="col-md-8">
                <div class="form-inline">
                    <input style="width:80px" min="0" max="20" type="number" name="minute_before_in" class="form-control text-center" value="{{$errors->any() ? old('minute_before_in') : $configs['minute_before_in_default']}}">
                    <small>Số phút cho phép điểm danh là "vào" trước khi tới giờ làm việc</small>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Số phút sau khi điểm danh vào:
               </label>
                <div class="col-md-8">
                <div class="form-inline">
                    <input style="width:80px" min="0" max="20" type="number" name="minute_after_in" class="form-control text-center" value="{{$errors->any() ? old('minute_after_in') : $configs['minute_after_in_default']}}">
                    <small>Số phút cho phép điểm danh là "vào" sau khi tới giờ làm việc</small>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                        
                </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_attendance_in_between_shift') : $configs['enable_attendance_in_between_shift_default']; ?>
                            <input type="hidden" name="enable_attendance_in_between_shift" value="0"/>
                            <input type="checkbox" name="enable_attendance_in_between_shift" value="1" {{$curCheck ? 'checked' :''}} />
                            Điểm danh vào giữa các ca
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    
               </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_out') : $configs['enable_out_default']; ?>
                            <input type="hidden" name="enable_out" value="0"/>
                            <input type="checkbox" name="enable_out" value="1" {{$curCheck ? 'checked' :''}} />
                            Cho phép điểm danh ra
                        </label>
                    </div>
            </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">
                    Số phút trước khi điểm danh ra:
                </label>
                <div class="col-md-8">
                <div class="form-inline">
                    <input style="width:80px" min="0" max="20" type="number" name="minute_before_out" class="form-control text-center" value="{{$errors->any() ? old('minute_before_out') : $configs['minute_before_out_default']}}">
                    <small>Số phút cho phép điểm danh là "ra" trước khi hết giờ làm việc</small>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Số phút sau khi điểm danh ra:
                </label>
                <div class="col-md-8">
                <div class="form-inline">
                    <input style="width:80px" min="0" max="20" type="number" name="minute_after_out" class="form-control text-center" value="{{$errors->any() ? old('minute_after_out') : $configs['minute_after_out_default']}}">
                    <small>Số phút cho phép điểm danh là "ra" sau khi hết giờ làm việc</small>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                        
                </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_attendance_out_between_shift') : $configs['enable_attendance_out_between_shift_default']; ?>
                            <input type="hidden" name="enable_attendance_out_between_shift" value="0"/>
                            <input type="checkbox" name="enable_attendance_out_between_shift" value="1" {{$curCheck ? 'checked' :''}} />
                            Điểm danh ra giữa các ca
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    
                </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_late') : $configs['enable_late_default']; ?>
                            <input type="hidden" name="enable_late" value="0"/>
                            <input type="checkbox" name="enable_late" value="1" {{$curCheck ? 'checked' :''}} />
                            Cho phép đi muộn
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-4">
                    Số phút cho phép đi muộn:
                </label>
                <div class="col-md-8">
                <div class="form-inline">
                    <input style="width:80px" min="0" type="number" name="minute_late" class="form-control text-center" value="{{$errors->any() ? old('minute_late') : $configs['minute_late_default']}}">
                    <small>Sau khoảng thời gian này tính từ thời điểm vào sẽ tính là "muộn"</small>
                    </div>
                </div>
            </div>
            <!-- End tham số đầu vào -->
            <!-- Đăng ký hoạt động -->
            <!-- Tham số đầu vào -->
            <div class="form-header col-md-12">
                Tự đăng ký
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    
                </label>
                <div class="col-md-8">
                    <div class="checkbox">
                        <label>
                            <?php $curCheck = $errors->any() ? old('enable_self_enrol') : $configs['enable_self_enrol_default']; ?>
                            <input type="hidden" name="enable_self_enrol" value="0"/>
                            <input type="checkbox" name="enable_self_enrol" value="1" {{$curCheck ? 'checked' :''}} />
                            Cho phép người dùng tự đăng ký
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Mật khẩu đăng ký:
                </label>
                <div class="col-md-4">
                    <input type="text" value="{{old('password_self_enrol')}}" name="password_self_enrol" class="form-control" id="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-4">
                    Hạn chót:
                </label>
                <div class="col-md-3">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" value="{{old('expired_self_enrol')}}" name="expired_self_enrol" class="actual_range form-control pull-right" id="expired_self_enrol">
                    </div>
                </div>
                <div class="checkbox">
                    <label for="">
                        <?php $curCheck = $errors->any() ? old('enable_expired_self_enrol') : true; ?>
                        <input type="checkbox" {{$curCheck ? 'checked': ''}} name="enable_expired_self_enrol" value="1"> Mở
                    </label>
                </div>
            </div>
            
                        
            <!-- End Đăng ký hoạt động -->
            <div class="form-group">
                <div class="col-sm-10 col-sm-offset-4">
                    <button type="submit" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </form>
    </div>
    <!-- box body -->
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                
                <div class="modal-body">
                    Bạn có chắc chắn xóa ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" id="btnDeleteRow" data-dismiss="modal" class="btn btn-primary">Có</button>
                </div>
        </div>
    </div>
</div>	
<!-- End modal Delete  -->

<!-- time picker -->
@endif

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.min.css') }}">

@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.vi.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-timepicker.min.js') }}"></script>
    
    <!-- <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script> -->
    <!-- <script type="text/javascript" src="{{ asset('js/bootstrap-daterangepicker.js') }}"></script> -->
    
	<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/pages/activity/create.js') }}"></script>
	<script type="text/javascript">
		$(function(){
            
			@if($errors->has('activitycode'))
				$('form#form-department').validate().showErrors({
				 	"activitycode": "{{$errors->first('activitycode')}}"
				});
              @endif
              $( ".actual_range" ).datepicker( "setDate", new Date());
              $('input[name="enable_end_date"]').datepicker('destroy');
        });
	</script>
@stop

