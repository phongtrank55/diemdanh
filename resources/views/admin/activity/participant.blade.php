@extends('layouts.master')

@section('title', 'Thành viên tham gia')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thành viên tham gia</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
     <!-- /.box-header -->
     <div class="box-header">
        @if(session()->has('messageEnrol'))
          <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Thông báo!</strong> {{session()->get('messageEnrol')}}
          </div>
        @endif                
        
          <div class="search-form col-md-8 col-sm-12">
              <form action="" method="GET" class="form-inline" role="form">
                  <div class="form-group">
                      <input type="text" name="idnumber" class="form-control" id="" placeholder="Mã thành viên" value="{{$idnumber}}">
                  </div>
                  <div class="form-group">
                      <input type="text" name="fullname" class="form-control" id="" placeholder="Họ tên" value="{{$fullname}}">
                  </div>
                  <!-- <div class="form-group">
                      <select name="roleid" class="form-control">
                          
                      </select>
                  </div> -->
                  <button type="submit" class="btn btn-primary btn-flat">
                      <i class="fa fa-filter"></i> Lọc
                  </button>
              </form>                 
          </div> 
          @if($canEnrol)
          <a class="btn btn-sm btn-primary pull-right" data-toggle="modal" href="#modal-enrol">
              <i class="fa fa-plus"></i> Thêm thành viên
          </a>
          @endif
      </div>
      <!-- /.box-footer -->

      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>#</th>
              <th>Họ tên</th>
              <th>Mã thành viên</th>
              <th>Ngày sinh</th>
              <th>Vai trò</th>
              <th>Trạng thái</th>
              @if($canEnrol)
              <th></th>
              @endif
            </tr>
            </thead>
            <tbody>
            <?php 
              $currentPage = $userEnrols->currentPage();
              $perPage = $userEnrols->perPage();
              $index = $perPage*($currentPage-1)+1; 
            ?>
              @foreach($userEnrols as $userEnrol)
                <tr>
                  <td class="text-center">{{$index++}}</td>
                  <td nowrap> {{ $userEnrol->getFullname() }}</td>
                  <td>{{$userEnrol->idnumber}}</td>
                  <td>{{$userEnrol->birthday}}</td>
                  <?php
                    $roleAssignments = $userEnrol->roleAssignments;
                  ?>
                  <td class="text-center">
                  @foreach($roleAssignments as $roleAssignment)
                        <span class="label label-primary">
                        {{$roleAssignment->role->name}}
                        </span>
                        <br>
                    @endforeach
                  </td>

                  <td class="text-center">

                    <?php $now = strtotime(date('Y-m-d'));?>
                    @if($now < $userEnrol->timestart)
                    <span class="label label-warning">
                      Chưa đến lúc
                    </span>
                    @elseif($userEnrol->timeend && $now > $userEnrol->timeend)
                    <span class="label label-warning">
                      Hết hạn
                    </span>
                    @else
                    <span class="label label-success">
                      Sẵn sàng
                    </span>
                    @endif
                    @if($userEnrol->status==Constraint::USER_STATUS_TYPE_INACTIVE)
                    <br>
                    <span class="label label-danger">
                      Không hoạt động
                    </span>
                    @endif
                  </td>
                  @if($canEnrol)
                  <td>
                    <a  class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete"
                      data-id = "{{$userEnrol->id}}" 
                      data-title = "{{$userEnrol->getFullname() }}"> 
                      <i class="fa fa-trash"></i> Rút tên </span></a>
                  </td>
                  @endif
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
    <!-- box body -->
    <div class="box-footer">
        {{ $userEnrols->links() }}        
    </div>
</div>

@if($canEnrol)
<!-- Modal Delete -->
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="{{route('admin.activity.unenrol-user', ['activityid'=>$activity->id])}}">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="userid">
                        Bạn có chắc chắn rút tên <strong id="modal-delete-name"></strong> khỏi hoạt động này ?
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End Modal Delete  -->

<!-- Modal Enrol -->
<div class="modal fade" id="modal-enrol">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Đăng ký thành viên</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form action="{{route('admin.activity.enrol-user')}}" method="post">
                @csrf
                <div class="modal-body">
                    <input type="hidden" id="modal-ennol-id" value="{{$activity->id}}" name="activityid">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Thành viên:
                            </label>
                            <div class="col-md-8">
                                <select name="userid" id="potential-user" class="form-control"></select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-8">
                                <div class="checkbox">
                                <label for="">
                                    <input type="checkbox" name="reattendance" value="1"> 
                                    Điểm danh từ những dữ liệu trước đó
                                </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Vai trò:
                            </label>
                            <div class="col-md-6">
                                <select name="roleid" id="" class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Ngày bắt đầu:
                            </label>
                            <div class="col-md-4">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="timestart" class="actual_range form-control pull-right" id="start_date" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Ghi chú:
                            </label>
                            <div class="col-md-9">
                                <input type="text" name="note" id="" class="form-control">
                            </div>
                        </div>
                    </div>
                    </div>
                <div class="modal-footer">
                    <button type="submit" id="" class="btn btn-primary">OK</button>    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End Modal Enrol  -->
@endif
@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">

@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.vi.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/pages/activity/participant.js') }}"></script>
	<script type="text/javascript">
		
	</script>
@stop

