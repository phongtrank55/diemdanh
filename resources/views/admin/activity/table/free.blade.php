<!-- free -->
<div class="col-md-12">
    <div class="table-responsivex">
        <table class="table no-margin text-center detail-work-calendar">
            <thead>
                <tr>
                    <th></th>
                    <th>Ngày</th>
                    <th>Giờ bắt đầu</th>
                    <th>Giờ kết thúc</th>
                    <th>Phòng</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($activityCalendars as $calendar)
                <tr class="form-inline">
                    <td>
                        <input type="hidden" name="calendarid[]" value="{{$calendar->id}}">
                    </td>
                    <td>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="start_date_calendar[]" value="{{date('d/m/Y', $calendar->start_date_calendar)}}" class="actual_range form-control pull-right" id="">
                        </div>
                    </td>
                    <td>
                        <!-- <div class="input-group">
                            <input type="text" class="form-control timepicker">
                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div> -->
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" value="{{$calendar->time_start}}" name="time_start[]" class="form-control time-start timepicker">

                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" name="time_end[]" value="{{$calendar->time_end}}" class="form-control time-end timepicker">

                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </td>
                    <td>
                        <select class="form-control room" name="roomid[]" id="">
                        @foreach($rooms as $room)
                            <option {{$calendar->roomid == $room->id ? 'selected' : ''}}  value="{{$room->id}}">{{$room->roomcode}}</option>
                        @endforeach
                        </select>  
                        
                    </td>
                    <td>
                        <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                <tr class="form-inline last-row">
                    <td>
                        <button type="button" id="btnAddRow" class="btn btn-sm btn-primary">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                    <td>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="start_date_calendar[]" class="actual_range form-control pull-right" id="">
                        </div>
                    </td>
                    <td>
                        <!-- <div class="input-group">
                            <input type="text" class="form-control timepicker">
                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div> -->
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" name="time_start[]" class="form-control time-start timepicker">

                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" name="time_end[]" class="form-control time-end timepicker">

                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </td>
                    <td>
                        <select class="form-control room" name="roomid[]" id="">
                        @foreach($rooms as $room)
                            <option value="{{$room->id}}">{{$room->roomcode}}</option>
                        @endforeach
                        </select>  
                        
                    </td>
                    <td>
                        <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
            <!-- end free -->