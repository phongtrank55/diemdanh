            <!-- some day -->
            <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table no-margin text-center detail-work-calendar">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Thời gian</th>
                                    <th>Thứ</th>
                                    <th>Ca làm việc</th>
                                    <th>Phòng</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($activityCalendars as $calendar)
                                <tr class="form-inline">
                                    <td>
                                        <input type="hidden" name="calendarid[]" value="{{$calendar->id}}">
                                    </td>
                                    <td>
                                        <div class="input-daterange">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="start_date_calendar[]" value="{{date('d/m/Y', $calendar->start_date_calendar)}}" class="form-control pull-right" id="">
                                            </div>
                                            <strong> - </strong>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="end_date_calendar[]" value="{{date('d/m/Y', $calendar->end_date_calendar)}}" class="form-control pull-right" id="">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <select name="day_of_week[]" id="" class="form-control">
                                        @for($i = 2; $i <= 8; $i++)
                                            <option {{$calendar->day_of_week == $i ? 'selected' : ''}} value="{{$i}}"> {{$i == 8 ? 'Chủ nhật' : "Thứ $i"}}</option>
                                        @endfor
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="shift_detail_start_id[]" id="">
                                        @foreach($shiftDetails as $detail)
                                            <option {{$calendar->shift_detail_start_id == $detail->id ? 'selected' : ''}} value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                        @endforeach
                                        </select>  
                                        <strong> - </strong>
                                        <select class="form-control" name="shift_detail_end_id[]" id="">
                                        @foreach($shiftDetails as $detail)
                                            <option {{$calendar->shift_detail_end_id == $detail->id ? 'selected' : ''}} value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                        @endforeach
                                        </select>              
                                    </td>
                                    <td>
                                        <select class="form-control room" name="roomid[]" id="">
                                        @foreach($rooms as $room)
                                            <option {{$calendar->roomid == $room->id ? 'selected' : ''}} value="{{$room->id}}">{{$room->roomcode}}</option>
                                        @endforeach
                                        </select>  
                                        
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                <tr class="form-inline last-row">
                                    <td>
                                        <button type="button" id="btnAddRow" class="btn btn-sm btn-primary">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <div class="input-daterange">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="start_date_calendar[]"  class="form-control pull-right" id="">
                                            </div>
                                            <strong> - </strong>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="end_date_calendar[]" class="form-control pull-right" id="">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <select name="day_of_week[]" id="" class="form-control">
                                        @for($i = 2; $i <= 8; $i++)
                                            <option value="{{$i}}"> {{$i == 8 ? 'Chủ nhật' : "Thứ $i"}}</option>
                                        @endfor
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="shift_detail_start_id[]" id="">
                                        @foreach($shiftDetails as $detail)
                                            <option value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                        @endforeach
                                        </select>  
                                        <strong> - </strong>
                                        <select class="form-control" name="shift_detail_end_id[]" id="">
                                        @foreach($shiftDetails as $detail)
                                            <option value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                        @endforeach
                                        </select>              
                                    </td>
                                    <td>
                                        <select class="form-control room" name="roomid[]" id="">
                                        @foreach($rooms as $room)
                                            <option value="{{$room->id}}">{{$room->roomcode}}</option>
                                        @endforeach
                                        </select>  
                                        
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- end some day -->