<!-- everyday -->
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table no-margin text-center detail-work-calendar">
                            <thead>
                                <tr>
                                    <th>Thứ</th>                                    
                                    <th>Ca làm việc</th>
                                    <th>Phòng</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($i = 2; $i <= 8; $i++)
                                <?php
                                    $calendar = null;
                                    if(!empty($activityCalendars)){
                                        $calendar = $activityCalendars->where('day_of_week', $i)->first();
                                    }
                                
                                ?>
                                <tr class="form-inline">
                                    <td>
                                        @if($calendar)
                                        <input type="hidden" name="calendarid[]" value="{{$calendar->id}}">
                                        @endif
                                        <input type="hidden" name="day_of_week[]" value="{{ $i }}">
                                        <strong>{{ $i == 8 ? 'Chủ nhật' : $i }}</strong>
                                    </td>
                                    <td>
                                        <select class="form-control shift-detail" name="shift_detail_start_id[]" id="">
                                           <option value="0">-- Chọn ca --</option>
                                           @foreach($shiftDetails as $detail)
                                            <option {{$calendar && $calendar->shift_detail_start_id == $detail->id ? 'selected' : ''}} value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                           @endforeach
                                        </select>  
                                        <strong> - </strong>
                                        <select class="form-control shift-detail" name="shift_detail_end_id[]" id="">
                                            <option value="0">-- Chọn ca --</option>
                                            @foreach($shiftDetails as $detail)
                                            <option {{$calendar && $calendar->shift_detail_end_id == $detail->id ? 'selected' : ''}} value="{{$detail->id}}"> {{$detail->name_detail}}</option>
                                            @endforeach
                                        </select>              
                                    </td>
                                    <td>
                                        <select class="form-control room" name="roomid[]" id="">
                                        <option value="0">-- Chọn phòng --</option>
                                            @foreach($rooms as $room)
                                            <option {{$calendar && $calendar->roomid == $room->id ? 'selected' : ''}} value="{{$room->id}}">{{$room->roomcode}}</option>
                                            @endforeach
                                        </select>  
                                        
                                    </td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- end everyday -->
            