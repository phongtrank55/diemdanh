@extends('admin.activity.attendance')
@section('sub-content')
@if(count($calendarDetails)==0)
    
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Chú ý!</strong> Hoạt động này chưa có lịch cụ thể!
    </div>
    
@else
<div class="box-header">
    <div class="col-md-12 form-horizontal">
        <div class="col-md-5">
            <div class="form-group">
                <label for="" class="control-label col-md-4">Chọn buổi:</label>
                <div class="col-md-8">
                    <select name="calendarDetailId" id="calendar-detail" class="form-control">
                        @foreach($calendarDetails as $detail)
                        <option {{$detail->id == $nearestCalendar->id ? 'selected' : ''}}  data-date="{{date('d/m/Y', $detail->date_attendance)}}" value="{{$detail->id}}">{{$detail->fullname}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group" id="ngay-hoc">

                <label for="txtNgayHoc" class="control-label col-md-4">Ngày:</label>
                <div class="col-md-8">
                    <div class="input-group date" id="datepicker">
                        <input id="date-attendance" type="text" class="form-control">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End box-header -->

<div class="box-body">
<div class="loader"></div>
    <div id="result">

    </div>
</div>
<!-- End box-body -->
@endif
@stop

@section('sub-styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
@stop


@section('sub-scripts')
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.vi.js') }}"></script>
    
<script src="{{asset('js/pages/activity/attendance/one.js')}}"></script>
    
@stop