@if($messages)

    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p class="text-center"><strong>{{$messages}}!</strong> </p>
    </div>
@endif
@if(isset($results))
@if($canEdit)
<form action="{{route('admin.activity.attendance-overridden', ['activityCalendarDetailId'=>$activityCalendarDetailId])}}" method="post">
    @csrf
@endif
<div class="box-body">
    <div class="table-responsive">
        <table class="table table-bordered no-margin">
            <thead>
                <tr>
                    <th rowspan="2">#</th>
                    <th rowspan="2">Họ tên</th>
                    <th rowspan="2">Mã thành viên</th>
                    @if(!$autoOverridden)
                    <th rowspan="2">Giờ vào</th>
                    <th rowspan="2">Giờ ra</th>
                    <th rowspan="2">% <br> Có mặt</th>
                    @if($canEdit)                    
                    <th rowspan="2">Ghi đè</th>
                    @endif
                    @endif
                    <th colspan="4">Điểm danh</th>
                    <th rowspan="2">Ghi chú</th>
                </tr>
                <tr>
                    <th>CM</th>
                    <th>M</th>
                    <th>KP</th>
                    <th>CP</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $index => $result)
                <tr>
                    <td class="text-center">{{ $index+1 }}</td>
                    <td> {{ $result->lastname . " " . $result->firstname }} </td>
                    <td> {{ $result->idnumber }} </td>
                    @if(!$autoOverridden)
                    <?php
                        // $times = explode(';', $result->timein);
                        $times = str_replace(Constraint::ACTIVITY_ATTENDANCE_DELIMITER_TIME, '<br>', $result->timein);
                    ?>
                    <td class="text-center">
                        {!! $times !!}
                    </td>
                    <?php
                        // $timeIns = explode(';', $result->timein);
                        $times = str_replace(Constraint::ACTIVITY_ATTENDANCE_DELIMITER_TIME, '<br>', $result->timeout);
                    ?>
                    <td class="text-center">
                        {!! $times !!}
                    </td>
                    <td class="text-center">
                        @if($result->overridden != Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE && !is_null($result->percent_present_shift) )
                        {{$result->percent_present_shift}}%
                        @endif
                    </td>
                    @if($canEdit)
                    <td class="text-center">
                        @if($result->id)
                        <input type="hidden" name="id[{{$result->userid}}]" value="{{$result->id}}">
                        @endif
                        <input type="checkbox" value="1" {{$result->overridden == Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE ? 'checked' : ''}} class="overridden" name="overridden[{{$result->userid}}]" id="">
                    </td>
                    @endif
                    @endif
                    <?php
                        //loc ket qua
                        $value = $result->overridden == Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE ? $result->finalvalue : $result->rawvalue;
                        if($value == null && $autoOverridden)
                            $value = Constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT;
                        $disabledText = $canEdit ? '' : 'disabled';
                    ?>
                    
                    <td class="text-center">
                        <input type="radio" {{$disabledText}} {{$value == Constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT ? 'checked': ''}} value="{{Constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT}}" name="finalvalue[{{$result->userid}}]">
                    </td>
                    <td class="text-center">
                        <input type="radio" {{$disabledText}} {{$value == Constraint::ACTIVITY_ATTENDANCE_RESULT_LATE ? 'checked': ''}} value="{{Constraint::ACTIVITY_ATTENDANCE_RESULT_LATE}}" name="finalvalue[{{$result->userid}}]">
                    </td>
                    <td class="text-center">
                        <input type="radio" {{$disabledText}} {{$value == Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE ? 'checked': ''}} value="{{Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE}}" name="finalvalue[{{$result->userid}}]">
                    </td>
                    <td class="text-center">
                        <input type="radio" {{$disabledText}} {{$value == Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITH_LEAVE ? 'checked': ''}} value="{{Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITH_LEAVE}}" name="finalvalue[{{$result->userid}}]">
                    </td>
                    <td>
                        <div class="form-group" style="margin-bottom:0px;"> 
                            <input {{$disabledText}} type="text" class="form-control" value="{{ $result->note }}" name="note[{{$result->userid}}]">
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div> 
    
    <div class="clearfix">
        <br> <br>
    </div>
    @if($canEdit)
    <button type="submit" class="col-md-2 btn btn-primary pull-right">Lưu</button>
    @endif
</div>
@if($canEdit)
</form>
@endif

@if($canEdit)

<script>
    $(function(){
        // setDisableIf($('input.overridden'), 'checked', false, $('input.overridden').closest('tr').find('input[type="text"], input[type="radio"]'));
        $('input.overridden').change(function(){
            $(this).closest('tr').find('input[type="text"], input[type="radio"]').prop('disabled', $(this).prop('checked') == false);
        });
        $('input.overridden').change();
    });
    
</script>
@endif
@endif