@extends('admin.activity.attendance')
@section('sub-content')
<?php $numDetails = count($calendarDetails); ?>
@if(!$numDetails)
    
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Chú ý!</strong> Hoạt động này chưa có lịch cụ thể!
    </div>
    
@else

<div class="box-body">
    <div class="box-header">
    <label for="" class="inactive">Nghỉ</label>
    <label for="" class="holiday">Nghỉ lễ</label>
    <form method="post" action="{{route('admin.activity.attendance.export', ['id'=>$activity->id])}}">
        @csrf
        <button type="submit" class="btn btn-primary pull-right">
            <i class="fa fa-file-excel-o"></i> Xuất Excel
        </button>
    </form>
    </div>
    
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Họ tên</th>
                    <th>Mã thành viên</th>
                    @foreach($calendarDetails as $detail)
                    <th>
                        <a href="{{route('admin.activity.attendance', ['id'=>$detail->activityid, 
                                    'section' => 'one', 'activitycalendar'=>$detail->id])}}">
                            {{$detail->shortname}}

                        </a>
                    </th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                
                @foreach($results as $index => $result)
                <tr>
                    <td class="text-center">{{$index+1}}</td>
                    <td>{{ $result->lastname . ' ' .$result->firstname }}</td>
                    <td>{{ $result->idnumber }}</td>
                    @for($i = 0; $i < $numDetails; $i++)
                        <?php
                            if($calendarDetails[$i]->status==Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE)
                                $className = 'inactive';
                            elseif($calendarDetails[$i]->status==Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY)
                                $className = 'holiday';
                            else
                                $className = '';
                        ?>
                        <td class="text-center text-bold {{$className}}" >
                            {{ $result->{'value'.$i} }}
                            
                        </td>
                    @endfor
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    
</div>
<!-- End box-body -->
@endif
@stop

@section('sub-styles')
    <style type="text/css">
        .inactive{
            background: orange;
            padding: 3px;
            color:white;
        
        }
        .holiday{
            padding: 3px;
            color:white;
            background: chartreuse;
        }
    </style>
@stop