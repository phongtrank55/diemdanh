@extends('admin.activity.attendance')
@section('sub-content')

<div class="box-header">
    <h3 class="text-center">Lịch hoạt động <strong>{{$activity->name}}</strong></h3>
</div>
@if($canEdit)
<form action="{{route('admin.activity.updatecalendar', ['id'=>$activity->id])}}" method="post">
    @csrf 
@endif
    <div class="box-body">
        <div class="col-md-12" id="work-calendar">
            <div class="table-responsivex">
                <table class="table no-margin text-center detail-work-calendar">
                    <thead>
                        <tr>
                            @if($canEdit)
                            <th></th>
                            @endif
                            <th>Tên đầy đủ</th>
                            <th>Tên <br> viết tắt</th>
                            <th>Ngày</th>
                            <th>Giờ bắt đầu</th>
                            <th>Giờ kết thúc</th>
                            <th>Phòng</th>
                            <th>Trạng thái</th>
                            @if($canEdit)
                            <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $allowEditText = $canEdit ? '' : 'disabled';
                        ?>
                        @foreach($calendarDetails as $key => $detail)
                        <tr class="form-inline">
                            @if($canEdit)
                            <td>
                                <input type="hidden" name="id[]" value="{{$detail->id}}">
                            </td>
                            @endif
                            <td>
                                <input style="width:9em" {{$allowEditText}} type="text" name="fullname[]" id="" value="{{$detail->fullname}}" class="form-control">
                            </td>
                            <td>
                                <input style="width:5em" {{$allowEditText}} type="text" name="shortname[]" id="" value="{{$detail->shortname}}" class="form-control">
                            </td>
                            <td>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <?php $disabled = $canEdit && $detail->modified == Constraint::ACTIVITY_CALENDAR_DETAIL_MODIFIED ? '' : 'disabled' ?>

                                    <input type="text" name="date_attendance[]" {{$disabled}} value="{{date('d/m/Y', $detail->date_attendance)}}" class="actual_range form-control pull-right" id="">
                                </div>
                            </td>
                            <td>
                                @if($attendanceBetweenShift && $disabled)
                                    @foreach($detail->activityCalendarDetailShifts as $detailShift)
                                    <div class="input-group bootstrap-timepicker">
                                        <input type="text" disabled class="form-control time-start timepicker" value="{{$detailShift->timestart}}">
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <br>
                                    @endforeach
                                @else
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="time_start[]" {{$disabled}} class="form-control time-start timepicker" value="{{$detail->time_start}}">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                @endif
                            </td>
                            <td>
                            @if($attendanceBetweenShift && $disabled)
                                @foreach($detail->activityCalendarDetailShifts as $detailShift)
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" disabled class="form-control time-end timepicker" value="{{$detailShift->timeend}}">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <br>
                                @endforeach
                            @else
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="time_end[]"  {{$disabled}} class="form-control time-end timepicker" value="{{$detail->time_end}}">
                                    <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            @endif
                            </td>
                            <td>
                                <select {{$allowEditText}} class="form-control room" name="roomid[]" id="">
                                @foreach($rooms as $room)
                                    <option {{$detail->roomid==$room->id ? 'selected': ''}} value="{{$room->id}}">{{$room->roomcode}}</option>
                                @endforeach
                                </select>  
                            </td>
                            <td>
                                <?php $status = $detail->status ?>
                                <select name="status[]" {{$allowEditText}}  id="" class="form-control">
                                    <option {{ $status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE ? 'selected': ''}} value="{{Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE}}">Hoạt động</option>
                                    @if($status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY)
                                    <option selected="selected" value="{{Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY}}">Nghỉ lễ</option>
                                    @else
                                    <option {{ $status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE ? 'selected': ''}} value="{{Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE}}">Nghỉ</option>
                                    @endif
                                </select>
                            </td>
                            @if($canEdit)
                            <td>
                                @if(!$disabled)
                                <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                                @endif
                            </td>
                            @endif
                        </tr>
                        @endforeach
                        @if($canEdit)
                        <tr class="form-inline last-row">
                            <td>
                                <button type="button" id="btnAddRow" class="btn btn-sm btn-primary">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </td>
                            <td>
                                <input style="width:9em" type="text" name="fullname[]" id="" class="form-control">
                            </td>
                            <td>
                                <input style="width:5em" type="text" name="shortname[]" id="" class="form-control">
                            </td>
                            <td>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="date_attendance[]" class="actual_range form-control pull-right" id="">
                                </div>
                            </td>
                            <td>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="time_start[]" class="form-control time-start timepicker" value="">

                                    <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" name="time_end[]" class="form-control time-end timepicker">
                                    <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <select class="form-control room" name="roomid[]" id="">
                                @foreach($rooms as $room)
                                    <option value="{{$room->id}}">{{$room->roomcode}}</option>
                                @endforeach
                                </select>  
                            </td>
                            <td>
                                <select name="status[]" id="" class="form-control">
                                    <option value="{{Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE}}">Hoạt động</option>
                                    <option value="{{Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE}}">Nghỉ</option>
                                </select>
                            </td>
     
                            <td>
                                <a class="btn btn-sm btn-danger" data-toggle="modal" href="#modal-delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>   
    </div>
    <!-- end box-body -->
    <div class="box-footer">
        <div class="pull-right">
            @if($canEdit)
            <button type="submit" class="btn btn-primary">Lưu</button>
            @endif
        </div>
    </div>
</form>
<!-- End box-footer -->
<!-- Modal delete -->
@if($canEdit)
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                
                <div class="modal-body">
                    Bạn có chắc chắn xóa ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" id="btnDeleteRow" data-dismiss="modal" class="btn btn-primary">Có</button>
                </div>
        </div>
    </div>
</div>	
@endif
<!-- End modal delete -->
@stop

@section('sub-styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-timepicker.min.css') }}">
@stop


@section('sub-scripts')
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.vi.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-timepicker.min.js') }}"></script>
    
<script src="{{asset('js/pages/activity/attendance/calendar.js')}}"></script>
    
@stop