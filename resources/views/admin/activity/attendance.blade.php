@extends('layouts.master')
@section('title', 'Điểm danh')

@section('content')

@foreach($errorMessages as $message)
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Cảnh báo!</strong> {!! $message !!}
</div>
@endforeach
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
  <li class="{{$section == Constraint::ACTIVITY_ATTENDANCE_SECTION_ONE ? 'active' : ''}}">
      <a href="{{$section != Constraint::ACTIVITY_ATTENDANCE_SECTION_ONE ? route('admin.activity.attendance', [ 'id' => $id, 'section' => Constraint::ACTIVITY_ATTENDANCE_SECTION_ONE]) : '#'}}">
        Xem theo ngày
      </a>
    </li>
    <li class="{{$section == Constraint::ACTIVITY_ATTENDANCE_SECTION_ALL ? 'active' : ''}}">
      <a href="{{$section != Constraint::ACTIVITY_ATTENDANCE_SECTION_ALL ? route('admin.activity.attendance', [ 'id' => $id, 'section' => Constraint::ACTIVITY_ATTENDANCE_SECTION_ALL]) : '#'}}">
        Xem toàn bộ
      </a>
    </li>
    <li class="{{$section == Constraint::ACTIVITY_ATTENDANCE_SECTION_CALENDAR ? 'active' : ''}}">
      <a href="{{$section != Constraint::ACTIVITY_ATTENDANCE_SECTION_CALENDAR ? route('admin.activity.attendance', [ 'id' => $id, 'section' => Constraint::ACTIVITY_ATTENDANCE_SECTION_CALENDAR]) : '#'}}">
        Lịch hoạt động
      </a>
    </li>
    
    
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
        @yield('sub-content')
    
    </div>

  </div>
  <!-- /.tab-content -->
</div>
@endsection

@section('styles')
  @yield('sub-styles')
@endsection

@section('scripts')
  @yield('sub-scripts')
@endsection