@extends('layouts.master')

@section('title', 'Thống kê')

@section ('content')


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Thống kê hoạt động {{$activity->name}}</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <!-- /.box-header -->
     <!-- /.box-header -->
     <div class="box-header">
         
          <div class="search-form col-md-8 col-sm-12">
              <form action="" method="GET" class="form-inline" id="formSort" role="form">
                  <div class="form-group">
                    <label for="" class="control-label">Sắp xếp:</label>
                      <select name="sortby" class="form-control" id="selSort">
                          <option {{$sortBy == 0 ? 'selected' : ''}} value="0"> Thứ tự alphabet </option>
                          <option {{$sortBy == 1 ? 'selected' : ''}} value="1"> Số lượng vắng giảm dần </option>
                          <option {{$sortBy == 2 ? 'selected' : ''}} value="2"> Số lượng muộn giảm dần </option>
                      </select>
                  </div>
                  
              </form>                 
          </div> 
      </div>
      <!-- /.box-footer -->

      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
            <tr>
              <th>#</th>
              <th>Họ tên</th>
              <th>Mã thành viên</th>
              <th>Vắng không phép</th>
              <th>Vắng phép</th>
              <th>Muộn</th>              
            </tr>
            </thead>
            <tbody>
                @foreach($results as $index=>$result)
                <tr>
                    <td class="text-center">{{$index+1}}</td>
                    <td>{{$result->lastname}} {{$result->firstname}}</td>
                    <td class="text-center">{{$result->idnumber}}</td>
                    <td class="text-center">{{$result->absent_without_leave}}</td>
                    <td class="text-center">{{$result->absent_with_leave}}</td>
                    <td class="text-center">{{$result->late}}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
    <!-- box body -->

</div>


@stop

@section('scripts')
    
    <script type="text/javascript" src="{{ asset('js/pages/activity/statistic.js') }}"></script>
	<script type="text/javascript">
		
	</script>
@stop

