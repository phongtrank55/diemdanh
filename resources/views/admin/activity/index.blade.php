@extends('layouts.master')
@section('title', 'Quản lý hoạt động')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/pages/activity/index.css')}}">
@endsection

@section('content')

<!-- Danh sach may dien danh -->
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Quản lý hoạt động</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
   <!-- box-body -->
    <div class="box-body">
    <!-- activity category table -->
        <div class="col-md-5 col-sm-12">
            <h4 class="text-center box-title">Danh mục hoạt động</h4>
            <div class="box-header">
                <a href="{{route('admin.activitycategory.create', ['activitycategory'=>$currentCategoryId])}}" class="btn btn-sm btn-info pull-right"> 
                    <i class="fa fa-plus"></i> Thêm danh mục hoạt động
                </a>
            </div>
            <div class="clearfix"></div>
            
            <!-- tree -->
            <ul id="treeActivityCategory" class="tree" data-widget="tree">
                <?php $length = count($activityCategories); ?>
                @foreach($activityCategories as $index => $category)
                <?php $hasChilds = $index < $length-1 && $category['id'] == $activityCategories[$index+1]['parent']; ?>
                <li>
                    <div  class="dropdown pull-right-container pull-right">
                        <a class=" dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-cog"></i> <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" >
                            <li><a href="{{route('admin.activitycategory.create', ['activitycategory' => $category['id']])}}">
                                <i class="fa fa-plus"></i> Thêm tiểu mục
                            </a></li>
                            <li><a href="{{route('admin.activitycategory.edit', ['id' => $category['id']])}}">
                                <i class="fa fa-pencil"></i> Sửa
                            </a></li>
                            <li><a data-toggle="modal" href="#modal-delete-activitycategory"
                                    data-id = "{{$category['id']}}" 
                                            data-title = "{{$category['name']}}">
                                <i class="fa fa-trash"></i> Xóa
                            </a></li>
                            
                        </ul>
                    </div> 
                    @if($hasChilds)
                    <a id="tree-{{$category['id']}}" class="treeview" href=""><i style="width:10px;" class="fa fa-plus"></i></a>
                    @else
                    <span><i style="width:10px;" class="fa fa-plus text-white"></i></span>
                    @endif
                    <a href="{{route('admin.activity.index', ['activitycategory' => $category['id']])}}">
                        <span class="title-item"> {{$category['name']}} ({{$category['activitycategorycode']}})</span>
                    </a>
                    @if($hasChilds)
                    <ul class="treeview-menu">
                    @else
                    </li>
                            <?php $num =  $index==$length-1 ? $category['depth'] : $category['depth'] - $activityCategories[$index+1]['depth']; ?>
                            @for($i=0; $i < $num; $i++)
                            </ul>  </li>
                            @endfor
                    @endif
                
                @endforeach
               
            </ul>
            <!-- end tree -->
        </div>
        <!-- End activity category table -->
        <!-- Activity table -->
        <div class="col-md-7 col-sm-12">
        <h4 class="box-title text-center">Hoạt động</h4>
        <!-- form filter -->
        <div class="box-header">
            <form action="{{route('admin.activity.index')}}" method="GET" class="form-inline" role="form">
                <div class="search-form col-md-9">
                    <div class="form-group">
                        <input type="text" name="keyword" value="{{$keyword}}" required class="form-control" placeholder="Tên hoặc mã hoạt động">
                    </div>
                    
                    <button type="submit" class="btn btn-primary btn-flat">
                        <i class="fa fa-filter"></i> Lọc
                    </button>
                </div> 
                <div class="pull-right">
                    <a href="{{route('admin.activity.create', ['activitycategory' => $currentCategoryId])}}" class="btn btn-sm btn-info"> 
                        <i class="fa fa-plus"></i> Thêm hoạt động
                    </a>
                </div>
            </form> 
        </div>
        <div c9lass="clearfix"></div>
        <!-- end form filter -->
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên</th>
                        <th>Ghi chú</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        if($activities){
                            $currentPage = $activities->currentPage();
                            $perPage = $activities->perPage();
                            $index = $perPage*($currentPage-1)+1; 
                        }
                    ?>
                    @foreach($activities as $activity)
                    <tr>
                        <td class="text-center">{{$index++}}</td>
                        <td>
                            <a class="{{$activity->display == Constraint::ACTIVITY_DISPLAY_OPEN ? '': 'text-muted'}}" href="{{route('admin.activity.attendance', ['id'=> $activity->id, 'section'=>'one'])}}"> {{$activity->name}} ({{$activity->activitycode}})</a>
                        </td>
                        <td> {{$activity->note}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary btn-sm btn-flat dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog"></i> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1"  style="right: 0; left: auto;">
                                    <li><a href="{{route('admin.activity.edit', ['id'=>$activity->id])}}">
                                        <i class="fa fa-pencil"></i> Sửa
                                    </a></li>
                                    <li><a data-toggle="modal" href="#modal-delete-activity"
                                            data-id = "{{$activity->id}}" 
                                            data-title = "{{$activity->activitycode}}"> 
                                        <i class="fa fa-trash"></i> Xóa
                                    </a></li>
                                    
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            <!-- box footer -->
            <div class="box-footer">
                @if($activities)
                    {{ $activities->links() }} 
                @endif
            </div>
            <!-- end box footer -->
        </div>
    <!-- End activity table -->
    </div>
    <!-- End activity table -->
    </div>
   <!-- End boxbody -->
   
</div>

<!-- Modal Delete Room Category-->
<div class="modal fade" id="modal-delete-activitycategory">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="">
                {{csrf_field()}}
                
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="id">
                    Bạn có chắc chắn xóa danh mục <strong id="modal-delete-name"></strong> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End modal Delete Room Category-->
<!-- Modal Delete Room -->
<div class="modal fade" id="modal-delete-activity">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cảnh báo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form method="post" action="">
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" id="modal-delete-id" name="id">
                    Bạn có chắc chắn xóa hoạt động <strong id="modal-delete-name"></strong> ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="submit" class="btn btn-primary">Có</button>
                </div>
            </form>
        </div>
    </div>
</div>	
<!-- End modal Delete Room-->
@endsection
@section('scripts')
    <script src="{{asset('js/pages/activity/index.js')}}"></script>
    <script type="text/javascript">
        
        $(function(){
            @foreach($currentCategoryPathNodes as $node)
                // console.log({{$node}});
                $('a#tree-{{$node}}').trigger('click');
            @endforeach

        });
    </script>

@endsection