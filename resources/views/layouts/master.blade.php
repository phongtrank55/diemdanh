
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
  <title>@yield('title') | Hệ thống điểm danh tự động</title>
  
  <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/AdminLTE.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/skins/skin-blue.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/skins/ionicons.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/mystyle.css')}}">

  {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 --}}
  @yield('styles') 
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper" id="app">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="{{route('home.index')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>AASV</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>AASV</b>inhuni</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div id="logo-site">
          <img src="{{asset('img/vinhuni.png')}}" alt="logo">
          <div class="title">
            <h5 class="title-site min-title">Trường Đại học Vinh</h3>
            <h4 class="title-site max-title">Hệ thống điểm danh tự động</h3>
          </div>
          </div>
    </nav>
    
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <?php
        $user=Auth::user();
        $pathAvatar = $user->avatar ? asset('storage/'. $user->avatar) : asset(Constraint::NO_AVATAR_IMG);
      ?>
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{$pathAvatar}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
          <p>{{$user->getFullname()}}
          </p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Trực tuyến</a>
      </div>
  </div>

  <!-- search form (Optional) -->
  <!-- <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
      </span>
  </div>
</form> -->
<!-- /.search form -->

<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <!-- <li class="header">HEADER</li> -->
    <!-- Optionally, you can add icons to the links -->

  @if(isset($showMenu) && $showMenu && isset($activity))
  <li class="treeview menu-open">
      <a href="#"><i class="fa fa-star"></i> <span>{{$activity->name}}</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu" style="display:block">
    <?php
          $classActive = setActive('admin/activity/attendance');
        ?>  
      <li class="{{$classActive}}"><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}"><i class="fa fa-check-square-o"></i> Điểm danh</a></li>
      <?php
          $classActive = setActive('admin/activity/participant');
        ?>  
      <li class="{{$classActive}}"><a href="{{route('admin.activity.participant', ['id'=>$activity->id])}}"> <i class="fa fa-user"></i> Thành viên tham gia</a></li>
      <?php
          $classActive = setActive('admin/activity/statistic');
        ?>  
      <li class="{{$classActive}}"><a href="{{route('admin.activity.statistic', ['id'=>$activity->id])}}"><i class="fa fa-file-text-o"></i> Thống kê</a></li>
    </ul>
  </li>
  @endif
	<li class ="{{setActive('/')}}"><a href="{{route('home.index')}}"><i class="fa fa-home"></i> <span>Trang chủ</span></a></li>
  <!-- <li><a href=""><i class="fa fa-link"></i> <span>Dữ liệu nhận dạng</span></a></li> -->
  <!-- <li class="treeview">
      <a href="#"><i class="fa fa-link"></i> <span>Các hoạt động</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="#">Các hoạt động bạn tham gia</a></li>
      <li><a href="#">Các hoạt động bạn quản lý</a></li>
      
    </ul>
  </li> -->
  <li class ="{{setActive('self-enrol')}}"><a href="{{route('self-enrol.index')}}"><i class="fa fa-user-plus"></i> <span>Đăng ký hoạt động</span></a></li>
<?php
$canManage = CheckPermission::hasCapability('system:manage_system', ContextSystem::instance());
?>
  @if($canManage)
  <li class="treeview {{!empty(setActive('admin')) ? 'menu-open': ''}}">
      <a href="#"><i class="fa fa-cog"></i> <span>Quản trị hệ thống</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu" style="display:{{!empty(setActive('admin')) ? 'block': 'none'}}">
      <?php
          $classActive = setActive('admin/activity');
          if(empty($classActive))
            $classActive = setActive('admin/activitycategory');
        ?>  
      <li class="{{$classActive}}"><a href="{{route('admin.activity.index')}}"><i class="fa fa-star"></i> Quản lý hoạt động</a></li>
      <?php
        $classActive = setActive('admin/room');
        if(empty($classActive))
          $classActive = setActive('admin/roomgroup');
      ?>
      <li class="{{$classActive}}"><a href="{{route('admin.room.index')}}"><i class="fa fa-bank"></i> Quản lý phòng</a></li>
      <li class="{{setActive('admin/machine')}}"><a href="{{route('admin.machine.index')}}"><i class="fa fa-camera"></i> Quản lý máy điểm danh</a></li>
      <?php
        $classActive = setActive('admin/user');
        if(empty($classActive))
          $classActive = setActive('admin/cohort');
      ?>
      <li class="{{$classActive}}"><a href="{{route('admin.user.index')}}"><i class="fa fa-users"></i> Quản lý thành viên</a></li>
      <li class="{{setActive('admin/shift')}}"><a href="{{route('admin.shift.index')}}"><i class="fa fa-calendar"></i> Quản lý ca làm việc</a></li>
      <li class="{{setActive('admin/holiday')}}"><a href="{{route('admin.holiday.index')}}"><i class="fa fa-calendar-times-o"></i> Quản lý ngày lễ</a></li>
      <li class="{{setActive('admin/config')}}"><a href="{{route('admin.config.index')}}"><i class="fa fa-wrench"></i> Thiết lập hệ thống</a></li>
    </ul>
  </li>
  @endif
  <li><a href="{{route('logout')}}"><i class="fa fa-power-off"></i> <span>Đăng xuất</span></a></li>
</ul>
<!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
 {{--    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol>
</section>
 --}}
<!-- Main content -->
<section class="content container-fluid">
      @yield('content')
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Đồ án tốt nghiệp
  </div>
  <!-- Default to the left -->
  <strong>&copy; {{date('Y')}}</strong> Bản quyền thuộc về <strong>#PT</strong>
</footer>

<!-- Control Sidebar -->

<!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED JS SCRIPTS -->
  <!-- Scripts -->
  <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
  
  <script type="text/javascript" src="{{asset('js/adminlte.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/myscript.js')}}"></script>

  @yield('scripts')  


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
 </body>
 </html>