@extends('layouts.master')
@section('title', 'Đăng ký hoạt động')
@section('content')

<!-- Thong bao neu chua co du lieu nhan dang -->

<!-- <div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý:</strong> Bạn chưa có dữ liệu nhận dạng trên hệ thống! Vui lòng truy cập <a href="">tại đây</a> để nhập dữ liệu nhận dạng.
</div> -->

<!-- End Thong bao neu chua co du lieu nhan dang -->

<!-- Su kien ban quan ly -->
<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title">Các hoạt động bạn có thể đăng ký</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-header">
        
          <div class="search-form col-md-8 col-sm-12">
                <form action="" method="GET" class="form-inline" role="form">
                    <div class="form-group">
                        <input type="text" name="keyword" class="form-control" id="" placeholder="Nhập tên hoặc mã hoạt động" value="{{$keyword}}">
                    </div>
                    <button type="submit" class="btn btn-primary btn-flat">
                        <i class="fa fa-filter"></i> Lọc
                    </button>
                </form>                 
            </div> 
        </div>
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hoạt động</th>
                        <th>Ngày hết hạn</th>
                        <th>Ghi chú</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $currentPage = $activities->currentPage();
                        $perPage = $activities->perPage();
                        $index = $perPage*($currentPage-1)+1; 
                    ?>
                    @foreach($activities as $activity)
                    <tr>
                        <td class="text-center">{{$index++}}</td>
                        <td>{{$activity->name}} ({{$activity->activitycode}})</td>
                        <td class="text-center">{{$activity->expired_self_enrol ? date('d/m/Y', $activity->expired_self_enrol) : 'Không thiêt lập'}}</td>
                        <td>{{$activity->note}}</td>
                        <td class="text-center">
                        <a  class="btn btn-sm btn-info" data-toggle="modal" href="#modal-enrol"
                      data-id = "{{$activity->id}}" 
                      data-title = "{{$activity->name}} ({{$activity->activitycode}})"> 
                      <i class="fa fa-user-plus"></i> Đăng ký </span></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        {{ $activities->links() }}    
    </div>
</div>

<!-- Modal enrol -->
<div class="modal fade" id="modal-enrol">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Đăng ký hoạt động</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
                
                <div class="modal-body">
                <div class="alert alert-danger" id="notification-enrol">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                         <span id="message">qAFĐ</span>
                    </div>
              
                @csrf
                <input type="hidden" id="modal-enrol-id"  name="activityid">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Hoạt động:
                            </label>
                            <label style="text-align:left;" id="modal-enrol-title" class="control-label col-md-8">
                                
                            </label>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">
                                Mật khẩu:
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-submit-enrol" class="btn btn-primary">OK</button>    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                </div>
        </div>
    </div>
</div>	


<!-- End modal enrol -->
@endsection
@section('scripts')
    
    <script type="text/javascript" src="{{ asset('js/pages/self-enrol/index.js') }}"></script>
	<script type="text/javascript">
        var url = '{{route("self-enrol.enrol")}}';
	</script>
@stop

