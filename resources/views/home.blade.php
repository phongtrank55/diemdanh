@extends('layouts.master')
@section('title', 'Trang chủ')
@section('content')

<!-- Thong bao neu chua co du lieu nhan dang -->

<!-- <div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Chú ý:</strong> Bạn chưa có dữ liệu nhận dạng trên hệ thống! Vui lòng truy cập <a href="">tại đây</a> để nhập dữ liệu nhận dạng.
</div> -->

<!-- End Thong bao neu chua co du lieu nhan dang -->
@if(count($manageCurrentActivities) || count($manageFutureActivities) || count($managePastActivities))
<!-- Su kien ban quan ly -->
<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title">Các hoạt động bạn quản lý</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a href="#tab_1" data-toggle="tab">Quá khứ</a></li>
                <li class="active"><a href="#tab_2" data-toggle="tab">Hiện tại</a></li>
                <li><a href="#tab_3" data-toggle="tab">Tương lai</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_1">
                    @if(!count($managePastActivities))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Không có hoạt động nào!
                        </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($managePastActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif                    
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_2">
                    @if(!count($manageCurrentActivities))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Không có hoạt động nào!
                        </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($manageCurrentActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    @if(!count($manageFutureActivities))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Không có hoạt động nào!
                    </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($manageFutureActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif
                </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
    </div>
    <!-- /.box-body -->

</div>
<!-- End su kien ban quan ly -->
@endif
<!-- Su kien ban bi diem danh -->
<div class="box box-info">
    <div class="box-header">
        <h3 class="box-title">Các hoạt động bạn tham gia</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a href="#tab_join_1" data-toggle="tab">Quá khứ</a></li>
                <li class="active"><a href="#tab_join_2" data-toggle="tab">Hiện tại</a></li>
                <li><a href="#tab_join_3" data-toggle="tab">Tương lai</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab_join_1">
                    @if(!count($joinPastActivities))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Không có hoạt động nào!
                        </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Kết quả</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($joinPastActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td class="text-center">
                                        <span class="label label-danger">KP : {{$activity->absent_without_leave}}</span> &nbsp;
                                        <span class="label label-warning">CP : {{$activity->absent_with_leave}}</span> &nbsp;
                                        <span class="label label-info">M : {{$activity->late}}</span>
                                    </td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane active" id="tab_join_2">
                    @if(!count($joinCurrentActivities))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Không có hoạt động nào!
                        </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Kết quả</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($joinCurrentActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td class="text-center">
                                        <span class="label label-danger">KP : {{$activity->absent_without_leave}}</span> &nbsp;
                                        <span class="label label-warning">CP : {{$activity->absent_with_leave}}</span> &nbsp;
                                        <span class="label label-info">M : {{$activity->late}}</span>
                                    </td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_join_3">
                    @if(!count($joinFutureActivities))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Không có hoạt động nào!
                        </div>
                    @else
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Hoạt động</th>
                                    <th>Kết quả</th>
                                    <th>Ghi chú</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($joinFutureActivities as $index=>$activity)
                                <tr>
                                    <td class="text-center">{{$index+1}}</td>
                                    <td><a href="{{route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one'])}}">{{$activity->name}} ({{$activity->activitycode}})</a></td>
                                    <td class="text-center">
                                        <span class="label label-danger">KP : {{$activity->absent_without_leave}}</span> &nbsp;
                                        <span class="label label-warning">CP : {{$activity->absent_with_leave}}</span> &nbsp;
                                        <span class="label label-info">M : {{$activity->late}}</span>
                                    </td>
                                    <td>{{$activity->note}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                    @endif
                </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
    </div>
    <!-- /.box-body -->

</div>

<!-- End su kien ban bi diem danh -->
@endsection
