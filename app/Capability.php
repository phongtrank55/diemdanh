<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capability extends Model
{
    protected $table = 'capabilities';
    protected $fillable = [
        'name', 'capabilitycode', 'contextlevel'
    ];

    public $timestamps = false;

    public function roleCapabilities()
    {
        //ten model, khoa ngoai cua model, khoa chinh model nay
        return $this->hasMany('App\RoleCapability', 'capabilitycode', 'capabilitycode');
    }
}
