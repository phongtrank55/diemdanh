<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleContextLevel extends Model
{
    protected $table = 'role_context_levels';
    protected $fillable = [
        'roleid', 'contextlevel'
    ];

    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo('App\Role','roleid');
    }
}

