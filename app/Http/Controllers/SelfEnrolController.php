<?php

namespace App\Http\Controllers;

use App\Role;
use App\Config;
use App\Activity;
use App\ActivityEnrol;
use App\RoleAssignment;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class SelfEnrolController extends Controller
{

    public function index()
    {
        $user = \Auth::user();
        $keyword = Input::get('keyword', '');
        $activities = Activity::select('id', 'name', 'activitycode', 'note', 'frequency', 'expired_self_enrol')     
                            ->where([['enable_self_enrol', 1], ['display', \Constraint::ACTIVITY_DISPLAY_OPEN]])
                            ->where(function($q){
                                $q->where('expired_self_enrol', 0)
                                ->orWhere('expired_self_enrol', '>=', time());   
                            })->whereDoesntHave('activityEnrols', function($q) use($user){
                                $q->where('userid', $user->id);
                            })
                            ->where(function($q) use($keyword){
                                $q->where('name', 'like', '%'.$keyword.'%')
                                ->orWhere('activitycode', 'like', '%'.$keyword.'%');
                            })
                            ->paginate(\Constraint::ITEMS_PER_PAGE);
                            
        

        $activities->withpath(route('self-enrol.index', compact('keyword')));
        return view('self-enrol.index', compact('activities', 'keyword'));
    }

    public function enrol(Request $request)
    {

        $result = [
            'success'=>false, 'message' => '', 'url' => route('admin.activity.attendance', ['section'=>'one', 'id'=>$request->activityid])
        ];
        $activity = Activity::find($request->activityid);
        if(!$activity || $activity->display != \Constraint::ACTIVITY_DISPLAY_OPEN){
            $result['message'] = 'Hoạt động không tồn tại hoặc bị ẩn';
            return $result; 
        }

        if(!$activity->enable_self_enrol || $activity->expired_self_enrol < time()){
            $result['message'] = 'Hoạt động không cho phép tự đăng ký hoặc quá hạn đăng ký';
            return $result;
        }

        $user = \Auth::user();
        //Kiểm tra đã ghi danh chưa
        if(ActivityEnrol::where([['activityid', $activity->id], ['userid', $user->id]])->first()){
            $result['message'] = 'Hoạt động này bạn đã được đăng ký';
            return $result;
        }
        
        //Kiểm tra mật khẩu
        if(trim($request->password) != trim($activity->password_self_enrol)){
            $result['message'] = 'Mật khẩu không chính xác';
            return $result;
        }   

        //Kiểm tra vai trò khả dụng
        $configRoleId = Config::where([['module', 'activity'], ['name', 'role_self_enrol']])->first()->value;
        if(!$configRoleId || !(Role::find($configRoleId))){
            $result['message'] = 'Chưa thiết lập vai trò cho tự đăng ký';
            return $result;
        }
        
        ActivityEnrol::updateOrCreate(['userid' => $user->id, 'activityid' => $activity->id],
                                    ['timestart' =>  \UtilityFunction::convertDateToInt(date('d/m/Y')),
                                        'method' => \Constraint::ACTIVITY_ENROL_SELF]);

        $context = \ContextActivity::instance($activity->id);
        RoleAssignment::updateOrCreate(['userid' => $user->id, 
                                        'roleid' => $configRoleId, 
                                        'contextid' => $context->id]);
        $result['success'] = true;
        return $result;
    }
}
