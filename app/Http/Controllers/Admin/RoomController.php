<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use App\RoomGroup;
use App\Machine;
use App\Config;
use App\RoomMachine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Constraint;
class RoomController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $roomGroupCurrent = Input::get('roomgroup', -1);
        $status = Input::get('status', -1);
        $rooms =[];
        if($roomGroupCurrent == 0)
            $rooms = Room::get();
        else if($roomGroupCurrent > 0)
            $rooms = Room::where('roomgroupid', $roomGroupCurrent)->get();

        if($roomGroupCurrent !=-1 && $status != -1){
            $rooms = $rooms->where('status', $status);
        }
        foreach($rooms as $room){
            if($room->status == Constraint::ROOM_HAS_MACHINE)
               $room->machines = Machine::select('id', 'machinecode', 'status')
                                 ->whereExists(function ($query) use ($room) {
                                     $query->select(DB::raw(1))
                                         ->from('room_machines')
                                         ->whereRaw('room_machines.roomid=' .$room->id
                                                 . ' AND room_machines.machineid = machines.id');
                                 })->get()->toArray();
            else
                $room->machines = [];
        }
        //load room group
        $roomGroups = RoomGroup::get();
        return view('admin.room.index', compact('roomGroups', 'roomGroupCurrent', 'status', 'rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $roomGroupCurrent = Input::get('roomgroup', 0);
        $roomGroups = RoomGroup::pluck('roomgroupcode', 'id');
        //$roomGroups =[];
        return view('admin.room.create', compact('roomGroups', 'roomGroupCurrent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(Room::where('roomcode', $data['roomcode'])->first()){
            $msgErrors['roomcode']='Mã phòng đã tồn tại!';
        }
       if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        $dep = Room::create($data); 
        return redirect()->route('admin.room.index', ['roomgroup' => $dep->roomgroupid]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $room = Room::findOrFail($id);
        $roomGroups = RoomGroup::pluck('roomgroupcode', 'id');
        return view('admin.room.edit', compact('room', 'roomGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $room = Room::findOrFail($data['id']);
        $msgErrors = [];
        if($room->roomcode != $data['roomcode']  && Room::where('roomcode', $data['roomcode'])->first()){
            $msgErrors['roomcode'] = 'Mã phòng đã tồn tại!';
        }
        
       if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        $room->update($data);
        return redirect()->route('admin.room.index', ['roomgroup' => $room->roomgroupid]);
    }


    public function assignMachine($id){
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $room = Room::findOrFail($id);
        
        $configMaxRoom = Config::where([['module', 'machine'], ['name', 'max_room_per_machine']])->first();
        $configMaxRoom = $configMaxRoom ? $configMaxRoom->value : 0;

        $potentialMachines = Machine::select('id', 'machinecode', 'status')
                            ->whereNotExists(function ($query) use ($room) {
                                $query->select(DB::raw(1))
                                    ->from('room_machines')
                                    ->whereRaw('room_machines.roomid=' .$room->id
                                            . ' AND room_machines.machineid = machines.id');
                            });
        if($configMaxRoom)
            $potentialMachines = $potentialMachines->where(DB::raw('(select count(*) from room_machines WHERE machineid=machines.id)'), '<', $configMaxRoom);
        $potentialMachines = $potentialMachines->get();
        // var_dump(empty($potentialMachines)); die;
        $usedOwnRoomMachines =  Machine::select('id', 'machinecode', 'status')
                                    ->whereExists(function ($query) use ($room) {
                                        $query->select(DB::raw(1))
                                            ->from('room_machines')
                                            ->whereRaw('room_machines.roomid=' .$room->id
                                                    . ' AND room_machines.machineid = machines.id');
                                    })->get();
        return view('admin.room.assignmachine', compact('room', 'potentialMachines', 'usedOwnRoomMachines'));
    }

    public function storeAssignMachine(Request $request){
        $data = $request->all();
        $room = Room::findOrFail($data['id']);

        $currentMachines = [];

        $configMaxRoom = Config::where([['module', 'machine'], ['name', 'max_room_per_machine']])->first();
        $configMaxRoom = $configMaxRoom ? $configMaxRoom->value : 0;
        
        if(isset($data['usemachines'])){
            foreach($data['usemachines'] as $index => $machineId){
                $machine = Machine::find($machineId);
                if($machine){
                    $currentMachines[] = $machineId;
                    //kiem tra da ton tai ban ghi chua
                    $room_machine = RoomMachine::where([['roomid', '=', $room->id], ['machineid', '=', $machineId]])->first();
                    if(!$room_machine){
                        if($configMaxRoom){
                        //kiem tra so luong phong ma may da gan
                            $numRooms = RoomMachine::where('machineid', $machineId)->count();
                            if($numRooms < $configMaxRoom)
                                RoomMachine::create(['roomid' => $room->id, 'machineid' => $machineId]);
                        }
                        else{
                            RoomMachine::create(['roomid' => $room->id, 'machineid' => $machineId]);
                        }
                    }
                }else{
                    unset($data['usemachines'][$index]);
                }
            }
        }
        //Xem máy có phòng
        $statusCurrent = empty($currentMachines) ? Constraint::ROOM_NO_MACHINE:Constraint::ROOM_HAS_MACHINE;
        if($room->status != $statusCurrent){
            $room->status = $statusCurrent;
            $room->save();
        }
        // Xóa các bản ghi đã bỏ
        RoomMachine::where('roomid', $room->id)->whereNotIn('machineid', $currentMachines)->delete();
        //$curremtNotUseMachines = RoomMachine::where('roomid', $room->id)->whereNotIn('machineid', $currentMachines)->get();
        //print_r($curremtNotUseMachines->count());
        // die;
        // foreach($curremtNotUseMachines as $machine){
        //     $machine->roomid = null;
        //     $machine->save();
        // }
        
        
        return redirect()->route('admin.room.assignmachine', ['id'=>$room->id])->with('messageAssignMachine', 'Thao tác thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }
}
