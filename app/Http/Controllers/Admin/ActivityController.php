<?php

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Machine;
use App\Shift;
use App\ShiftDetail;
use App\User;
use App\Room;
use App\Context;
use App\Config;
use App\Holiday;
use App\ActivityCategory;
use App\ActivityCalendar;
use App\ActivityCalendarDetail;
use App\ActivityCalendarDetailShift;
use App\Attendance\AttendanceFromMachine;
use App\ActivityEnrol;
use App\RoleAssignment;
use App\ActivityAttendance;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Constraint;
use App\Common\UtilityFunction;
use App\Common\AssignRole;
use App\Exports\ExportAttendanceResult;
use Excel;

class ActivityController extends \App\Http\Controllers\Controller
{
    private $attendance;
    
    public function __construct()
    {
        $this->attendance = new AttendanceFromMachine;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentCategoryId = Input::get('activitycategory', 0);
        $currentActivityId = Input::get('activity', 0);
        $keyword = Input::get('keyword', '');
        $currentCategory = ActivityCategory::select('path', 'depth')->where('id', $currentCategoryId)->first();
        $currentCategoryPathNodes = [];
        if($currentCategory && $currentCategory->depth > 1 ){
            $currentCategoryPathNodes = explode('/', $currentCategory->path);
            //Xoa 2 cai 2 dau
            unset($currentCategoryPathNodes[0]);
            unset($currentCategoryPathNodes[$currentCategory->depth]);
        }

        $activities = []; 
        if($keyword){
            $activities = Activity::where('name', 'like', '%'.$keyword.'%')->orWhere('activitycode', 'like', '%'.$keyword.'%')->paginate(Constraint::ITEMS_PER_PAGE);
            $activities->withPath(route('admin.activity.index', ['keyword'=>$keyword]));
        }else if($currentCategoryId > 0){
            $activities = Activity::where('activitycategoryid', $currentCategoryId)->paginate(Constraint::ITEMS_PER_PAGE);
            $activities->withPath(route('admin.activity.index', ['activitycategory'=>$currentCategoryId]));
        }
        //load group
        $activityCategories = ActivityCategory::orderBy('path', 'ASC')->get()->toArray();        
        return view('admin.activity.index', compact('activities', 'keyword', 'activityCategories', 'currentCategoryPathNodes', 'currentActivityId', 'currentCategoryId'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $activityCategoryCurrent = Input::get('activitycategory', 0);
        $activityCategories = ActivityCategory::loadParents(0);
        $configs = Config::getConfigs(Constraint::CONFIG_SECTION_ACTIVITY);
        $shifts = Shift::select('id', 'name')->get();
        $hasRoom = Room::count() > 0;
        return view('admin.activity.create', compact('shifts', 'activityCategories', 'activityCategoryCurrent', 'configs', 'hasRoom'));
    }


    public function subViewTableFrequency($frequency, $shift = 0){
        $viewName = '';
        $shiftDetails = null;
        $activityId = Input::get('activity', 0);
        // return $activityId;
        $activityCalendars = [];
        if($activityId){
            $activity = Activity::select('id', 'frequency', 'shiftid')->findOrFail($activityId);
            if($activity->frequency == $frequency && $activity->shiftid == $shift){
                $activityCalendars = ActivityCalendar::where('activityid', $activity->id)->get();
                // return $activityCalendars;
            }
        }
        $rooms = Room::select('id', 'roomcode')->get();
        switch($frequency){
            case Constraint::ACTIVITY_FREQUENCY_SOME_DAY: 
                $shiftDetails = ShiftDetail::select('id', 'name_detail')->where('shiftid', $shift)->orderBy('sortorder', 'ASC')->get();
                $viewName = 'someday';
                break;
            case Constraint::ACTIVITY_FREQUENCY_EVERY_DAY: 
                $viewName = 'everyday';
                $shiftDetails = ShiftDetail::select('id', 'name_detail')->where('shiftid', $shift)->orderBy('sortorder', 'ASC')->get();
                break;
            case Constraint::ACTIVITY_FREQUENCY_FREE: 
                $viewName = 'free';
                break;
            default:
                return abort(404);
        }
        return view('admin.activity.table.' . $viewName, compact('shiftDetails', 'rooms', 'activityCalendars'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(Activity::where('activitycode', $data['activitycode'])->first()){
            $msgErrors['activitycode']='Mã hoạt động đã tồn tại!';
        }
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        $data['start_date'] = UtilityFunction::convertDateToInt($data['start_date']);
        $data['end_date'] = !isset($data['enable_end_date']) ? 0 : UtilityFunction::convertDateToInt($data['end_date']);
        $data['expired_self_enrol'] = $data['enable_self_enrol'] && isset($data['enable_expired_self_enrol']) ? UtilityFunction::convertDateToInt($data['expired_self_enrol']) : 0;
        //add activity
        $activity = Activity::create($data);
        //add calendar
        $calendar =[];
        foreach($data['roomid'] as $index => $roomid){
            $calendar['roomid'] = $roomid;
            $calendar['activityid'] = $activity->id;
            if($activity->frequency == Constraint::ACTIVITY_FREQUENCY_SOME_DAY){
            // return $data['start_date_calendar'][$index];
                if(!$data['start_date_calendar'][$index] or !$data['end_date_calendar'][$index])
                    continue;
                $calendar['start_date_calendar'] = UtilityFunction::convertDateToInt($data['start_date_calendar'][$index]);
                $calendar['end_date_calendar'] = UtilityFunction::convertDateToInt($data['end_date_calendar'][$index]);                    
                $calendar['day_of_week'] = $data['day_of_week'][$index];
                $calendar['shift_detail_start_id'] = $data['shift_detail_start_id'][$index];
                $calendar['shift_detail_end_id'] = $data['shift_detail_end_id'][$index];
            }else if($activity->frequency == Constraint::ACTIVITY_FREQUENCY_EVERY_DAY){ 
                $calendar['shift_detail_start_id'] = $data['shift_detail_start_id'][$index];
                $calendar['shift_detail_end_id'] = $data['shift_detail_end_id'][$index];
                $calendar['day_of_week'] = $data['day_of_week'][$index];
                if(!$roomid || !$calendar['shift_detail_start_id'] || !$calendar['shift_detail_end_id'] || !$calendar['roomid'])
                    continue;
            }else{
                if(!$data['start_date_calendar'][$index])
                    continue;  
                $calendar['start_date_calendar'] = UtilityFunction::convertDateToInt($data['start_date_calendar'][$index]);
                $calendar['time_start'] = $data['time_start'][$index];
                $calendar['time_end'] = $data['time_end'][$index];
            }            
            $actvityCalendar = ActivityCalendar::create($calendar);
        }
        Context::create([
            'contextlevel' =>Constraint::CONTEXT_LEVEL_ACTIVITY,
            'instanceid' => $activity->id
        ]);
        $this->initCalendarDetail($activity);
        return redirect()->route('admin.activity.participant', ['id'=>$activity->id]);
    }

    //check login activity
    private function checkAccess($activity, $user = null){
        if(!$activity){
            return abort(404);
        }
        if(!$user){
            $user = \Auth::user();
        }

        if(\CheckPermission::isAdmin($user)){
            return true;
        }
        
        if($activity->display==Constraint::ACTIVITY_DISPLAY_OPEN){
            //Kiểm tra xem co trong danh sạcsh hiển tại hay không
            $context = \ContextActivity::instance($activity->id);
            if(!$context)
                return abort(404);
            $userInActivity = RoleAssignment::where([['contextid', $context->id], ['userid', $user->id]])->count();
            if($userInActivity){
                return true;
            }
            
            //kiem tra neu chua danmng ky xem co tu dang ky khong
            if($activity->enable_self_enrol){
                // return redirect()->route();
            }
        }
        return abort(403);
    }

// Enrol User

    public function participant($id){
        $activity = Activity::findOrfail($id);
        $this->checkAccess($activity);
        $idnumber = Input::get('idnumber', '');
        $fullname = Input::get('fullname', '');
    
        //Tim vai tro có thể bổ nhiệm
        
        $context = \ContextActivity::instance($id);
        $roles = [];
        $canEnrol = \CheckPermission::hasCapability('activity:enrol_manual', $context);
        // return $canEnrol;
        if($canEnrol){
            $roles = AssignRole::getRoleCanAssign($context);
            // return $roles;
        }        

        $userEnrols = User::select('users.id', 'idnumber', 'firstname','lastname','email','birthday','status',
                                'activity_enrols.id as enrolid', 'activity_enrols.timeend', 'activity_enrols.timestart')
                        ->with(['roleAssignments' => function($p) use($context){
                            $p->select('roleid','userid')->where('contextid', $context->id)->with('role:id,name');
                        }])
                        ->join('activity_enrols', 'activity_enrols.userid', '=', 'users.id' )
                        ->where('activityid', $id)
                        ->where('idnumber', 'like', '%'.$idnumber.'%')->where(DB::raw('CONCAT(lastname, " ", firstname)'), 'like', '%'.$fullname.'%');
                        
        $userEnrols = $userEnrols->paginate(Constraint::ITEMS_PER_PAGE);
        $userEnrols->withpath(route('admin.activity.participant', compact('id', 'idnumber', 'fullname')));
        $showMenu = true;
        return view('admin.activity.participant', compact('idnumber', 'fullname', 'userEnrols', 'activity', 'roles', 'canEnrol', 'showMenu'));
    }


    public function getPotentialUser($id){
        // return $id;
        // return ['a'=>322];
        $activity = Activity::findOrFail($id);
        return User::select('id', 'firstname', 'lastname', 'idnumber')
        ->whereNotExists(function($query) use($id){
            $query->select(DB::raw(1))
            ->from('activity_enrols')
            ->whereRaw('activity_enrols.activityid = ' .$id
                    . ' AND activity_enrols.userid = users.id');
        })
        ->orderBy('firstname', 'ASC')
        ->orderBy('lastname', 'ASC')->get();
    }

    public function enrolUser($method = Constraint::ACTIVITY_ENROL_MANUAL, Request $request){
        $data = $request->all();
        // return $data;
        if(!isset($data['roleid']) || !isset($data['userid']) ){
            return abort(403);
        }
        $activity = Activity::findOrFail($data['activityid']);
        ActivityEnrol::updateOrCreate(['userid' => $data['userid'], 'activityid' => $data['activityid']],
                                    ['timestart' =>  UtilityFunction::convertDateToInt($data['timestart']),
                                        'method' => $method]);

        $context = \ContextActivity::instance($data['activityid']);
        RoleAssignment::updateOrCreate(['userid' => $data['userid'], 
                                        'roleid' => $data['roleid'], 
                                        'contextid' => $context->id]);
        
        return redirect()->route('admin.activity.participant', ['id'=>$activity->id])->with('messageEnrol', 'Thao tác thành công!');
    }

    public function unenrolUser($activityId, Request $request){
        // return $activityId;
        $activity = Activity::findOrFail($activityId);
        $data = $request->all();
        ActivityEnrol::where([['userid', $data['userid']], ['activityid', $activityId]])->delete();

        $context = \ContextActivity::instance($activityId);
        RoleAssignment::where([['userid', $data['userid']], ['contextid', $context->id]])->delete();
        return redirect()->route('admin.activity.participant', ['id'=>$activity->id])->with('messageEnrol', 'Thao tác thành công!');
    }
    // end enrol user


    //add calendar detail
    private function initCalendarDetail(Activity $activity, $force=false){
        if($force){
            ActivityCalendarDetail::where([['activityid', $activity->id], ['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL]])->delete();
        }
        if($activity->frequency == Constraint::ACTIVITY_FREQUENCY_FREE){
            if(ActivityCalendarDetail::where([['activityid', $activity->id], ['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL]])->count() == 0){
                $calendars = ActivityCalendar::where('activityid', $activity->id)
                                    ->orderBy('start_date_calendar', 'ASC')
                                    ->orderBy('time_start', 'ASC')->get();
                if(empty($calendars)) return;
                foreach($calendars as $index => $calendar){
                    // Xét ngày nghỉ lễ
                    $date_attendance = $calendar->start_date_calendar;
                    $holiday = Holiday::select('id')
                    ->where([['type', Constraint::HOLIDAY_TYPE_SPECIFIC_DAY],
                        ['startdate', '<=', $date_attendance],
                        ['enddate', '>=', $date_attendance]])
                    ->orWhere([['type', Constraint::HOLIDAY_TYPE_SOLAR],
                        ['dayeveryyear', date('d', $date_attendance)],
                        ['montheveryyear', date('m', $date_attendance)]])
                    ->first();
            
                    $status = $holiday ? Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY : 
                                                Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE;                  

                    $detail = [
                        'fullname' => 'Buổi '. ($index+1),
                        'shortname' => 'B'. ($index+1),
                        'time_start' => $calendar->time_start,
                        'time_end' => $calendar->time_end,
                        'holidayid' => $holiday ? $holiday->id : null,
                        'date_attendance' => $date_attendance,
                        'activityid' => $activity->id,
                        'roomid' => $calendar->roomid,
                        'status' => $status,
                        'modified' => Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL
                    ];
                    $activityCalendarDetail = ActivityCalendarDetail::create($detail);
                    if($activity->enable_attendance_in_between_shift || $activity->enable_attendance_out_between_shift){
                        ActivityCalendarDetailShift::create([
                            'activitycalendardetailid' => $activityCalendarDetail->id,
                            'timestart' => $calendar->time_start,
                            'timeend' => $calendar->time_end
                        ]);
                    }
                    
                }
            }
        }
        elseif($activity->frequency == Constraint::ACTIVITY_FREQUENCY_EVERY_DAY){
            
            $activityCalendarDetails = ActivityCalendarDetail::where([['activityid', $activity->id], ['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL]]);
            // Neu khong co ngay ket thuc hoat dong thi kiem tra trong thang da co chua
            
            if($activityCalendarDetails->count() == 0 || (!$activity->end_date && $activityCalendarDetails->whereRaw('month(FROM_UNIXTIME(date_attendance)) = '.date('m'))->count()==0)){
                $calendars = ActivityCalendar::where('activityid', $activity->id)
                    ->with('shiftDetailStart')
                    ->with('shiftDetailEnd')
                    ->orderBy('start_date_calendar', 'ASC')
                    ->orderBy('day_of_week', 'ASC')->get();
                if(empty($calendars)) return;

                //neu khong trong lay ngay dau thang
                // return date('01/m/Y');
                $startDate = $activityCalendarDetails->count() == 0 ? $activity->start_date : UtilityFunction::convertDateToInt(date('01/m/Y'));
                //lấy ngay cuoi thang neu khong co
                $endDate = $activity->end_date ? $activity->end_date : UtilityFunction::convertDateToInt(date('t/m/Y'));
                // return date('d/m/Y', $endDate);
                $calendarDetails = [];
                $calendarDetailShifts = [];
                $shift = Shift::findOrFail($activity->shiftid);
                //tính theo mùa
                $calculateBySeason = $shift->season == Constraint::CALCULATE_BY_SEASON;
                // Tính khoảng thời gian mùa hè
                if($calculateBySeason){
                    // Tìm các năm dien ra hoat dong
                    $yearActvities = [];
                    
                    $yearStart = date('Y', $startDate);
                    $yearEnd = date('Y', $endDate);
                    for($i=$yearStart; $i <= $yearEnd; $i++){
                        $yearActvities[$i] = $i;
                    }
                    // return $yearActvities;
                    //Tính các khoảng thời gian mùa hè
                    $arangeSummers = [];
                    foreach($yearActvities as $year){
                        $date = new \stdClass();
                        //tìm thứ của ngày bắt đầu mà hè => tìm ngày đầu tuần
                        $dayOfWeekStartSummer = UtilityFunction::getDayOfWeek($shift->datestartsummer.'/'.$year);
                        //Tìm ngày đầu tuần
                        $date->start = UtilityFunction::addDay($shift->datestartsummer.'/'.$year, -($dayOfWeekStartSummer-2));
                        //tìm ngày bắt đầu mà đông và tìm ngày chủ nhật tuần trước là ngày kết thúc muà hè
                        //nếu cùng năm mà ngày bắt đầu bé hơn ngày kết thúc thì đưa ngày kết thúc sang năm sau
                        if(UtilityFunction::convertDateToInt($shift->datestartwinter.'/'.$year) < $date->start)
                            $year++;
                        $dayOfWeekStartWinter = UtilityFunction::getDayOfWeek($shift->datestartwinter.'/'.$year);
                        //tìm ngày chủ nhật
                        $date->end = UtilityFunction::addDay($shift->datestartwinter.'/'.$year, -($dayOfWeekStartWinter-1));
                        $arangeSummers[] = $date;
                    }
                    unset($yearActvities);
                    // return $arangeSummers; 
                }
                // return $calendars; 
                foreach($calendars as $calendar){
                    
                    // Tính ngày và thời gian
                    $dayOfWeekStartDate = UtilityFunction::getDayOfWeek($startDate, 'U');
                    $intevalDay = $calendar->day_of_week - $dayOfWeekStartDate;

                    if($intevalDay < 0) $intevalDay += 7; 
                    $date_attendance = UtilityFunction::addDay($startDate, $intevalDay, 'U');
                    
                    while($date_attendance <= $endDate){
                        //chuyen ve gio 00:00:00
                        // $date_attendance = UtilityFunction::convertDateToInt(date('d/m/Y', $date_attendance));
                        // echo date('d/m/Y', $date_attendance);
                        // echo ' - ';
                        // Xác định mùa
                        $isSummer = false;
                        if($calculateBySeason){
                            foreach($arangeSummers as $summer){
                                if($date_attendance >= $summer->start && $date_attendance <= $summer->end){
                                    $isSummer = true;
                                    break;
                                }
                            }
                        }
                        //nếu k tính theo mùa thì tính theo mùa hè
                        if(!$calculateBySeason || $isSummer){
                            // echo 'mùa hè';
                            $time_start = $calendar->shiftDetailStart->time_summer_start;
                            $time_end = $calendar->shiftDetailEnd->time_summer_end;
                        }else{
                            // echo 'mùa đông';
                            $time_start = $calendar->shiftDetailStart->time_winter_start;

                            $time_end = $calendar->shiftDetailEnd->time_winter_end;
                        }

                        // Xét ngày nghỉ lễ
                        $holiday = Holiday::select('id')
                                    ->where([['type', Constraint::HOLIDAY_TYPE_SPECIFIC_DAY],
                                        ['startdate', '<=', $date_attendance],
                                        ['enddate', '>=', $date_attendance]])
                                    ->orWhere([['type', Constraint::HOLIDAY_TYPE_SOLAR],
                                        ['dayeveryyear', date('d', $date_attendance)],
                                        ['montheveryyear', date('m', $date_attendance)]])
                                    ->first();
                            // return date('m', $date_attendance);
                            // echo date('d/m/Y', $date_attendance) . '<br>';
                        
                        $status = $holiday ? Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY : 
                                                    Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE;                  

                        $detail = [
                            'time_start' => $time_start,
                            'time_end' => $time_end,
                            'holidayid' => $holiday ? $holiday->id : null,
                            'date_attendance' => $date_attendance,
                            'activityid' => $activity->id,
                            'roomid' => $calendar->roomid,
                            'status' => $status,
                            'modified' => Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL
                        ];
                        //để sắp xếp
                        // return date('d/m/Y', $date_attendance) . " $time_start";
                        $key = UtilityFunction::convertDateToInt(date('d/m/Y', $date_attendance) . " $time_start", true, 'd/m/Y H:i');
                        // return $key;
                        $calendarDetails[$key] = $detail;
                        // return $calendarDetails;
                        // echo '<br>';
                        //Xét diem danh giua cac ca
                        // return $calendar->shiftDetailStart;
                        if($activity->enable_attendance_in_between_shift || $activity->enable_attendance_out_between_shift){
                            $shiftDetails = ShiftDetail::where('shiftid', $activity->shiftid)
                                            ->whereBetween('sortorder', [$calendar->shiftDetailStart->sortorder, $calendar->shiftDetailEnd->sortorder])
                                                ->get();
                            
                            foreach($shiftDetails as $shiftDetail){
                                //nếu k tính theo mùa thì tính theo mùa hè
                                if(!$calculateBySeason || $isSummer){
                                    // echo 'mùa hè';
                                    $timestart = $shiftDetail->time_summer_start;
                                    $timeend = $shiftDetail->time_summer_end;
                                }else{
                                    // echo 'mùa đông';
                                    $timestart = $shiftDetail->time_winter_start;
                                    $timeend = $shiftDetail->time_winter_end;
                                }
                                // return  compact('key', 'timeend','timestart');
                                $calendarDetailShifts[] = compact('key', 'timeend','timestart');
                            }
                            
                        }
                        
                        $date_attendance = UtilityFunction::addDay($date_attendance, 7, 'U');
                    }

                }
                // die;
                //sort calendar
                ksort($calendarDetails);
                // return $calendarDetails;
                // return $calendarDetailShifts;
                $index = 1;
                foreach($calendarDetails as $key=>$detail){
                    $detail['fullname'] = 'Buổi '.$index;
                    $detail['shortname'] = 'B'.$index;
                    $calendarDetails[$key] = ActivityCalendarDetail::create($detail)->id;
                    $index++;
                }
                // return $calendarDetails;
                foreach($calendarDetailShifts as $detail){
                    $detail['activitycalendardetailid'] = $calendarDetails[$detail['key']];
                    ActivityCalendarDetailShift::create($detail);
                }
                unset($calendarDetails); unset($calendarDetailShifts);
                
            }
        
        }
      
        elseif($activity->frequency == Constraint::ACTIVITY_FREQUENCY_SOME_DAY){
            if(ActivityCalendarDetail::where([['activityid', $activity->id], ['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL]])->count() == 0){
                $calendars = ActivityCalendar::where('activityid', $activity->id)
                                    ->with('shiftDetailStart')
                                    ->with('shiftDetailEnd')
                                    ->orderBy('start_date_calendar', 'ASC')
                                    ->orderBy('day_of_week', 'ASC')->get();
                if(empty($calendars)) return;
                $calendarDetails = [];
                $calendarDetailShifts = [];
                $shift = Shift::findOrFail($activity->shiftid);
                //tính theo mùa
                $calculateBySeason = $shift->season == Constraint::CALCULATE_BY_SEASON;
                // Tính khoảng thời gian mùa hè
                if($calculateBySeason){
                    // Tìm các năm dien ra hoat dong
                    $yearActvities = [];
                    foreach($calendars as $calendar){
                        $yearStart = date('Y', $calendar->start_date_calendar);
                        $yearEnd = date('Y', $calendar->end_date_calendar);
                        for($i=$yearStart; $i <= $yearEnd; $i++){
                            $yearActvities[$i] = $i;
                        }
                    }
                    //Tính các khoảng thời gian mùa hè
                    $arangeSummers = [];
                    foreach($yearActvities as $year){
                        $date = new \stdClass();
                        //tìm thứ của ngày bắt đầu mà hè => tìm ngày đầu tuần
                        $dayOfWeekStartSummer = UtilityFunction::getDayOfWeek($shift->datestartsummer.'/'.$year);
                        //Tìm ngày đầu tuần
                        $date->start = UtilityFunction::addDay($shift->datestartsummer.'/'.$year, -($dayOfWeekStartSummer-2));
                        //tìm ngày bắt đầu mà đông và tìm ngày chủ nhật tuần trước là ngày kết thúc muà hè
                        //nếu cùng năm mà ngày bắt đầu bé hơn ngày kết thúc thì đưa ngày kết thúc sang năm sau
                        if(UtilityFunction::convertDateToInt($shift->datestartwinter.'/'.$year) < $date->start)
                            $year++;
                        $dayOfWeekStartWinter = UtilityFunction::getDayOfWeek($shift->datestartwinter.'/'.$year);
                        //tìm ngày chủ nhật
                        $date->end = UtilityFunction::addDay($shift->datestartwinter.'/'.$year, -($dayOfWeekStartWinter-1));
                        $arangeSummers[] = $date;
                    }
                    unset($yearActvities);
                    // return $arangeSummers; 
                }
                foreach($calendars as $calendar){
                    
                    // Tính ngày và thời gian
                    $dayOfWeekStartDate = UtilityFunction::getDayOfWeek($calendar->start_date_calendar, 'U');
                    $intevalDay = $calendar->day_of_week - $dayOfWeekStartDate;

                    if($intevalDay < 0) $intevalDay += 7; 
                    $date_attendance = UtilityFunction::addDay($calendar->start_date_calendar, $intevalDay, 'U');
                    
                    while($date_attendance <= $calendar->end_date_calendar){
                        //chuyen ve gio 00:00:00
                        // $date_attendance = UtilityFunction::convertDateToInt(date('d/m/Y', $date_attendance));
                        // echo date('d/m/Y', $date_attendance);
                        // echo ' - ';
                        // Xác định mùa
                        $isSummer = false;
                        if($calculateBySeason){
                            foreach($arangeSummers as $summer){
                                if($date_attendance >= $summer->start && $date_attendance <= $summer->end){
                                    $isSummer = true;
                                    break;
                                }
                            }
                        }
                        //nếu k tính theo mùa thì tính theo mùa hè
                        if(!$calculateBySeason || $isSummer){
                            // echo 'mùa hè';
                            $time_start = $calendar->shiftDetailStart->time_summer_start;
                            $time_end = $calendar->shiftDetailEnd->time_summer_end;
                        }else{
                            // echo 'mùa đông';
                            $time_start = $calendar->shiftDetailStart->time_winter_start;

                            $time_end = $calendar->shiftDetailEnd->time_winter_end;
                        }

                        // Xét ngày nghỉ lễ
                        $holiday = Holiday::select('id')
                                    ->where([['type', Constraint::HOLIDAY_TYPE_SPECIFIC_DAY],
                                        ['startdate', '<=', $date_attendance],
                                        ['enddate', '>=', $date_attendance]])
                                    ->orWhere([['type', Constraint::HOLIDAY_TYPE_SOLAR],
                                        ['dayeveryyear', date('d', $date_attendance)],
                                        ['montheveryyear', date('m', $date_attendance)]])
                                    ->first();
                            // return date('m', $date_attendance);
                            // echo date('d/m/Y', $date_attendance) . '<br>';
                        
                        $status = $holiday ? Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY : 
                                                    Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE;                  

                        $detail = [
                            'time_start' => $time_start,
                            'time_end' => $time_end,
                            'holidayid' => $holiday ? $holiday->id : null,
                            'date_attendance' => $date_attendance,
                            'activityid' => $activity->id,
                            'roomid' => $calendar->roomid,
                            'status' => $status,
                            'modified' => Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL
                        ];
                        //để sắp xếp
                        // return date('d/m/Y', $date_attendance) . " $time_start";
                        $key = UtilityFunction::convertDateToInt(date('d/m/Y', $date_attendance) . " $time_start", true, 'd/m/Y H:i');
                        // return $key;
                        $calendarDetails[$key] = $detail;
                        // return $calendarDetails;
                        // echo '<br>';
                        //Xét diem danh giua cac ca
                        // return $calendar->shiftDetailStart;
                        if($activity->enable_attendance_in_between_shift || $activity->enable_attendance_out_between_shift){
                            $shiftDetails = ShiftDetail::where('shiftid', $activity->shiftid)
                                            ->whereBetween('sortorder', [$calendar->shiftDetailStart->sortorder, $calendar->shiftDetailEnd->sortorder])
                                                ->get();
                            
                            foreach($shiftDetails as $shiftDetail){
                                //nếu k tính theo mùa thì tính theo mùa hè
                                if(!$calculateBySeason || $isSummer){
                                    // echo 'mùa hè';
                                    $timestart = $shiftDetail->time_summer_start;
                                    $timeend = $shiftDetail->time_summer_end;
                                }else{
                                    // echo 'mùa đông';
                                    $timestart = $shiftDetail->time_winter_start;
                                    $timeend = $shiftDetail->time_winter_end;
                                }
                                // return  compact('key', 'timeend','timestart');
                                $calendarDetailShifts[] = compact('key', 'timeend','timestart');
                            }
                            
                        }
                        
                        $date_attendance = UtilityFunction::addDay($date_attendance, 7, 'U');
                    }

                }
                // die;
                //sort calendar
                ksort($calendarDetails);
                // return $calendarDetails;
                // return $calendarDetailShifts;
                $index = 1;
                foreach($calendarDetails as $key=>$detail){
                    $detail['fullname'] = 'Buổi '.$index;
                    $detail['shortname'] = 'B'.$index;
                    $calendarDetails[$key] = ActivityCalendarDetail::create($detail)->id;
                    $index++;
                }
                // return $calendarDetails;
                foreach($calendarDetailShifts as $detail){
                    $detail['activitycalendardetailid'] = $calendarDetails[$detail['key']];
                    ActivityCalendarDetailShift::create($detail);
                }
                unset($calendarDetails); unset($calendarDetailShifts);
                
                
            }
        }
        
    }

    // attendance
    public function attendance($id, $section)
    {
        $activity = Activity::findOrFail($id);
        $this->checkAccess($activity);   
        $this->initCalendarDetail($activity);
        $context = \ContextActivity::instance($id);
        $showMenu = true;
        //check pre condittion
        //máy k hoạt động
        //phòng không có máy
        //Check nguoi dung k co du lieu

        $errorMessages = $this->checkPreCondition($activity->id);
        // return $errorMessages;
        $viewSection ='';
        $params = [];
        switch($section){
            case Constraint::ACTIVITY_ATTENDANCE_SECTION_ONE:
                $activityCalendarDetailId = Input::get('activitycalendar', 0);
                $viewSection = 'one';
                $nearestCalendar = null;
                
                $calendarDetails = ActivityCalendarDetail::
                                            orderBy('date_attendance', 'ASC')->orderBy('time_start', 'ASC')
                                            ->where('activityid', $activity->id)->get();
                    
                if($activityCalendarDetailId){
                    $nearestCalendar = ActivityCalendarDetail::findOrFail($activityCalendarDetailId);    
                    // $this->attendance->attendance($nearestCalendar, true);
                }else{
                    //tim kiem ngay gan day nhat
                    $today = time();
                    // UtilityFunction::convertDateToInt(date('d/m/Y'));
                    // $foundMax = false;
                    foreach($calendarDetails as $index => $detail){
                        if($detail->completed == Constraint::ACTIVITY_CALENDAR_DETAIL_NO_COMPLTED
                            && $detail->status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE){
                            //diem danh
                            $this->attendance->attendance($detail);
                        }
                        //Tim buoi gan nhat
                        if($nearestCalendar && $detail->date_attendance > $today){
                            // return $nearestCalendar;
                            break;
                        }else{
                            $nearestCalendar = $detail;
                        }
                    }
                }
                $canEdit = \CheckPermission::hasCapability('activity:overridden', $context);
                $params = compact('calendarDetails', 'nearestCalendar', 'canEdit');
                break;
            case Constraint::ACTIVITY_ATTENDANCE_SECTION_CALENDAR:
                $rooms = Room::get();
                $viewSection = 'calendar';
                
                $attendanceBetweenShift = $activity->enable_attendance_in_between_shift || $activity->enable_attendance_out_between_shift;
                $calendarDetails = ActivityCalendarDetail::where('activityid', $activity->id);
                if($attendanceBetweenShift){
                    //load các ca giữa
                    $calendarDetails = $calendarDetails->with('activityCalendarDetailShifts');                    
                }
                $calendarDetails = $calendarDetails->orderBy('date_attendance', 'ASC')->get();
                
                $canEdit = \CheckPermission::hasCapability('activity:update_calendar_detail', $context);
        
                // $calendarDetails = $calendarDetails->first();
                // return $calendarDetails->activityCalendarDetailShifts;
                // return $calendarDetails;
                $params = compact('rooms', 'calendarDetails', 'attendanceBetweenShift', 'canEdit');
                break;
            case Constraint::ACTIVITY_ATTENDANCE_SECTION_ALL:
                $calendarDetails = ActivityCalendarDetail::select('id', 'status', 'shortname', 'activityid')
                                    ->where('activityid', $activity->id)
                                    ->orderBy('date_attendance', 'ASC')
                                    ->orderBy('time_start', 'ASC')
                                    ->get();
                
                // $enrolUsers = ActivityEnrol::select('userid')
                //                 ->where('activityid', $activity->id)
                //                 ->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')
                $results = $this->getAllResult($calendarDetails);
                // return $results;      
                $viewSection = 'all';

                $params = compact('calendarDetails', 'results');
                break;
            default: 
                return abort(404);   
            break;
        }
        // return $calendarDetails;
        return view("admin.activity.section.attendance.$viewSection", compact('section', 'id', 'activity', 'showMenu', 'errorMessages')+$params);
    }

    public function exportResultAttendance($id){
        $activity = Activity::findOrFail($id);
        $filename = $activity->name .'.xlsx';
        // return $filename;
        $calendarDetails = ActivityCalendarDetail::select('id', 'status', 'shortname', 'activityid')
                            ->where('activityid', $id)
                            ->orderBy('date_attendance', 'ASC')
                            ->orderBy('time_start', 'ASC')
                            ->get();
        $data = $this->getAllResult($calendarDetails);
        $heading = ['STT', 'Họ', 'Tên', 'Mã thành viên'];
        foreach($calendarDetails as $detail){
            $heading[] = $detail->shortname; 
        }
        return Excel::download(new ExportAttendanceResult($heading, $data), $filename);
        
    }
    // lấy kết quả tổng kết điểm danh
    private function getAllResult($calendarDetails){
        $numDetails = count($calendarDetails);
        if(!$numDetails){
            return [];
        }
                // $enrolUsers = ActivityEnrol::select('userid')
        //                 ->where('activityid', $activity->id)
        //                 ->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')

        $sql = "SELECT firstname, lastname, idnumber";
        $subSqls = [];
        foreach($calendarDetails as $index=>$detail){
            $subSql = ", (SELECT rawvalue FROM activity_attendances 
                        WHERE activitycalendardetailid = ".$detail->id." AND userid = u.id) as 'rawvalue$index'";
            $subSql .= ", (SELECT finalvalue FROM activity_attendances 
                        WHERE activitycalendardetailid = ".$detail->id." AND userid = u.id) as 'finalvalue$index'";
            $subSql .= ", (SELECT overridden FROM activity_attendances 
                        WHERE activitycalendardetailid = ".$detail->id." AND userid = u.id) as 'overridden$index'";
            $subSqls[] = $subSql;
        }
        $sql .= implode(' ', $subSqls);
        //find all role
        $configAttendanceRole = Config::where('name', 'attendance_role')->first()->value;
        $configAttendanceRole = str_replace(Constraint::CONFIG_DELIMITER, ',', $configAttendanceRole);
        $sql .= " FROM users u
                    WHERE EXISTS (SELECT * FROM role_assignments
                            WHERE userid = u.id
                            and contextid = ( SELECT id from contexts where 
                            
                            instanceid = ". $calendarDetails[0]->activityid.")
                            and roleid in ($configAttendanceRole) )";
        $sql .= " ORDER BY firstname ASC, lastname ASC";
        $attendances = DB::select($sql);
        // return $attendances;
        $results = [];
        
        foreach($attendances as $attendance){
            $result = new \stdClass;
            $result->lastname = $attendance->lastname;
            $result->firstname = $attendance->firstname;
            $result->idnumber = $attendance->idnumber;
          
            //nếu ghi đè thì lấy kq ghi đè
            for($i = 0; $i < $numDetails; $i++){
                $value = $attendance->{'overridden'.$i} == Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE ?
                                                $attendance->{'finalvalue'.$i} : $attendance->{'rawvalue'.$i};
                switch($value){
                    case Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE:
                        $result->{'value'.$i} = 'KP';
                        break;
                    case Constraint::ACTIVITY_ATTENDANCE_RESULT_LATE:
                        $result->{'value'.$i} = 'M';
                        break;
                    case Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITH_LEAVE:
                        $result->{'value'.$i} = 'CP';
                        break;
                    default:
                        $result->{'value'.$i} = '';
                        break;
                }                
            }
            $results[] = $result;
        }
        return $results;
                
    }

    public function resultAttendanceByDetail($id, $activityCalendarDetailId){
        // return $activityCalendarDetailId;
        $activityCalendarDetail = ActivityCalendarDetail::findOrFail($activityCalendarDetailId);
        $readyAttendance = $this->attendance->checkReadyAttenance($activityCalendarDetail);
        
        // return $readyAttendance;
        switch($readyAttendance){
            case AttendanceFromMachine::ATTENDANCE_STATUS_READY:
                $messages='';
                $showResult = true;
                break;
            case AttendanceFromMachine::ATTENDANCE_STATUS_HOLIDAY:
                $holiday = Holiday::select('fullname')->find($activityCalendarDetail->holidayid);
                $messages = 'Hôm nay nghỉ lễ "'.$holiday->fullname.'" nên không điểm danh';
                $showResult = false;
                break;
            case AttendanceFromMachine::ATTENDANCE_STATUS_NOT_FINISH:
                $messages = 'Hoạt động hôm nay chưa kết thúc nên chưa có kết quả điểm danh';
                $showResult = true;
                break;
            case AttendanceFromMachine::ATTENDANCE_STATUS_INACTIVE:
                $messages = 'Hôm nay được cán bộ giám sát cho nghỉ nên không điểm danh';
                $showResult = false;
                break;
                case AttendanceFromMachine::ATTENDANCE_STATUS_NOT_MACHINE:
                $messages = 'Hoạt động hôm nay diễn ra tại phòng chưa gán máy điểm danh nên phải điểm danh thủ công';
                $showResult = true;
                break;
            case AttendanceFromMachine::ATTENDANCE_STATUS_NOT_ENABLE:
                $messages = 'Hoạt động này không kích hoạt chế độ tự động nên phải điểm danh thủ công';
                $showResult = true;
                break;
            default:
                $messages = 'Hoạt động hôm nay chưa bắt đầu nên chưa có kết quả điểm danh';
                $showResult = true;
                break;
        }

        $params = compact('messages', 'activityCalendarDetailId');
        // $showResult = true;
        if($showResult){
            // return $this->attendance->attendance($activityCalendarDetail);
            // return 'abc';

            //find all role
            $configAttendanceRole = Config::where('name', 'attendance_role')->first()->value;
            $configAttendanceRole = str_replace(Constraint::CONFIG_DELIMITER, ',', $configAttendanceRole);
            // $sql .= " 
            //         WHERE EXISTS (SELECT * FROM role_assignments
            //                 WHERE userid = u.id
            //                 and contextid = ( SELECT id from contexts where 
                            
            //                 instanceid = ". $calendarDetails[0]->activityid.")
        
            $context = \ContextActivity::instance($activityCalendarDetail->activityid);

            $results = Db::table('v_attendance')
            ->select('id', 'userid', 'timein', 'timeout', 'percent_present_shift', 'finalvalue', 'rawvalue', 'overridden', 'note', 'firstname', 'lastname', 'idnumber')
            ->where('activitycalendardetailid', $activityCalendarDetailId)
            ->whereRaw("EXISTS (SELECT * FROM role_assignments
                             WHERE userid = v_attendance.userid 
                             and contextid = " . $context->id .
                             " and roleid in ($configAttendanceRole)) ")
            ->WhereRaw('timestartenrol <= date_attendance AND (timeendenrol is null or timeendenrol >= date_attendance)')
            ->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')
            ->get();
            // return $results;
            //cho phép tu dong chon điểm danh thủ công

            $autoOverridden = $readyAttendance == AttendanceFromMachine::ATTENDANCE_STATUS_NOT_ENABLE
                                || $readyAttendance == AttendanceFromMachine::ATTENDANCE_STATUS_NOT_MACHINE;
        
            
            $canEdit = \CheckPermission::hasCapability('activity:overridden', $context);
            $params = $params + compact('results', 'autoOverridden', 'canEdit');
        }
        return view('admin.activity.section.attendance.result-attendance-detail', $params);
    }

    //Kiem tra dieu kien truoc
    private function checkPreCondition($activityId){
        $errorMessages = [];
        $inactiveRooms = [];
        $inactiveMachines = [];
        $roomHasMachineId = [];
        $machineActives = [];
        // return $this->attendance->getRoomsAndMachines($activity->id);
        [$rooms, $machines] = $this->attendance->getRoomsAndMachines($activityId);
        // return $rooms;
        //phòng không có máy
        foreach($rooms as $room){
            if($room->status == Constraint::ROOM_NO_MACHINE)
                $inactiveRooms[] = $room->name;
            else
                $roomHasMachineId[] = $room->id;
        }
       
        if($inactiveRooms)
            $errorMessages[] = '<strong>' . implode(', ', $inactiveRooms) . '</strong> chưa gán máy điểm danh';
        // return $machines;
        //máy không hoạt động
        foreach($machines as $machine){
            if($machine->status == Constraint::MACHINE_STATUS_INACTIVE)
                $inactiveMachines[] = $machine->machinecode;
            else
                $machineActives[] = $machine;
        }
        if($inactiveMachines)
            $errorMessages[] = 'Máy <strong>' . implode(', ', $inactiveMachines) . '</strong> không hoạt động'; 

        //Kiem tra nguoi chua co du lieu diem danh
        foreach($machineActives as $machine)
        {
            $userNotMachine = ActivityEnrol::select('userid')->with('user:id,idnumber,firstname,lastname')
                            ->where('activityid', $activityId)
                            ->whereNotExists(function ($query) use($machine) {
                                $query->select(DB::raw(1))
                                    ->from('user_machines')
                                    ->where('machineid', $machine->id)
                                    ->whereRaw('user_machines.userid = activity_enrols.userid');
                            })->get();

            $userNotMachineData = [];
            foreach($userNotMachine as $user){
                $userNotMachineData[] = $user->user->getFullname() . ' (' . ($user->user->idnumber) . ')';
            }
            if($userNotMachineData)
                $errorMessages[] = 'Thành viên <strong>' . implode(', ', $userNotMachineData) .
                    '</strong> chưa có dữ liệu nhận dạng trên máy <strong>'.$machine->machinecode.'</strong>'; 
        }
        return $errorMessages;
    }

    //update lich chi tiet
    public function updateCalendar($id, Request $request){
        $activity = Activity::findOrFail($id);
        $data = $request->all();
        // return $data;
        $numDetailInitial = ActivityCalendarDetail::where([['activityid', $id],['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_INITIAL]])
                            ->count();
        // return $numDetailInitial;
        $len = count($data['roomid']);
        $currentDetailId = []; 
        for ($i=0; $i < $len; $i++) { 
            $fullname = $data['fullname'][$i];
            $shortname = $data['shortname'][$i];
            $detailid = isset($data['id'][$i]) ? $data['id'][$i] : 0 ;
            $roomid = $data['roomid'][$i];
            $status = $data['status'][$i];
            if(!$fullname || !$shortname)
                continue;
            if($detailid){
                $detail = ActivityCalendarDetail::findOrFail($detailid);
            }
            else{
                $detail = new ActivityCalendarDetail;
                $detail->modified = Constraint::ACTIVITY_CALENDAR_DETAIL_MODIFIED;

            }
            $indexDetailModified = $i-$numDetailInitial; //vi cac tham so cua initial deu bi khoa
                // return $indexDetailModified;
            if($indexDetailModified >= 0){
                $date_attendance = $data['date_attendance'][$indexDetailModified] ? UtilityFunction::convertDateToInt($data['date_attendance'][$indexDetailModified]) : 0;
                if(!$date_attendance || !$data['time_start'][$indexDetailModified] || !$data['time_end'][$indexDetailModified])
                    continue;
                $detail->date_attendance = $date_attendance;
                $detail->time_start = $data['time_start'][$indexDetailModified];
                $detail->time_end = $data['time_end'][$indexDetailModified];
                $detail->activityid = $id;
                // return $detail->time_end;
            }
                
            $detail->fullname = $fullname;
            $detail->shortname = $shortname;
            $detail->roomid = $roomid;
            $detail->status = $status;
            $detail->completed = Constraint::ACTIVITY_CALENDAR_DETAIL_NO_COMPLTED;
            $detail->save();
            if($indexDetailModified >= 0){ //vi cac ban ghi initial deu bi khoa
                $currentDetailId[] = $detail->id;
                if($activity->enable_attendance_in_between_shift || 
                    $activity->enable_attendance_out_between_shift){
                    //chen vao detail->shift
                    ActivityCalendarDetailShift::where('activitycalendardetailid', $detail->id)->delete();
                    ActivityCalendarDetailShift::create([
                        'activitycalendardetailid' => $detail->id,
                        'timestart' => $detail->time_start,
                        'timeend' => $detail->time_end
                    ]);
                }
            }
            
            // Điểm danh lại
            $this->attendance->attendance($detail, true);            
        }

        // return $currentDetailId;
        //xoa cac ban ghi da xoa (modified)
                    
        ActivityCalendarDetail::where([['activityid', $id],['modified', Constraint::ACTIVITY_CALENDAR_DETAIL_MODIFIED]])
                    ->whereNotIn('id', $currentDetailId)->delete();
        return redirect()->route('admin.activity.attendance', ['id'=>$id, 'section'=>Constraint::ACTIVITY_ATTENDANCE_SECTION_CALENDAR]);
       
    }

    // Overridden Attendance
    public function overridenAttendance($activityCalendarDetailId, Request $request){
        $activityCalendarDetail = ActivityCalendarDetail::findOrFail($activityCalendarDetailId);
        $data = $request->all();
        // return $data;
        if(isset($data['note'])){
            foreach($data['note'] as $index => $note){
                $finalValue = isset($data['finalvalue'][$index]) ? $data['finalvalue'][$index] : null;
                $activitieAttendance = ActivityAttendance::updateOrCreate(
                    ['activitycalendardetailid' => $activityCalendarDetailId, 'userid' => $index],
                    ['overridden' => Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE,
                    'finalvalue' => $finalValue,
                    'note' => $note,
                    'useroverridden' => \Auth::user()->id]);
                unset($data['id'][$index]);
            }
        }
        //Xoa cac ban ghi huy override
        if(isset($data['id']) && !empty($data['id']))
            ActivityAttendance::whereIn('id', $data['id'])->update(
                                                ['overridden' => Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_FALSE,
                                                'finalvalue' => null,
                                                'useroverridden' => null,
                                                'note' => null]);
        // print_r($data); die;
        return redirect()->route('admin.activity.attendance', ['id'=>$activityCalendarDetail->activityid, 
                                                                'section' => 'one', 'activitycalendar'=>$activityCalendarDetailId]);
    }
    // End attendance
    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // return 'ok';
        // return config('app.machine_dir');
    
        // return $this->attendance->attendance();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::findOrFail($id);
        $context = \ContextActivity::instance($id);
        \CheckPermission::requireCapability('activity:edit', $context);
        $activityCategories = ActivityCategory::loadParents(0);
        $configs = Config::getConfigs(Constraint::CONFIG_SECTION_ACTIVITY);
        $shifts = Shift::select('id', 'name')->get();
        $hasRoom = Room::count() > 0;
        return view('admin.activity.edit', compact('activity','shifts', 'activityCategories', 'hasRoom'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $activity = Activity::findOrFail($id);
        $data = $request->all();
        
        $msgErrors = [];
        if($activity->activitycode != $data['activitycode']  && Activity::where('activitycode', $data['activitycode'])->first()){
            $msgErrors['activitycode'] = 'Mã hoạt động đã tồn tại!';
        }
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        $data['start_date'] = UtilityFunction::convertDateToInt($data['start_date']);
        $data['end_date'] = !isset($data['enable_end_date']) ? 0 : UtilityFunction::convertDateToInt($data['end_date']);
        $data['expired_self_enrol'] = $data['enable_self_enrol'] && isset($data['enable_expired_self_enrol']) ? UtilityFunction::convertDateToInt($data['expired_self_enrol']) : 0;
        //add activity
        $activity->update($data);
        //update calendar
        ActivityCalendar::where('activityid', $activity->id)->delete();
        $calendar =[];
        foreach($data['roomid'] as $index => $roomid){
            $calendar['roomid'] = $roomid;
            $calendar['activityid'] = $activity->id;
            if($activity->frequency == Constraint::ACTIVITY_FREQUENCY_SOME_DAY){
            // return $data['start_date_calendar'][$index];
                if(!$data['start_date_calendar'][$index] or !$data['end_date_calendar'][$index])
                    continue;
                $calendar['start_date_calendar'] = UtilityFunction::convertDateToInt($data['start_date_calendar'][$index]);
                $calendar['end_date_calendar'] = UtilityFunction::convertDateToInt($data['end_date_calendar'][$index]);                    
                $calendar['day_of_week'] = $data['day_of_week'][$index];
                $calendar['shift_detail_start_id'] = $data['shift_detail_start_id'][$index];
                $calendar['shift_detail_end_id'] = $data['shift_detail_end_id'][$index];
            }else if($activity->frequency == Constraint::ACTIVITY_FREQUENCY_EVERY_DAY){ 
                $calendar['shift_detail_start_id'] = $data['shift_detail_start_id'][$index];
                $calendar['shift_detail_end_id'] = $data['shift_detail_end_id'][$index];
                $calendar['day_of_week'] = $data['day_of_week'][$index];
                if(!$roomid || !$calendar['shift_detail_start_id'] || !$calendar['shift_detail_end_id'] || !$calendar['roomid'])
                    continue;
            }else{
                if(!$data['start_date_calendar'][$index])
                    continue;  
                $calendar['start_date_calendar'] = UtilityFunction::convertDateToInt($data['start_date_calendar'][$index]);
                $calendar['time_start'] = $data['time_start'][$index];
                $calendar['time_end'] = $data['time_end'][$index];
            }            
            $actvityCalendar = ActivityCalendar::create($calendar);
        }
        $this->initCalendarDetail($activity, true);
        return redirect()->route('admin.activity.attendance', ['id'=>$activity->id, 'section'=>'one']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */

    public function statistic($id){
        $activity = Activity::findOrFail($id);
        $this->checkAccess($activity);
        $showMenu = true;
        $sortBy = Input::get('sortby', '0');
        //tim vai tro bi diem danh
        $configAttendanceRole = Config::where('name', 'attendance_role')->first()->value;
        $configAttendanceRole = str_replace(Constraint::CONFIG_DELIMITER, ',', $configAttendanceRole);
    
        $context = \ContextActivity::instance($id);

        $results = Db::table('v_statistics')
        ->where('activityid', $id)
        ->whereRaw("EXISTS (SELECT * FROM role_assignments
                         WHERE userid = v_statistics.userid 
                         and contextid = " . $context->id .
                         " and roleid in ($configAttendanceRole)) ");
        if($sortBy==1){
            $results = $results->orderBy('absent_without_leave', 'DESC')->orderBy('absent_with_leave', 'DESC');
        }elseif($sortBy==2){
            $results = $results->orderBy('late', 'DESC');
        }
        
        $results = $results->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')
                    ->get();

        return view('admin.activity.statistic', compact('activity', 'showMenu', 'results', 'sortBy'));
    }

    public function destroy(Activity $activity)
    {
        //
    }
}
