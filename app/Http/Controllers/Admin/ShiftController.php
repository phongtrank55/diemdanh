<?php

namespace App\Http\Controllers\Admin;

use App\Shift;
use App\ShiftDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Constraint;

class ShiftController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=0)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $shifts = Shift::paginate(Constraint::ITEMS_PER_PAGE);      
        $params = compact('shifts');
        if($id){
            $curShift = Shift::findOrFail($id);
            $shiftDetails = ShiftDetail::where('shiftid', $id)->orderBy('sortorder', 'ASC')->get();
            $params = $params + compact('curShift', 'shiftDetails');
        }
        $paramPaginates = [];
        if($id){
            $paramPaginates = compact('id');
        }
        $shifts->withPath(route('admin.shift.index', $paramPaginates));
        return view('admin.shift.index', $params);
    }


    // private function createTreeDetail($shiftDetails){
    //     ì
    //     $result =[];
        
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        return view('admin.shift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // echo '<pre>'; print_r($data);  echo '</pre>'; die;
        $msgErrors = [];
        
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        if(!isset($data['season'])){
            $data['season'] = 0;
        }
        $shift = Shift::create($data);
        $lenDetail = count($data['hour_summer_start']);
        for($i=0; $i < $lenDetail; $i++){
            $hour_summer_start = empty($data['hour_summer_start'][$i]) ? 0 : $data['hour_summer_start'][$i];
            $minute_summer_start = empty($data['minute_summer_start'][$i]) ? 0 : $data['minute_summer_start'][$i];
            $hour_summer_end = empty($data['hour_summer_end'][$i]) ? 0 : $data['hour_summer_end'][$i];
            $minute_summer_end = empty($data['minute_summer_end'][$i]) ? 0 : $data['minute_summer_end'][$i];
            $hour_winter_start = empty($data['hour_winter_start'][$i]) ? 0 : $data['hour_winter_start'][$i];
            $minute_winter_start = empty($data['minute_winter_start'][$i]) ? 0 : $data['minute_winter_start'][$i];
            $hour_winter_end = empty($data['hour_winter_end'][$i]) ? 0 : $data['hour_winter_end'][$i];
            $minute_winter_end = empty($data['minute_winter_end'][$i]) ? 0 : $data['minute_winter_end'][$i];

            $time_summer_start = strtotime("$hour_summer_start:$minute_summer_start");
            $time_summer_end = strtotime("$hour_summer_end:$minute_summer_end");

            if($time_summer_start >= $time_summer_end)
                continue;
            
            $detail = ['time_summer_start' => date('H:i', $time_summer_start),
                        'time_summer_end' => date('H:i', $time_summer_end)];

            if($data['season']){
                
                $time_winter_start = strtotime("$hour_winter_start:$minute_winter_start");
                $time_winter_end = strtotime("$hour_winter_end:$minute_winter_end");
            
                if($time_winter_start >= $time_winter_end)
                    continue;
                $detail = $detail + ['time_winter_start' => date('H:i', $time_winter_start),
                                    'time_winter_end' => date('H:i', $time_winter_end)];

            }
            $detail['name_detail'] = $data['name_detail'][$i];
            $detail['shiftid'] = $shift->id;
            ShiftDetail::create($detail);
        }
        $this->sortDetailShift($shift->id);
        
        return redirect()->route('admin.shift.index', ['id' => $shift->id]);
    }

    private function sortDetailShift($shiftid){
        $shift = Shift::findOrFail($shiftid);
        $shiftDetails = ShiftDetail::where('shiftid', $shiftid)->orderBy('time_summer_start', 'ASC')->get();
        foreach($shiftDetails as $index => $detail){
            $detail->sortorder = $index+1;
            $detail->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $shift = Shift::findOrFail($id);
        $shiftDetails = ShiftDetail::where('shiftid', $id)->orderBy('sortorder', 'ASC')->get();
            
        return view('admin.shift.edit', compact('shift', 'shiftDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        // echo '<pre>'; print_r($data);  echo '</pre>'; die;
        $shift = Shift::findOrFail($data['id']);
        $msgErrors = [];
        
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        if(!isset($data['season'])){
            $data['season'] = 0;
            $data['datestartsummer'] = null;
            $data['datestartwinter'] = null;
        }
        $shift->update($data);
        $curDetails = []; // luu chi tiet dang cap nhat
        $lenDetail = count($data['hour_summer_start']);
        for($i=0; $i < $lenDetail; $i++){
            $hour_summer_start = empty($data['hour_summer_start'][$i]) ? 0 : $data['hour_summer_start'][$i];
            $minute_summer_start = empty($data['minute_summer_start'][$i]) ? 0 : $data['minute_summer_start'][$i];
            $hour_summer_end = empty($data['hour_summer_end'][$i]) ? 0 : $data['hour_summer_end'][$i];
            $minute_summer_end = empty($data['minute_summer_end'][$i]) ? 0 : $data['minute_summer_end'][$i];
            $hour_winter_start = empty($data['hour_winter_start'][$i]) ? 0 : $data['hour_winter_start'][$i];
            $minute_winter_start = empty($data['minute_winter_start'][$i]) ? 0 : $data['minute_winter_start'][$i];
            $hour_winter_end = empty($data['hour_winter_end'][$i]) ? 0 : $data['hour_winter_end'][$i];
            $minute_winter_end = empty($data['minute_winter_end'][$i]) ? 0 : $data['minute_winter_end'][$i];
            
            $time_summer_start = strtotime("$hour_summer_start:$minute_summer_start");
            $time_summer_end = strtotime("$hour_summer_end:$minute_summer_end");

            if($time_summer_start >= $time_summer_end)
                continue;
            
            $detail = ['time_summer_start' => date('H:i', $time_summer_start),
                        'time_summer_end' => date('H:i', $time_summer_end)];

            if($data['season']){
                
                $time_winter_start = strtotime("$hour_winter_start:$minute_winter_start");
                $time_winter_end = strtotime("$hour_winter_end:$minute_winter_end");
            
                if($time_winter_start >= $time_winter_end)
                    continue;
                $detail = $detail + ['time_winter_start' => date('H:i', $time_winter_start),
                                    'time_winter_end' => date('H:i', $time_winter_end)];
            }else{
                $detail = $detail + ['time_winter_start' => null,
                                    'time_winter_end' => null];
            }
            $detail['name_detail'] = $data['name_detail'][$i];
            $detail['shiftid'] = $shift->id;
            $detail['id'] = $data['detail_id'][$i];
            
            $curDetail = null;
            if($detail['id']){
                $curDetail = ShiftDetail::where([['id', '=', $detail['id']],
                                                ['shiftid', '=', $shift->id]])->first();
            }
            if($curDetail)
                $curDetail->update($detail);
            else
                $curDetail = ShiftDetail::create($detail);

            $curDetails[] = $curDetail->id;
            
        }
           // Xóa các bản ghi đã bỏ
        ShiftDetail::where('shiftid', $shift->id)->whereNotIn('id', $curDetails)->delete();
        $this->sortDetailShift($shift->id);
        return redirect()->route('admin.shift.index', ['id' => $shift->id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        //
    }
}
