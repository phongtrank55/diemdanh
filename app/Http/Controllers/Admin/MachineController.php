<?php

namespace App\Http\Controllers\Admin;

use App\Machine;
use App\UserMachine;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Constraint;
use Illuminate\Support\Facades\DB;

class MachineController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $keyword = Input::get('keyword', '');
        $status = Input::get('status', -1);
        
        $machines = Machine::where(function ($query) use($keyword) {
            $query->where('machinecode', 'like', '%'.$keyword.'%')->orWhere('model', 'like', '%'.$keyword.'%');
        });
        if($status==Constraint::MACHINE_STATUS_ACTIVE or $status==Constraint::MACHINE_STATUS_INACTIVE){
            $machines = $machines->where('status', $status);
        }
        
        $machines = $machines->with('rooms.room')->paginate(Constraint::ITEMS_PER_PAGE); 
        // return $machines;
        if($status == -1 && empty($keyword))
            $machines->withPath(route('admin.machine.index'));
        else
            $machines->withPath(route('admin.machine.index', compact('status', 'keyword')));
        return view('admin.machine.index', compact('machines', 'keyword', 'status'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        return view('admin.machine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(Machine::where('machinecode', $data['machinecode'])->first()){
            $msgErrors['machinecode']='Mã máy đã tồn tại!';
        }
        if(Machine::where('filename', $data['filename'])->first()){
            $msgErrors['filename']='Tên tệp này đã tồn tại!';
        }
       if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        $dep = Machine::create($data); 
        return redirect()->route('admin.machine.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function show(Machine $machine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $machine = Machine::findOrFail($id);
        return view('admin.machine.edit', compact('machine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $machine = Machine::findOrFail($data['id']);
        $msgErrors = [];
        if($machine->machinecode != $data['machinecode']  && Machine::where('machinecode', $data['machinecode'])->first()){
            $msgErrors['machinecode'] = 'Mã máy đã tồn tại!';
        }
        if($machine->filename != $data['filename'] && Machine::where('filename', $data['filename'])->first()){
            $msgErrors['filename']='Tên tệp này đã tồn tại!';
        }        
       if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        $machine->update($data);
        return redirect()->route('admin.machine.index');
    }

    public function showIdentityUser($id){
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $idnumber = Input::get('idnumber', '');
        $fullname = Input::get('fullname', '');

        $machine = Machine::findOrFail($id);
        $params = ['id'=>$id];
        if(!empty($idnumber)){
            $params = $params + compact('idnumber');     
        }
        if(!empty($fullname)){
            $params = $params + compact('fullname');     
        }
        $userMachines = UserMachine::where('machineid', $machine->id);
        if(!empty($fullname) || !empty($idnumber)){
            $userMachines = $userMachines->whereHas('user', function($query) use($fullname, $idnumber){
                $query->where([[DB::raw('CONCAT(lastname, firstname)'), 'like', '%'.$fullname.'%'], 
                                ['idnumber', 'like', '%'.$idnumber.'%']]);
            });
        }
        $userMachines = $userMachines->with('user:id,firstname,lastname,idnumber,birthday')
                                        ->paginate(Constraint::ITEMS_PER_PAGE);
        
        // return $userMachines;
        $userMachines->withPath(route('admin.machine.update-identity-user', $params));
        
        return view('admin.machine.identity-user', compact('machine', 'userMachines', 'idnumber', 'fullname'));
    }

    public function getPotentialUser($id){
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $machine = Machine::findOrFail($id);
        return User::select('id', 'firstname', 'lastname', 'idnumber')
                    ->whereDoesntHave('userMachines', function($query) use($id){
                        $query->where('machineid', $id);
                    })->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')
                    ->get();
    }

    public function storeIdentityUser($id, Request $request){
        // return $id;
        // return $request->all();
        $machine = Machine::findOrFail($id);
        if(empty(trim($request->usercode))){
            return ['success' => false, 'message' => 'Mã định danh không được để trống'];
        }
        if(empty(trim($request->userid))){
            return ['success' => false, 'message' => 'Phải chọn một người'];
        }
        //kiểm tra trùng mã định danh
        $userMachine = UserMachine::where([['machineid', $id], ['usercode', $request->usercode]])->first();
        if(!$userMachine || $userMachine->userid == $request->userid){
            UserMachine::updateOrCreate(['machineid' => $id, 'userid' => $request->userid],
                                            ['usercode'=> $request->usercode]);
            return ['success' => true, 'message' => 'Thành công'];
        }
        return ['success' => false, 'message' => 'Trùng mã định danh'];
    }

    public function deleteIdentityUser(Request $request){
        $userMachine = UserMachine::findOrFail($request->id);
        // return $userMachine;
        $userMachine->delete();
        return redirect()->route('admin.machine.update-identity-user', ['id' => $userMachine->machine->id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Machine $machine)
    {
        //
    }
}
