<?php

namespace App\Http\Controllers\Admin;

use App\RoomGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Constraint;
class RoomGroupController extends \App\Http\Controllers\Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        return view('admin.roomgroup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(RoomGroup::where('roomgroupcode', $data['roomgroupcode'])->first()){
            $msgErrors['roomgroupcode']='Mã nhóm phòng đã tồn tại!';
        }
       if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        $dep = RoomGroup::create($data); 
        return redirect()->route('admin.room.index');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoomGroup  $roomgroup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $roomGroup = RoomGroup::findOrFail($id);
        return view('admin.roomgroup.edit', compact('roomGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoomGroup  $roomgroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $roomgroup = RoomGroup::findOrFail($data['id']);
        $msgErrors = [];
        if($roomgroup->roomgroupcode != $data['roomgroupcode']  && RoomGroup::where('roomgroupcode', $data['roomgroupcode'])->first()){
            $msgErrors['roomgroupcode'] = 'Mã nhóm phòng đã tồn tại!';
        }
        
       if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        $roomgroup->update($data);
        return redirect()->route('admin.room.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoomGroup  $roomgroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoomGroup $roomgroup)
    {
        //
    }
}
