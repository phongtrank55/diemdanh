<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Constraint;
use Illuminate\Support\Facades\Hash;
use Storage;

class UserController extends \App\Http\Controllers\Controller
{

    public function index(){
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance()); 
        $username = trim(Input::get('username', ''));
        $fullname = trim(Input::get('fullname', ''));
        $status = Input::get('status', -1);
        $users = User::where(function ($query) use($username, $fullname) {
            $query->where('username', 'like', '%'.$username.'%')->where(DB::raw('CONCAT(lastname, " ", firstname)'), 'like', '%'.$fullname.'%');
        });
        if($status==Constraint::USER_STATUS_TYPE_ACTIVE or $status==Constraint::USER_STATUS_TYPE_INACTIVE){
            $users = $users->where('status', $status);
        }
        $users = $users->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')->paginate(Constraint::ITEMS_PER_PAGE); 
        // $users = $users->orderBy('firstname', 'ASC')->orderBy('lastname', 'ASC')->paginate(1); 
        if($status == -1 && empty($keyword))
            $users->withPath(route('admin.user.index'));
        else
            $users->withPath(route('admin.user.index', compact('status', 'username', 'fullname')));
        return view('admin.user.index', compact('users', 'username', 'fullname', 'status'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance()); 
        return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(isset($data['idunmber']) && User::where('idunmber', $data['idunmber'])->first()){
            $msgErrors['idunmber']='Mã thành viên đã tồn tại!';
        }
        if(isset($data['username']) && User::where('username', $data['username'])->first()){
            $msgErrors['username']='Tên đăng nhập đã tồn tại!';
        }
        if(isset($data['email']) && trim($data['email']) != '' && User::where('email', $data['email'])->first()){
            $msgErrors['email']='Email đã tồn tại!';
        }
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        $data['password'] = Hash::make($data['password']);
        if(isset($data['avatar'])){
            $imgName =  time();
            // $data['Avatar'] = $imgName;
            // $path = $request->Avatar->storeAs('public/avatar', $imgName); //in Storage
            $path = Storage::putFile(Constraint::PATH_AVATAR .$imgName, $request->avatar);
            // return $path;
            $data['avatar'] = $path;
        }else{
            $data['avatar'] = null;
        }
        $user = User::create($data); 
        return redirect()->route('admin.user.index');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());        

        $user = User::findOrFail($id);
        // $parents = self::loadParents($id);
        return view('admin.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $user = User::findOrFail($data['id']);
        $msgErrors = [];

        if(isset($data['idunmber']) && $user->idnumber != $data['idnumber'] && User::where('idunmber', $data['idunmber'])->first()){
            $msgErrors['idunmber']='Mã thành viên đã tồn tại!';
        }
        if(isset($data['username']) && $user->username != $data['username'] && User::where('username', $data['username'])->first()){
            $msgErrors['username']='Tên đăng nhập đã tồn tại!';
        }
        if(isset($data['email']) && trim($data['email']) != '' && $user->email != $data['email'] && User::where('email', $data['email'])->first()){
            $msgErrors['email']='Email đã tồn tại!';
        }  
        if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        if($data['changePassword'] && trim($data['password']) != '' )
            $data['password'] = Hash::make($data['password']);
        else{
            unset($data['password']);
        }
        if(isset($data['avatar'])){
           $imgName =  time();
           $path = Storage::putFile(Constraint::PATH_AVATAR .$imgName, $request->avatar);
           $data['avatar'] = $path;
        }else{
            unset($data['avatar']);
        }
        $user->update($data); 
        return redirect()->route('admin.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
