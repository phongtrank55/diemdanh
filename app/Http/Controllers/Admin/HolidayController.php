<?php

namespace App\Http\Controllers\Admin;

use App\Holiday;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Common\Constraint;
use App\Common\UtilityFunction;
class HolidayController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $holidays = Holiday::paginate(Constraint::ITEMS_PER_PAGE); 
        $holidays->withPath(route('admin.holiday.index'));
        return view('admin.holiday.index', compact('holidays', 'keyword', 'status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
     
        return view('admin.holiday.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(Holiday::where('shortname', $data['shortname'])->first()){
            $msgErrors['shortname']='Tên viết tắt này đã tồn tại!';
        }

        //xét âm lịch
        // if($data['type'] == Constraint::HOLIDAY_TYPE_LUNAR){
        //     $msgErrors['startdate']='Ngày âm này không tồn tại!';
        //     $msgErrors['enddate']='Ngày âm này không tồn tại!';
        // }

        
        if($data['type'] == Constraint::HOLIDAY_TYPE_SOLAR){
            $dateEveryyear = explode('/', $data['startdate']);
            $dayEveryyear = $dateEveryyear[0];
            $monthEveryyear = $dateEveryyear[1];
            $data['startdate'] = null;
            //Xét không có ngày 29/02
            if($dayEveryyear >= 29 and $monthEveryyear == 2 )
                $msgErrors['startdate']='Ngày 29/02 không được phép!';
            $data['montheveryyear']=$monthEveryyear;
            $data['dayeveryyear'] = $dayEveryyear;
        }
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);

        if($data['type'] == Constraint::HOLIDAY_TYPE_SOLAR){
            $data['startdate'] = UtilityFunction::convertDateToInt($data['startdate'].'/2019');
            $data['enddate'] = UtilityFunction::convertDateToInt($data['enddate'].'/2019');
        }else if($data['type'] == Constraint::HOLIDAY_TYPE_SPECIFIC_DAY){
            $data['startdate'] = UtilityFunction::convertDateToInt($data['startdate']);
            $data['enddate'] = UtilityFunction::convertDateToInt($data['enddate']);
        }
        
        $dep = Holiday::create($data); 
        return redirect()->route('admin.holiday.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function show(Holiday $holiday)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $holiday = Holiday::findOrFail($id);
        return view('admin.holiday.edit', compact('holiday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $holiday = Holiday::findOrFail($data['id']);
        $msgErrors = [];
        if($holiday->shortname != $data['shortname']  && Holiday::where('shortname', $data['shortname'])->first()){
            $msgErrors['shortname'] = 'Tên viết tắt này đã tồn tại!';
        }
         //xét âm lịch
        // if($data['type'] == Constraint::HOLIDAY_TYPE_LUNAR){
        //     $msgErrors['startdate']='Ngày âm này không tồn tại!';
        //     $msgErrors['enddate']='Ngày âm này không tồn tại!';
        // }
        if($data['type'] == Constraint::HOLIDAY_TYPE_SOLAR){
            $dateEveryyear = explode('/', $data['startdate']);
            $dayEveryyear = $dateEveryyear[0];
            $monthEveryyear = $dateEveryyear[1];
            $data['startdate'] = null;
            //Xét không có ngày 29/02
            if($dayEveryyear >= 29 and $monthEveryyear == 2 )
                $msgErrors['startdate']='Ngày 29/02 không được phép!';
            $data['montheveryyear']=$monthEveryyear;
            $data['dayeveryyear'] = $dayEveryyear;
        }

        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        if($data['type'] == Constraint::HOLIDAY_TYPE_SOLAR){
            $data['startdate'] = UtilityFunction::convertDateToInt($data['startdate'].'/2019');
            $data['enddate'] = UtilityFunction::convertDateToInt($data['enddate'].'/2019');
        }else if($data['type'] == Constraint::HOLIDAY_TYPE_SPECIFIC_DAY){
            $data['startdate'] = UtilityFunction::convertDateToInt($data['startdate']);
            $data['enddate'] = UtilityFunction::convertDateToInt($data['enddate']);
        }
        
        $holiday->update($data);
        return redirect()->route('admin.holiday.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Holiday  $holiday
     * @return \Illuminate\Http\Response
     */
    public function destroy(Holiday $holiday)
    {
        //
    }
}
