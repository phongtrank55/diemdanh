<?php

namespace App\Http\Controllers\Admin;

use App\Config;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Constraint;

class ConfigController extends \App\Http\Controllers\Controller
{
    public function __construct()
    {
        
    }

    public function index($section = Constraint::CONFIG_SECTION_ACTIVITY){
        // $configs['enable_before_in_default'] = 1;
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $params = [];
        switch ($section){
            case Constraint::CONFIG_SECTION_ACTIVITY:
                $roles = Role::select('id', 'name')
                    ->whereHas('roleContextLevels', function($q){
                    $q->where('contextlevel', Constraint::CONTEXT_LEVEL_ACTIVITY);
                })->get();
                $params = compact('roles');
                break;
            case Constraint::CONFIG_SECTION_MACHINE:
                break;
            case Constraint::CONFIG_SECTION_ROLE:
                return \App::call('App\Http\Controllers\Admin\RoleController@index');
                break;
            case Constraint::CONFIG_SECTION_SYSTEM:
                break;
            case Constraint::CONFIG_SECTION_EMAIL:
                break;
            default:  return abort(404);
        }
        // return view('admin.config.index', compact('section', 'render'));
        $render = "admin.config.sections.$section";
        $configs = Config::getConfigs($section);
        
        return view($render, compact('section', 'configs')+$params);
    }
    
    public function update(Request $request, $section = Constraint::CONFIG_SECTION_ACTIVITY){
        // return $section; 
        $data = $request->all();
        if($section == Constraint::CONFIG_SECTION_ACTIVITY){
            $data['attendance_role'] = isset($data['attendance_role']) ?
                                implode(Constraint::CONFIG_DELIMITER, $data['attendance_role']) : ''; 
            
        }
        
        foreach($data as $key => $value){
            $cfg = Config::where([['module', '=', $section], ['name', '=', $key]])->first();
            if($cfg){
                $cfg->value = $value;
                $cfg->save();
            }
        }
        return redirect()->route('admin.config.index', compact('section'))->with('messageConfig', 'Đã lưu thành công!');
    }
}
