<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\RoleContextLevel;
use App\RoleCapability;
use App\Capability;
use App\RoleAllowAssign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Constraint;
class RoleController extends \App\Http\Controllers\Controller
{ 
    private $section = 'role';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $roles = Role::paginate(Constraint::ITEMS_PER_PAGE); 
        $roles->withPath(route('admin.config.index', ['section'=>$this->section]));

        return view('admin.config.sections.role.index', compact('roles') + ['section' => $this->section]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $roles = Role::all();
        $capabilities = Capability::all();

        return view('admin.config.sections.role.create', compact('capabilities', 'roles') + ['section' => $this->section]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // return $data;
        $msgErrors = [];
        if(Role::where('rolecode', $data['rolecode'])->first()){
            $msgErrors['rolecode']='Mã vai trò đã tồn tại!';
        }
       if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        
        $role = Role::create(['rolecode'=>$data['rolecode'], 
                            'name'=>$data['name'],
                            'description'=>$data['description']]);
        if(isset($data['contextlevel10'])){
            RoleContextLevel::create(['roleid' => $role->id, 'contextlevel' => Constraint::CONTEXT_LEVEL_SYSTEM]);
        }
        if(isset($data['contextlevel100'])){
            RoleContextLevel::create(['roleid' => $role->id, 'contextlevel' => Constraint::CONTEXT_LEVEL_ACTIVITY]);
        }
        if(isset($data['allowassign'])){
            foreach($data['allowassign'] as $roleAssign){
                RoleAllowAssign::create(['roleid' => $role->id, 'roleidassign' => $roleAssign]); 
            }
        }
        if(isset($data['capabilties'])){
            foreach($data['capabilties'] as $capability){
                RoleCapability::create(['roleid' => $role->id, 'capabilitycode' => $capability]); 
            }
        }
        return redirect()->route('admin.config.index', ['section' => 'role']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $role = Role::findOrFail($id);
        $roleContextSystem = RoleContextLevel::where([['roleid', $id], ['contextlevel', Constraint::CONTEXT_LEVEL_SYSTEM]])->first();
        $roleContextActivity = RoleContextLevel::where([['roleid', $id], ['contextlevel', Constraint::CONTEXT_LEVEL_ACTIVITY]])->first();
        $roles = Role::withCount(['roleBeAssigns'=>function ($q) use($id){
            $q->where('roleid', $id);
        }])->get();
        // return $roles;

        $capabilities = Capability::withCount(['roleCapabilities'=>function ($q) use($id){
            $q->where('roleid', $id);
        }])->get();
        // return $capabilities;
        return view('admin.config.sections.role.edit', compact('roleContextActivity', 'roleContextSystem',
                                 'capabilities', 'role', 'roles') + ['section' => $this->section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $role = Role::findOrFail($id);
        $data = $request->all();
        $msgErrors = [];
        if($role->rolecode != $data['rolecode']  && Role::where('rolecode', $data['rolecode'])->first()){
            $msgErrors['rolecode'] = 'Mã vai trò đã tồn tại!';
        }

        $role->update(['rolecode'=>$data['rolecode'], 
                            'name'=>$data['name'],
                            'description'=>$data['description']]);
        if(isset($data['contextlevel10'])){
            RoleContextLevel::updateOrCreate(['roleid' => $id, 'contextlevel' => Constraint::CONTEXT_LEVEL_SYSTEM]);
        }else{
            RoleContextLevel::where([['roleid', $id], ['contextlevel', Constraint::CONTEXT_LEVEL_SYSTEM]])
                            ->delete();
        }
        if(isset($data['contextlevel100'])){
            RoleContextLevel::updateOrCreate(['roleid' => $id, 'contextlevel' => Constraint::CONTEXT_LEVEL_ACTIVITY]);
        }else{
            RoleContextLevel::where([['roleid', $id], ['contextlevel', Constraint::CONTEXT_LEVEL_ACTIVITY]])
                            ->delete();
        }
        $currentAllowAssigns = [];
        if(isset($data['allowassign'])){
            foreach($data['allowassign'] as $roleAssign){
                RoleAllowAssign::updateOrCreate(['roleid' => $id, 'roleidassign' => $roleAssign]); 
                $currentAllowAssigns[] = $roleAssign;
            }
        }
        RoleAllowAssign::where('roleid', $id)->whereNotIn('roleidassign', $currentAllowAssigns)
                        ->delete();

        $currentCapabilities = [];
        if(isset($data['capabilties'])){
            foreach($data['capabilties'] as $capability){
                RoleCapability::updateOrCreate(['roleid' => $role->id, 'capabilitycode' => $capability]); 
                $currentCapabilities[] = $capability;
            }
        }
        RoleCapability::where('roleid', $id)->whereNotIn('capabilitycode', $currentCapabilities)
                        ->delete();
       if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        $role->update($data);
        return redirect()->route('admin.config.index', ['section' => 'role']);
    }

    /*
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $role = Role::findOrFail($request->id);
        $role->delete();
        return redirect()->route('admin.config.index', ['section' => 'role']);
    }
}
