<?php

namespace App\Http\Controllers\Admin;

use App\ActivityCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Constraint;

class ActivityCategoryController extends \App\Http\Controllers\Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());
        $currentCategoryId = Input::get('activitycategory', 0);        
        $parents = ActivityCategory::loadParents();

        return view('admin.activitycategory.create', compact('currentCategoryId', 'parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $msgErrors = [];
        if(ActivityCategory::where('activitycategorycode', $data['activitycategorycode'])->first()){
            $msgErrors['activitycategorycode']='Mã danh mục hoạt động đã tồn tại!';
        }
        if(!empty($msgErrors) > 0) return redirect()->back()->withInput()->withErrors($msgErrors);
        if($data['parent'] != 0)
            $activityCategoryParent = ActivityCategory::select('id', 'depth', 'path')->findOrFail($data['parent']);
        else{
            $activityCategoryParent = new \stdClass();
            $activityCategoryParent->path='';
            $activityCategoryParent->depth = 0;
        }
        
        $activityCategory = ActivityCategory::create($data); 
        $activityCategory->depth = $activityCategoryParent->depth + 1;
        $activityCategory->path = $activityCategoryParent->path . '/' . $activityCategory->id;
        $activityCategory->save();
        return redirect()->route('admin.activity.index', ['activitycategory'=> $activityCategory->id]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ActivityCategory  $activitycategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \CheckPermission::requireCapability('system:manage_system', \ContextSystem::instance());

        $activityCategory = ActivityCategory::findOrFail($id);
        $parents = ActivityCategory::loadParents($id);
        return view('admin.activitycategory.edit', compact('activityCategory', 'parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ActivityCategory  $activitycategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $activityCategory = ActivityCategory::findOrFail($data['id']);
        $msgErrors = [];
        if($activityCategory->activitycategorycode != $data['activitycategorycode']  && ActivityCategory::where('activitycategorycode', $data['activitycategorycode'])->first()){
            $msgErrors['activitycategorycode'] = 'Mã danh mục hoạt động đã tồn tại!';
        }
        
       if(!empty($msgErrors)) return redirect()->back()->withInput()->withErrors($msgErrors);
        if($activityCategory->parent != $data['parent']){
            if($data['parent'] != 0)
                $activityCategoryParent = ActivityCategory::select('id', 'depth', 'path')->findOrFail($data['parent']);
            else{
                $activityCategoryParent = new \stdClass();
                $activityCategoryParent->path='';
                $activityCategoryParent->depth = 0;
            }
        }else {
            unset($data['parent']);
        }
        $activityCategory->update($data);
        if(isset($activityCategoryParent)){
            //Tính sự thay đổi depth
            $newDepth = $activityCategoryParent->depth + 1;
            $subDepth = $newDepth - $activityCategory->depth;
            $oldPath = $activityCategory->path;
            $newPath = $activityCategoryParent->path . '/' . $activityCategory->id;
            //update this category
            $activityCategory->depth = $newDepth;
            $activityCategory->path = $newPath;
            $activityCategory->save();
            //update child nodes
            // $childAactivityCategories = ActivityCategory::where('path', 'like', $activityCategory->path . '%')->get();
            DB::table('activity_categories')->where('path', 'like',  "$oldPath/%")
                                        ->update(['depth' => DB::raw("depth + $subDepth"),
                                                    'path' => DB::raw("REPLACE(path, '$oldPath', '$newPath')")]);


        }
        return redirect()->route('admin.activity.index', ['activitycategory'=> $activityCategory->id]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ActivityCategory  $activitycategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ActivityCategory $activitycategory)
    {
        //
    }
}
