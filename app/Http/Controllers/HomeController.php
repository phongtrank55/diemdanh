<?php

namespace App\Http\Controllers;

use App\Role;
use App\Config;
use App\RoleAssignment;
use App\Context;
use App\Activity;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Các hoạt động bạn quản lý
        $roleIdCanOverrides = Role::whereHas('roleCapabilities', function($q){
            $q->where('capabilitycode','activity:overridden');
        })->pluck('id');
        // return $roleIdCanOverrides;
        $user = \Auth::user();
        // return $user;
        //get all context
        $instanceIds = Context::whereHas('roleAssignments', function($q) use($roleIdCanOverrides, $user){
                                    $q->where('userid', $user->id)->whereIn('roleid', $roleIdCanOverrides);
                                })->where('contextlevel', \Constraint::CONTEXT_LEVEL_ACTIVITY)
                                ->pluck('instanceid');
        // return $instanceIds;
        $now = time();
        $manageCurrentActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['start_date', '<=', $now]])
                                    ->whereRaw("(end_date is null or end_date = 0 or end_date >= $now)")
                                    ->get();
        $manageFutureActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['start_date', '>', $now]])
                                    ->get();
        $managePastActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['end_date', '<>', 0], ['end_date', '<', $now]])
                                    ->get();
        // return $manageCurrentActivities;
        //Các hoạt động bạn tham gia
        $configRoleBeAttendances = Config::where('name', 'attendance_role')->first()->value;
        $roleIdBeAttendances = explode(\Constraint::CONFIG_DELIMITER, $configRoleBeAttendances);
        // return $roleIdBeAttendances;

        $instanceIds = Context::whereHas('roleAssignments', function($q) use($roleIdBeAttendances, $user){
            $q->where('userid', $user->id)->whereIn('roleid', $roleIdBeAttendances);
        })->where('contextlevel', \Constraint::CONTEXT_LEVEL_ACTIVITY)
        ->pluck('instanceid');
        // return $instanceIds;

        $joinCurrentActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date', 'absent_without_leave', 'absent_with_leave', 'late')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['start_date', '<=', $now]])
                                    ->whereRaw("(end_date is null or end_date = 0 or end_date >= $now)")
                                    ->join('v_statistics', function($join) use($user){
                                        $join->on('activities.id', '=', 'v_statistics.activityid')
                                                ->where('v_statistics.userid', $user->id);
                                    })->get();
        
        $joinPastActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date', 'absent_without_leave', 'absent_with_leave', 'late')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['end_date', '<>', 0], ['end_date', '<', $now]])
                                    ->join('v_statistics', function($join) use($user){
                                        $join->on('activities.id', '=', 'v_statistics.activityid')
                                                ->where('v_statistics.userid', $user->id);
                                    })->get();
        $joinFutureActivities = Activity::select('id', 'name', 'activitycode' ,'note', 'start_date', 'end_date', 'absent_without_leave', 'absent_with_leave', 'late')
                                    ->whereIn('id', $instanceIds)
                                    ->where([['display', \Constraint::ACTIVITY_DISPLAY_OPEN], ['start_date', '>', $now]])
                                    ->join('v_statistics', function($join) use($user){
                                        $join->on('activities.id', '=', 'v_statistics.activityid')
                                                ->where('v_statistics.userid', $user->id);
                                    })->get();
        // return $joinCurrentActivities;
        return view('home', compact('manageCurrentActivities', 'manageFutureActivities', 'managePastActivities', 'joinCurrentActivities', 'joinFutureActivities', 'joinPastActivities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
