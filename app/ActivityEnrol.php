<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityEnrol extends Model
{
    protected $table = 'activity_enrols';
    protected $fillable = [
        'activityid', 'userid', 'timestart', 'timeend', 'method', 'note'
    ];

    public $timestamps = false;

    public function activity()
    {
        return $this->belongsTo('App\Activity','activityid');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','userid');
    }
}