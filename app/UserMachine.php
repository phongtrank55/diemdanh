<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMachine extends Model
{
    public $table ='user_machines';
    protected $fillable = [
        'userid', 'machineid', 'usercode'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User','userid');
    }

    public function machine()
    {
        return $this->belongsTo('App\Machine','machineid');
    }
}
