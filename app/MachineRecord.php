<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineRecord extends Model
{
    public $table ='machine_records';
    protected $fillable = [
        'machineid', 'usercode', 'timeattendance'
    ];

    public $timestamps = false;

    public function machine()
    {
        return $this->belongsTo('App\Machine','machineid');
    }
}
