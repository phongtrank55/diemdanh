<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'email', 'password', 'status', 'lastname', 'avatar', 
        'gender', 'address', 'idnumber', 'username',  'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     // 'email_verified_at' => 'datetime',
    // ];
    public function getFullname(){
        return $this->lastname . ' '. $this->firstname;
    }

    public function machines()
    {
        return $this->hasMany('App\UserMachine');
    }

    public function activityAttendances()
    {
        return $this->hasMany('App\ActivityAttendance');
    }

    public function activityEnrols()
    {
        return $this->hasMany('App\ActivityEnrol', 'userid');
    }

    public function userMachines(){
        return $this->hasMany('App\UserMachine', 'userid');      
    }
    
    public function roleAssignments()
    {
        return $this->hasMany('App\RoleAssignment', 'userid', 'id');
    }
}
