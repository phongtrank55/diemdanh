<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Context extends Model
{
    protected $table = 'contexts';
    protected $fillable = [
        'contextlevel', 'instanceid', 'path', 'depth'
    ];

    public $timestamps = false;

    public function roleAssignments()
    {
        return $this->hasMany('App\RoleAssignment', 'contextid');
    }
}
