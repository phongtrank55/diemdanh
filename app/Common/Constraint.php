<?php
namespace App\Common;

class Constraint{
    public const ITEMS_PER_PAGE = 10;
    public const MACHINE_STATUS_ACTIVE = 1;
    public const MACHINE_STATUS_INACTIVE = 0;

    public const ROOM_HAS_MACHINE = 1;
    public const ROOM_NO_MACHINE = 0;

    public const USER_STATUS_TYPE_ACTIVE = 1;
    public const USER_STATUS_TYPE_INACTIVE = 0;

    public const PATH_AVATAR = 'public/avatar/';
    public const NO_AVATAR_IMG = 'img/avatar04.png';

    public const SUMMER_SEASON = 1;
    public const WINTER_SEASON = 0;

    public const CALCULATE_BY_SEASON = 1;
    public const NO_CALCULATE_BY_SEASON = 0;

    public const MAX_ROOM_PER_MACHINE = 2;

    public const CONFIG_SECTION_ACTIVITY = "activity";
    public const CONFIG_SECTION_MACHINE = "machine";
    public const CONFIG_SECTION_ROLE ="role";
    public const CONFIG_SECTION_SYSTEM ="system";
    public const CONFIG_SECTION_EMAIL ="email";

    public const ACTIVITY_DISPLAY_OPEN = 1;
    public const ACTIVITY_DISPLAY_CLOSE = 0;

    public const ACTIVITY_FREQUENCY_SOME_DAY = 1;
    public const ACTIVITY_FREQUENCY_EVERY_DAY = 2;
    public const ACTIVITY_FREQUENCY_FREE = 3;
    
    public const MONDAY = 2;
    public const TUESDAY = 3;
    public const WEDNESDAY = 4;
    public const THURSDAY = 5;
    public const FRIDAY = 6;
    public const SATURDAY = 7;
    public const SUNDAY = 8;

    public const HOLIDAY_TYPE_SPECIFIC_DAY = 0;
    public const HOLIDAY_TYPE_SOLAR  = 1; //dương lịch
    public const HOLIDAY_TYPE_LUNAR = 2; //âm lịch
    
    public const ACTIVITY_ENROL_MANUAL = 1;
    public const ACTIVITY_ENROL_SELF = 2;

    public const ACTIVITY_ATTENDANCE_SECTION_ONE = "one" ;
    public const ACTIVITY_ATTENDANCE_SECTION_ALL = "all" ;
    public const ACTIVITY_ATTENDANCE_SECTION_CALENDAR = "calendar" ;

    public const ACTIVITY_CALENDAR_DETAIL_MODIFIED = 1;
    public const ACTIVITY_CALENDAR_DETAIL_INITIAL = 0;

    public const ACTIVITY_CALENDAR_DETAIL_STATUS_ACTIVE = 1;
    public const ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE = 0;
    public const ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY = 2;

    public const ACTIVITY_CALENDAR_DETAIL_COMPLETED = 1;
    public const ACTIVITY_CALENDAR_DETAIL_NO_COMPLTED = 0;


    public const ACTIVITY_ATTENDANCE_DELIMITER_TIME = ';';
    public const CONFIG_DELIMITER = ',';

    public const ACTIVITY_ATTENDANCE_RESULT_PRESENT = 1; //có mặt
    public const ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE = 3; //KP
    public const ACTIVITY_ATTENDANCE_RESULT_LATE = 2; // M
    public const ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITH_LEAVE = 4; //CP

    public const ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE = 1;
    public const ACTIVITY_ATTENDANCE_OVERRIDDEN_FALSE = 0;

    public const CONTEXT_LEVEL_SYSTEM = 10;
    public const CONTEXT_LEVEL_ACTIVITY = 100;
    
}
