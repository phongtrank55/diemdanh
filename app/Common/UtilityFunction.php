<?php
namespace App\Common;

class UtilityFunction{

    public static function convertDateToInt($value, $includeTime = false, $format="d/m/Y H:i:s"){
        if(!$includeTime && $format=="d/m/Y H:i:s"){
            $value .= ' 00:00:00';
        }
        if(empty($value)) return 0;
        $d = \DateTime::createFromFormat($format, $value);
        $timeStamp = ($d && $d->format($format) == $value) ? $d->getTimestamp() : 0;
        return $timeStamp;
    }

    public static function getDayOfWeek($value, $format="d/m/Y"){
        $d = \DateTime::createFromFormat($format, $value);
        return $d->format('N')+1;
    }

    public static function addDay($startDate, $intvalDay ,$format="d/m/Y"){
        $d = \DateTime::createFromFormat($format, $startDate);
        // return $d->add(new \DateInterval('P'.$intvalDay.'D'))->getTimeStamp();
        $interval = ($intvalDay < 0 ? '':'+')."$intvalDay day";
        $d->modify($interval);
        return $d->getTimeStamp();
    }

    public static function addMinute($startDate, $intvalMinute ,$format="d/m/Y H:i:s"){
        $d = \DateTime::createFromFormat($format, $startDate);
        // return $d->add(new \DateInterval('P'.$intvalMinute.'D'))->getTimeStamp();
        $interval = ($intvalMinute < 0 ? '':'+')."$intvalMinute minute";
        $d->modify($interval);
        return $d->getTimeStamp();
    }
}
