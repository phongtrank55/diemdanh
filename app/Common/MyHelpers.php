<?php

function setActive($path)
{
    return Request::is($path . '*') ? 'active' :  '';
}

function getFullname($user){
    return $user->lastname . ' '. $user->firstname;
}