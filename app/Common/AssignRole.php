<?php

namespace App\Common;
use App\Config;
use App\Context;
use App\Role;
use App\RoleAssignment;
use App\Capability;
use App\RoleContextLevel;
// use App\

class AssignRole{

    //Lấy danh sách mầ người này có thể gán
    public static function getRoleCanAssign(Context $context, $user = null){
        if(!$user){
            $user = \Auth::user();
        }
        //lấy tất cả vai trò có thể đc gán ở context level hiện tại
        $roleCanAssigns = Role::select('id', 'name')
            ->whereHas('roleContextLevels', function($q) use ($context){
            $q->where('contextlevel', $context->contextlevel);
        });
        // return $roleCanAssigns;

        if(\CheckPermission::isAdmin($user)){
            return $roleCanAssigns->get();
        }

        //lấy vai trò của nguoi hien tai

        $userRoleIds = RoleAssignment::where([['contextid', $context->id], ['userid', $user->id]])
                                    ->pluck('roleid');
                                    // return $userRoleIds;
        $result = null;
        if($userRoleIds){
            $result = $roleCanAssigns->whereHas('roleBeAssigns', function($q) use($userRoleIds){
                $q->whereIn('roleid', $userRoleIds);
            })->get();
        }
        // return $roleCanAssigns->whereHas('roleContextLevels', function($q) use ($context){
        //     $q->where('contextlevel', $context->contextlevel);
        // });
        


        return $result;
    }

}

?>