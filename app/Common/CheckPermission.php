<?php

namespace App\Common;
use App\Config;
use App\Context;
use App\Role;
use App\Capability;
use App\RoleCapability;
use App\RoleAssignment;

class CheckPermission{

    public static function hasCapability($capabilityCode, Context $context, $user=null){
        if(!$user){
            $user = \Auth::user();
        }

        //check admin
        if(self::isAdmin($user))
            return true;
           //lấy vai trò của nguoi hien tai

        $userRoleIds = RoleAssignment::where([['contextid', $context->id], ['userid', $user->id]])
                                    ->pluck('roleid');
    
        // return $userRoleIds;
                                    
        if($userRoleIds && RoleCapability::where('capabilitycode', $capabilityCode)->whereIn('roleid', $userRoleIds)->count()){
            return true;
        }
        return false;

        // return $user;
    }

    public static function isAdmin($user = null){
        if(!$user){
            $user = \Auth::user();
        }
        $configAdmin = Config::where([['module', 'system'], ['name', 'admin']])->first();
        // return $configAdmin; 
        // return explode(Constraint::CONFIG_DELIMITER, $configAdmin->value);
        if($configAdmin){
           if(in_array($user->id, explode(Constraint::CONFIG_DELIMITER, $configAdmin->value))) 
                return true;
        }
        return false;
    }

    public static function requireCapability($capabilityCode, $context, $user=null){
        // 

        if(!self::hasCapability($capabilityCode, $context, $user))
        return abort(403);
    }
}

?>