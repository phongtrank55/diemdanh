<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name', 'rolecode', 'description'
    ];

    public $timestamps = false;

    public function roleCapabilities()
    {
        return $this->hasMany('App\RoleCapability', 'roleid');
    }

    public function roleContextLevels()
    {
        return $this->hasMany('App\RoleContextLevel', 'roleid');
    }

    public function roleAssigns() // vai trò gán
    {
        return $this->hasMany('App\RoleAllowAssign','roleid');
    }

    public function roleBeAssigns() // vai trò đc gán
    {
        return $this->hasMany('App\RoleAllowAssign','roleidassign');
    }

    public function roleAssignments()
    {
        return $this->hasMany('App\RoleAssignment', 'roleid');
    }
}
