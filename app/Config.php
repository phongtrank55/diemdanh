<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    public $table = 'configs';
    
    protected $fillable = [
        'module', 'name', 'value'
    ];

    public $timestamps = false;

    public static function getConfigs($module = null){
        $configs = [];
        if($module)
            $cfgs = self::where('module', $module)->get();
        else
            $cfgs = self::get();
        foreach($cfgs as $cfg){
            $configs[$cfg->name] = $cfg->value;
        }
        return $configs;
    }
}
