<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    protected $fillable = [
        'model', 'status', 'machinecode', 'note','lastrecord','filename'
    ];

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\UserMachine', 'machineid');
    }

    public function rooms()
    {
        return $this->hasMany('App\RoomMachine', 'machineid');
    }
    public function records()
    {
        return $this->hasMany('App\MachineRecord', 'machineid');
    }
}
