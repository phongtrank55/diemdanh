<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityCalendarDetailShift extends Model
{
    protected $table = 'activity_calendar_detail_shifts';
    protected $fillable = [
        'activitycalendardetailid', 'timestart', 'timeend'
    ];

    public $timestamps = false;

    public function activityCalendarDetail()
    {
        return $this->belongsTo('App\ActivityCalendarDetail','activitycalendardetailid');
    }
        
}