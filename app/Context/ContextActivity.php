<?php

namespace App\Context;

use Constraint;
use App\Context;

class ContextActivity{
    public static function instance($instanceid){
        return Context::where([['instanceid', $instanceid], ['contextlevel',  Constraint::CONTEXT_LEVEL_ACTIVITY]])->first();
    }
}