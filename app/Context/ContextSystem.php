<?php

namespace App\Context;

use Constraint;
use App\Context;

class ContextSystem{
    public static function instance($instanceid = 0){
        return Context::where([['instanceid', $instanceid], ['contextlevel',  Constraint::CONTEXT_LEVEL_SYSTEM]])->first();
    }
}