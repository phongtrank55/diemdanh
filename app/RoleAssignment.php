<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAssignment extends Model
{
    protected $table = 'role_assignments';
    protected $fillable = [
        'roleid', 'contextid', 'userid'
    ];

    public $timestamps = false;

    public function context()
    {
        return $this->belongsTo('App\Context','contextid');
    }

    public function role()
    {
        return $this->belongsTo('App\Role','roleid');
    }
    public function user()
    {
        return $this->belongsTo('App\User','userid');
    }
}

