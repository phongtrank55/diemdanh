<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityAttendance extends Model
{
    protected $table = 'activity_attendances';
    protected $fillable = [
        'activitycalendardetailid', 'userid', 'rawvalue', 'finalvalue',
        'overridden', 'useroverridden', 'note',
        'percent_present_shift', //so ca co mat
        'timein', 'timeout'
    ];

    public $timestamps = false;

    public function activityCalendarDetail()
    {
        return $this->belongsTo('App\ActivityCalendarDetail','activitycalendardetailid');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','userid');
    }
    
}