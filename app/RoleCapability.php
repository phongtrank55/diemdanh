<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleCapability extends Model
{
    protected $table = 'role_capabilities';
    protected $fillable = [
        'roleid', 'capabilitycode'
    ];

    public $timestamps = false;

    public function capability()
    {
        return $this->belongsTo('App\Capability','capabilitycode');
    }

    public function role()
    {
        return $this->belongsTo('App\Role','roleid');
    }
}

