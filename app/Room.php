<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'rooms';
    protected $fillable = [
        'roomcode', 'name', 'note', 'roomgroupid', 'status'
    ];

    public $timestamps = false;

    public function roomGroup()
    {
        return $this->belongsTo('App\RoomGroup','roomgroupid');
    }
  
    public function machines()
    {
        return $this->hasMany('App\RoomMachine');
    }
  
    public function acvtivityCalendars()
    {
        return $this->hasMany('App\ActivityCalendar');
    }
    
    public function acvtivityCalendarDetails()
    {
        return $this->hasMany('App\ActivityCalendarDetail');
    }
}