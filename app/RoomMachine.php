<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomMachine extends Model
{
    public $table ='room_machines';
    protected $fillable = [
        'roomid', 'machineid'
    ];

    public $timestamps = false;

    public function room()
    {
        return $this->belongsTo('App\Room','roomid');
    }
    public function machine()
    {
        return $this->belongsTo('App\Machine','machineid');
    }
}
