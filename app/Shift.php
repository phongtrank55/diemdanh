<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class shift extends Model
{
    protected $table = 'shifts';
    protected $fillable = [
         'name', 'note', 'season', 'datestartsummer', 'datestartwinter'
    ];

    public $timestamps = false;

    public function shiftDetails()
    {
        return $this->hasMany('App\ShiftDetail');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

}
