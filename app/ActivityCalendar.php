<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityCalendar extends Model
{
    protected $table = 'activity_calendars';
    protected $fillable = [
        'start_date_calendar', 'end_date_calendar', 'time_start', 'time_end', 
        'roomid', 'activityid', 'shift_detail_start_id', 'shift_detail_end_id',
        'day_of_week'
    ];

    public $timestamps = false;

    public function activity()
    {
        return $this->belongsTo('App\Activity','activityid');
    }
    
    public function room()
    {
        return $this->belongsTo('App\Room','roomid');
    }
    
    public function shiftDetailStart()
    {
        return $this->belongsTo('App\ShiftDetail','shift_detail_start_id');
    }
    
    public function shiftDetailEnd()
    {
        return $this->belongsTo('App\ShiftDetail','shift_detail_end_id');
    }
    
}