<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $fillable = [
        'activitycode', 'name', 'note', 'activitycategoryid', 'display',
        'frequency', 'shiftid', 'start_date', 'end_date', 
        'enable_in', 'minute_before_in', 'minute_after_in', 'enable_attendance_in_between_shift', 
        'enable_out', 'minute_before_out', 'minute_after_out', 'enable_attendance_out_between_shift', 
        'enable_late', 'minute_late',
        'enable_self_enrol', 'password_self_enrol', 'expired_self_enrol'
    ];

    // public $timestamps = false;

    public function activityCategory()
    {
        return $this->belongsTo('App\ActivityCategory','activitycategoryid');
    }
    
    public function shift()
    {
        return $this->belongsTo('App\Shift','shiftid');
    }

    public function acvtivityCalendars()
    {
        return $this->hasMany('App\ActivityCalendar');
    }

    public function acvtivityCalendarDetails()
    {
        return $this->hasMany('App\ActivityCalendarDetail');
    }

    public function activityEnrols()
    {
        return $this->hasMany('App\ActivityEnrol', 'activityid');
    }
}