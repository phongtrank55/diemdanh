<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomGroup extends Model
{
    protected $table = 'room_groups';
    protected $fillable = [
        'roomgroupcode', 'name', 'note'
    ];

    public $timestamps = false;

    public function rooms()
    {
        return $this->hasMany('App\Machine');
    }
}
