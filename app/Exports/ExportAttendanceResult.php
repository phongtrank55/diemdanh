<?php

namespace App\Exports;

use Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportAttendanceResult implements FromCollection, WithHeadings
{
    use Exportable;

    private $data;
    private $heading;

    public function  __construct(array $heading, array $data){
        $this->data = $data;
        $this->heading = $heading;
    }

    public function collection()
    {
        $result = [];
        foreach ($this->data as $index => $row) {
            $row = (array) $row;
            $item = [];
            // $item[] = $index;
            $item[] = $index + 1;
            foreach($row as $r){
                $item[] = $r;
            }
            $result[] = $item;
        }
        return (collect($result));
    }

    public function headings(): array{
        return $this->heading;
    }
    
}