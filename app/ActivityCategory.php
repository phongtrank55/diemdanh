<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityCategory extends Model
{
    protected $table = 'activity_categories';
    protected $fillable = [
        'activitycategorycode', 'name', 'note', 'depth', 'path', 'parent'
    ];

    public $timestamps = false;

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public static function loadParents($exceptCategoryId = -1){
        $activityCategories = self::select('id', 'name', 'depth', 'path')->orderBy('path', 'ASC')->get();
        $parents = [];
        if($exceptCategoryId != 0)
            $parents = $parents + ['0' => 'Hệ thống'];

        foreach($activityCategories as $category){
            if($category->id == $exceptCategoryId)
                continue;
            $nodes = explode('/', $category->path);
            unset($nodes[0]);
            $parents[$category->id] = '';
            foreach($nodes as $node){ 
                if($node == $exceptCategoryId){
                    unset($parents[$category->id]);
                    break;
                }
                if($node==$category->id)
                    $parents[$category->id] .= $category->name; 
                else{
                    $curCategory = $activityCategories->find($node);
                    $name = $curCategory ? $curCategory->name :'';
                    $parents[$category->id] .= $name .' / '; 
                }                
            }
        }
        return $parents;
    }
    
}
