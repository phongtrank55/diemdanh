<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftDetail extends Model
{
    protected $table = 'shift_details';
    protected $fillable = [
        'shiftid', 'time_summer_start', 'time_summer_end', 'time_winter_start', 
        'time_winter_end', 'name_detail', 'sortorder'
    ];

    public $timestamps = false;

    public function shift()
    {
        return $this->belongsTo('App\Shift','shiftid');
    }

    public function acvtivityCalendars()
    {
        return $this->hasMany('App\ActivityCalendar');
    }

}