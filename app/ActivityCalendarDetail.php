<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityCalendarDetail extends Model
{
    protected $table = 'activity_calendar_details';
    protected $fillable = [
        'activityid', 'roomid', 'fullname', 'shortname', 'time_start', 'time_end', 'date_attendance', 'status',
        'holidayid', 'modified'
    ];

    public $timestamps = false;

    public function activity()
    {
        return $this->belongsTo('App\Activity','activityid');
    }
    
    public function holiday()
    {
        return $this->belongsTo('App\Holiday','holidayid');
    }
    
    public function room()
    {
        return $this->belongsTo('App\Room','roomid');
    }
    
    public function activityAttendances()
    {
        return $this->hasMany('App\ActivityAttendance');
    }   
    
    public function activityCalendarDetailShifts()
    {
        return $this->hasMany('App\ActivityCalendarDetailShift', 'activitycalendardetailid');
    }

}