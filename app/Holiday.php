<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    public $table = 'holidays';
    
    protected $fillable = [
        'fullname', 'shortname', 'startdate', 'dayeveryyear', 'montheveryyear' ,'enddate', 'type', 'note'
    ];

    public $timestamps = false;
    
    public function activityCalendarDetails()
    {
        return $this->hasMany('App\ActivityCalendarDetail');
    }   
}
