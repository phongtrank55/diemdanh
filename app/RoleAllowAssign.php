<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleAllowAssign extends Model
{
    protected $table = 'role_allow_assigns';
    protected $fillable = [
        'roleid', 'roleidassign'
    ];

    public $timestamps = false;

    public function capability()
    {
        return $this->belongsTo('App\Capability','capabilitycode');
    }

    public function role()
    {
        return $this->belongsTo('App\Role','roleid');
    }
    public function roleAssign()
    {
        return $this->belongsTo('App\Role','roleidassign');
    }
}

