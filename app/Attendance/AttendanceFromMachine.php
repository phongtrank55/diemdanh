<?php

namespace App\Attendance;

use App\Activity;
use App\Machine;
use App\User;
use App\Room;
use App\Holiday;
use App\MachineRecord;
use App\ActivityCalendar;
use App\Config;
use App\ActivityCalendarDetail;
use App\UserMachine;
use App\ActivityCalendarDetailShift;
use App\ActivityEnrol;
use App\ActivityAttendance;
use Illuminate\Support\Facades\DB;

use Constraint;
use App\Common\UtilityFunction;

class AttendanceFromMachine{

    // Lấy máy và phòng
    // Active = -1 : get all
    // Active = 0 : inactive
    // Active = 1; Active
    public function getRoomsAndMachines($activityId, $active = -1){
        $rooms = Room::select('id', 'name', 'status')
            ->whereExists(function ($query) use($activityId) {
                $query->select(DB::raw(1))
                    ->from('activity_calendar_details')
                    ->where('activityid', $activityId)
                    ->whereRaw('activity_calendar_details.roomid = rooms.id');
        });
    
        if($active == 1){
            $rooms = $rooms->where('status', Constraint::ROOM_HAS_MACHINE)->get();
        }else if($active == 0){
            $rooms = $rooms->where('status', Constraint::ROOM_NO_MACHINE)->get();
        }else{
            $rooms = $rooms->get();
        }
        $roomActiveIds = [];
        foreach($rooms as $room)
            if($room->status == Constraint::ROOM_HAS_MACHINE)
                $roomActiveIds[] = $room->id;
        if($roomActiveIds){
            $machines = Machine::select('id', 'machinecode', 'status', 'filename')
                    ->whereExists(function ($query) use($roomActiveIds) {
                        $query->select(DB::raw(1))
                            ->from('room_machines')
                            ->whereIn('roomid', $roomActiveIds)
                            ->whereRaw('room_machines.machineid = machines.id');
                    });
            if($active == 1){
                $machines = $machines->where('status', Constraint::MACHINE_STATUS_ACTIVE)->get();
            }else if($active == 0){
                $machines = $machines->where('status', Constraint::MACHINE_STATUS_INACTIVE)->get();
            }else{
                $machines = $machines->get();
            }
        }else{
            $machines = [];
        }
        // return compact('rooms', 'machines');
        return [$rooms, $machines];
    }
    //GIA TRI TRA VE CAC TRANG THAI
    public const ATTENDANCE_STATUS_READY = 1;
    public const ATTENDANCE_STATUS_INACTIVE = 0;
    public const ATTENDANCE_STATUS_HOLIDAY = -1;
    public const ATTENDANCE_STATUS_NOT_FINISH = -2;
    public const ATTENDANCE_STATUS_NOT_START = -3;
    public const ATTENDANCE_STATUS_NOT_ENABLE = -4;
    public const ATTENDANCE_STATUS_NOT_MACHINE = -5;
    
    //Kiem tra san sang diem danh hom nay chua
    public function checkReadyAttenance(ActivityCalendarDetail $activityCalendarDetail){
        //Xet ngay nghỉ
        if($activityCalendarDetail->status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_INACTIVE)
            return self::ATTENDANCE_STATUS_INACTIVE;
        if($activityCalendarDetail->status == Constraint::ACTIVITY_CALENDAR_DETAIL_STATUS_HOLIDAY)
            return self::ATTENDANCE_STATUS_HOLIDAY;
        
        //Xét bật kích hoạt điểm danh tự động
        if(Activity::where([['id', $activityCalendarDetail->activityid],
            ['enable_in', 0], ['enable_out', 0], ['enable_late', 0]])->count() > 0)
            return self::ATTENDANCE_STATUS_NOT_ENABLE;

         // Kiem tra may cua buoi nay
        if(Machine::where('status', Constraint::MACHINE_STATUS_ACTIVE)
            ->whereExists(function ($query) use($activityCalendarDetail) {
                $query->select(DB::raw(1))
                    ->from('room_machines')
                    ->where('roomid', $activityCalendarDetail->roomid)
                    ->whereRaw('room_machines.machineid = machines.id');
                })->count() == 0)
            
            return self::ATTENDANCE_STATUS_NOT_MACHINE;
       

        // check hoạt động chưa kết thúc
        $now = time();
        $date = date('d/m/Y', $activityCalendarDetail->date_attendance);
        $timeStart = UtilityFunction::convertDateToInt($date.' '.$activityCalendarDetail->time_start, true, 'd/m/Y H:i');
         // return(date('d/m/Y H:i', $timeEnd));
        // return date('d/m/Y H:i:s', $timeStart).' ==> '.date('d/m/Y H:i:s', $now);
        if($timeStart > $now)
            return self::ATTENDANCE_STATUS_NOT_START;
      
        $timeEnd = UtilityFunction::convertDateToInt($date . ' ' . $activityCalendarDetail->time_end, true, 'd/m/Y H:i');
       
        if($timeEnd > $now)
            return self::ATTENDANCE_STATUS_NOT_FINISH;
        
        return self::ATTENDANCE_STATUS_READY;
        
    }

    public const ATTENDANCE_BY_USER_OK = 1;
    public const ATTENDANCE_BY_USER_NO_MACHINE = -1;
    public const ATTENDANCE_BY_USER_NO_ARRANGE = -2; //KHONG CO KHOANG XET
    public const ATTENDANCE_BY_USER_NO_DATA_IDENTY = -3; //KHONG CO DU LIEU NHAN DANG
    public const ATTENDANCE_BY_USER_NO_ACTIVITY = -4; //KHONG CO hoat dong
    //diem danh theo nguoi va buoi
    public function attendanceByUser(ActivityCalendarDetail $activityCalendarDetail, $userid, Activity $activity = null, 
                                    $arrangeAttendance = null, $machines = null){
        // return $arrangeAttendance;
        
        if(!$activity){
            $activity = Activity::find($activityCalendarDetail->activityId);
            if(!$activity) return self::ATTENDANCE_BY_USER_NO_ACTIVITY;
        }
        
        //lay thong tin may
        if(!$machines){
            $machines = $this->getMachinesByCalendarDetail($activityCalendarDetail);
            if(!$machines) return self::ATTENDANCE_BY_USER_NO_MACHINE;
        }
        // return $machines;
        
        foreach($machines as $machine)
            $machineIds[] = $machine->id; 
        // return $machineIds;

        //check nguoi nay khong co diu lieu tren cac may dang hoat dong
        if(UserMachine::where('userid', $userid)
                        ->whereIn('machineid', $machineIds)->count() == 0){
                            //nếu đã có bản ghi đè rồi thì ok
            if(ActivityAttendance::where([['userid', $userid],
                            ['activitycalendardetailid', $activityCalendarDetail->id],
                            ['overridden', Constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_TRUE]])
                            ->count() == 0)
                return self::ATTENDANCE_BY_USER_NO_DATA_IDENTY;
        }
        
        //neu xet giua ca
        $attendanceBetweenShift = $activity->enable_attendance_in_between_shift 
                                                || $activity->enable_attendance_out_between_shift;
        
        if(!$arrangeAttendance){
            $arrangeAttendance = $this->getArrangeTimeAttendance($activityCalendarDetail, $activity);
            if(!$arrangeAttendance) return self::ATTENDANCE_BY_USER_NO_ARRANGE;
        }  
        $timeAttendanceInArrange = $arrangeAttendance->in;
        $timeAttendanceOutArrange = $arrangeAttendance->out;
        $timeAttendanceLateArrange = $arrangeAttendance->late;
        // return [$timeAttendanceInArrange, $timeAttendanceOutArrange, $timeAttendanceLateArrange];
        // print_r($timeAttendanceInArrange); die;
        //Danh gia ket qua
        
        $rawValueArrange = [];
        $numberArrange = count($timeAttendanceInArrange);
        $timeInOfUser = [];
        $timeOutOfUser = [];
        $numPresentArrange = 0; // ty le tiet co mat (ca muon)
        for($i=0; $i < $numberArrange; $i++){
            //Xet trong khoang hien tai
            $currentRawValue = null;
            //Xet diem danh vao
            if($activity->enable_in){
                
                $timeBefore = $timeAttendanceInArrange[$i]->start;
                $timeAfter = $timeAttendanceInArrange[$i]->end; 
                // return date('d/m/Y H:i:s', $timeAfter);
                
                $records = DB::table('v_raw_records')->where('userid', $userid)
                            ->whereIn('machineid', $machineIds)
                            ->whereBetween('timeattendance', [$timeBefore, $timeAfter])->first();
                // return $records;
                if($records){
                    $timeInOfUser[] = date('H:i:s', $records->timeattendance);
                    // return $timeInOfUser; 
                    $currentRawValue = constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT; // co mat
                }else{
                    $currentRawValue = Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE; // vang
                }
            }
            //Xet den muon
            if($currentRawValue != constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT && $activity->enable_late){
                
                $timeBefore = $timeAttendanceLateArrange[$i]->start;
                $timeAfter = $timeAttendanceLateArrange[$i]->end; 
                // return date('d/m/Y H:i:s', $timeAfter);
                
                $records = DB::table('v_raw_records')->where('userid', $userid)
                            ->whereIn('machineid', $machineIds)
                            ->whereBetween('timeattendance', [$timeBefore, $timeAfter])->first();
                // return $records;
                if($records){
                    $timeInOfUser[] = date('H:i:s', $records->timeattendance);
                    // return $timeInOfUser; 
                    $currentRawValue = constraint::ACTIVITY_ATTENDANCE_RESULT_LATE; // muon
                }else{
                    $currentRawValue = Constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE; // vang
                }
            }   
            //Xet ra ve
            if($activity->enable_out){
                
                $timeBefore = $timeAttendanceOutArrange[$i]->start;
                $timeAfter = $timeAttendanceOutArrange[$i]->end; 
                // return date('d/m/Y H:i:s', $timeAfter);
                
                $records = DB::table('v_raw_records')->where('userid', $userid)
                            ->whereIn('machineid', $machineIds)
                            ->whereBetween('timeattendance', [$timeBefore, $timeAfter])->first();
                // return $records;
                if($records){
                    $timeOutOfUser[] = date('H:i:s', $records->timeattendance);
                    // return $timeInOfUser; 
                    //neu khong xet muon hoac xet dienm danh vao
                    if(is_null($currentRawValue))
                        $currentRawValue = constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT; // co mat
                }else{
                    $currentRawValue = constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE; // VANG
                }
            }
            //neu khong xet ra ve hay vao hay muon => CM
            if(is_null($currentRawValue))
                $currentRawValue = constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT; // co mat
            //tinh % co mat
            if($currentRawValue != constraint::ACTIVITY_ATTENDANCE_RESULT_ABSENT_WITHOUT_LEAVE)
                $numPresentArrange++;
            $rawValueArrange[] = $currentRawValue;
        }
        //Xet toan bo
        // neu vang 1 khoang => vang
        //neu muon 1 khoang => muon
        // neu dung gio ca => co mat
        // return $rawValueArrange;
        $rawValue = Constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT;
        foreach($rawValueArrange as $value){
            if($value != Constraint::ACTIVITY_ATTENDANCE_RESULT_PRESENT){
                $rawValue = $value;
                break;
            }
        }
        //UPDATE
        ActivityAttendance::updateOrCreate(
            ['userid' => $userid,
            'activitycalendardetailid' => $activityCalendarDetail->id],
            ['rawvalue' => $rawValue,
            'timein' => implode(Constraint::ACTIVITY_ATTENDANCE_DELIMITER_TIME, $timeInOfUser),
            'timeout' => implode(Constraint::ACTIVITY_ATTENDANCE_DELIMITER_TIME, $timeOutOfUser),
            'percent_present_shift' => (round($numPresentArrange / $numberArrange, 2)*100)]
        );
        // echo 'acx'; die;
        return self::ATTENDANCE_BY_USER_OK;
    }

    //get TimeArrange
    private function getArrangeTimeAttendance(ActivityCalendarDetail $activityCalendarDetail, Activity $activity=null){
        //bat dau diem danh
        [$timeStart, $timeEnd] = $this->getOnTime($activityCalendarDetail, $activity);
        $dateAttendance = date('d/m/Y', $activityCalendarDetail->date_attendance);

        if(!$dateAttendance || !$timeStart || !$timeEnd)
            return 0;

        if(!is_array($timeStart)){
            $timeStart = [$timeStart];
            $timeEnd = [$timeEnd];
        }

        //so khoang xet
        $numberArrange = count($timeStart);
        for($i = 0; $i < $numberArrange; $i++){
        //xEST KHOARNG DDUNGS GIO
            $timeAttendanceInArrange[] = (object)[
                'start' => UtilityFunction::addMinute($dateAttendance.' '.$timeStart[$i].':00', -$activity->minute_before_in, 'd/m/Y H:i:s'),
                'end' => UtilityFunction::addMinute($dateAttendance.' '.$timeStart[$i].':00', $activity->minute_after_in, 'd/m/Y H:i:s'),
            ];
            // return [date('d/m/Y H:i:s', $timeAttendanceInArrange[0]['start']), date('d/m/Y H:i:s', $timeAttendanceInArrange[0]['end'])];
    
            $timeAttendanceOutArrange[] = (object)[
                'start' => UtilityFunction::addMinute($dateAttendance.' '.$timeEnd[$i].':00', -$activity->minute_before_out, 'd/m/Y H:i:s'),
                'end' => UtilityFunction::addMinute($dateAttendance.' '.$timeEnd[$i].':00', $activity->minute_after_out, 'd/m/Y H:i:s'),
            ];
            // return [date('d/m/Y H:i:s', $timeAttendanceOutArrange[0]['start']), date('d/m/Y H:i:s', $timeAttendanceOutArrange[0]['end'])];

            $timeAttendanceLateArrange[] = (object)[
                'start' => UtilityFunction::addMinute($dateAttendance.' '.$timeStart[$i].':01', $activity->minute_after_in, 'd/m/Y H:i:s'),
                'end' => UtilityFunction::addMinute($dateAttendance.' '.$timeStart[$i].':00', $activity->minute_late, 'd/m/Y H:i:s'),
            ];
            // return [date('d/m/Y H:i:s', $timeAttendanceLateArrange[0]['start']), date('d/m/Y H:i:s', $timeAttendanceLateArrange[0]['end'])];
        }
        return (object) ['in'=>$timeAttendanceInArrange, 'out' => $timeAttendanceOutArrange, 'late'=> $timeAttendanceLateArrange];
    }
    //lay thoi gian dung gio
    public function getOnTime(ActivityCalendarDetail $activityCalendarDetail, Activity $activity = null){
        
        if(!$activity){
            $activity = Activity::find($activityCalendarDetail->activityid);
            if(!$activity) return [0, 0];
        }
        //neu xet giua ca
        $attendanceBetweenShift = $activity->enable_attendance_in_between_shift 
                                                || $activity->enable_attendance_out_between_shift;
        if($attendanceBetweenShift){
            $activityCalendarDetailShift = ActivityCalendarDetailShift::where('activitycalendardetailid', $activityCalendarDetail->id)
                                                ->orderBy('timestart', 'ASC')->get();
            if(!$activityCalendarDetailShift)
                return [0, 0];
            foreach($activityCalendarDetailShift as $detailShift){
                $timeStart[] = $detailShift->timestart;
                $timeEnd[] = $detailShift->timeend;
            } 
                
        }else{
            $timeStart = $activityCalendarDetail->time_start;
            $timeEnd = $activityCalendarDetail->time_end;
        }
        
        return [$timeStart, $timeEnd];

    }

    //lay may theo buoi
    private function getMachinesByCalendarDetail(activityCalendarDetail $activityCalendarDetail){
        $machines = Machine::where('status', Constraint::MACHINE_STATUS_ACTIVE)
            ->whereExists(function ($query) use($activityCalendarDetail) {
                $query->select(DB::raw(1))
                    ->from('room_machines')
                    ->where('roomid', $activityCalendarDetail->roomid)
                    ->whereRaw('room_machines.machineid = machines.id');
                })->get();
        return $machines;
    }
    //diem danh theo buoi
    public function attendance(ActivityCalendarDetail $activityCalendarDetail, $force = false){
        if($force){
            //xoa cac ban ghi chua bi de
            ActivityAttendance::where([['activitycalendardetailid', $activityCalendarDetail->id],
                                        ['overridden', constraint::ACTIVITY_ATTENDANCE_OVERRIDDEN_FALSE]])
                                ->delete();
        }
        
        if($activityCalendarDetail->completed == Constraint::ACTIVITY_CALENDAR_DETAIL_COMPLETED)
            return;
        if($this->checkReadyAttenance($activityCalendarDetail) != self::ATTENDANCE_STATUS_READY)
            return;
        //readFile
        // return 'sss';
        $machines = $this->getMachinesByCalendarDetail($activityCalendarDetail);
        // return $machines;
        $completed = true;
        foreach($machines as $machine){
            //nếu loi doc file thi khong the xet hoan thanh
            if(!$this->readFile($machine))
                $completed = false;
            
        }
        // return 'ok';
        

        $activity = Activity::find($activityCalendarDetail->activityid);
        if(!$activity) return 0;
        // Xét khoảng giữa các ca 
        $arrangeAttendance  = $this->getArrangeTimeAttendance($activityCalendarDetail, $activity);
        if(!$arrangeAttendance) return 0;

        // return $this->getOnTime($activityCalendarDetail, $activity);
        //
         //find all role
        $configAttendanceRole = Config::where('name', 'attendance_role')->first()->value;
        $configAttendanceRole = str_replace(Constraint::CONFIG_DELIMITER, ',', $configAttendanceRole);
        $users = ActivityEnrol::select('userid')
                        ->where([['activityid', $activity->id], 
                                        ['timestart', '<=', $activityCalendarDetail->date_attendance]])
                        ->whereRaw('(timeend is null or timeend >= '.$activityCalendarDetail->date_attendance.')')
                        ->whereRaw("EXISTS (SELECT * FROM role_assignments
                                        WHERE userid = activity_enrols.userid 
                                        and contextid = ( SELECT id from contexts where 
                                        instanceid = ". $activity->id.")
                                        and roleid in ($configAttendanceRole)) ")                
                        ->get();
        // return $users;
        foreach($users as $user){
            $result = $this->attendanceByUser($activityCalendarDetail, $user->userid, $activity, $arrangeAttendance, $machines);
            if($result != self::ATTENDANCE_BY_USER_OK)
                $completed = false;
        }

        $activityCalendarDetail->completed = $completed;
        $activityCalendarDetail->save();
        // return 'ok';
        // return date('d/m/Y H:i:s', $time);
        // return [$timeStart, $timeEnd];
    }

    //rEADFILE
    public function readFile(Machine $machine){
        $pathFile = config('app.machine_dir'). '/'.$machine->filename;
        // return $pathFile;
        if(!file_exists($pathFile)) return 0;
        $file = fopen($pathFile , "r+");// or die('error read file');
        if(!$file) return 0;
        // return LOCK_EX.' axc';
        // $lines = file($pathFile);
        // return $lines;       
        // lock de khong cho tien trinh khac doc
        if(flock($file, LOCK_SH)){
            //file lastest date for update of machine
            $lastTime = $machine->lastrecord ? $machine->lastrecord : 0;
            // return $lastTime;
            $lines = file($pathFile);
            // return $lines;
            // return count($lines);
            $c=0;
            foreach ($lines as $line) {
                $line = trim($line);
                // return $line;
                if(empty($line)) continue;
                $data = explode(" ", $line);
                
                if(count($data)!=3)
                    continue;
                $timeAttendance = UtilityFunction::convertDateToInt("$data[1] $data[2]", true, 'd/m/Y H:i:s');
                if(!$timeAttendance){
                    // return "$data[1] $data[2]";
                    continue;
                }
                // $c++;
                if($timeAttendance >= $lastTime) 
                {
                    $lastTime = $timeAttendance;
                    MachineRecord::updateOrCreate([
                        'timeattendance' => $timeAttendance,
                        'machineid' => $machine->id,
                        'usercode' => $data[0]
                    ]);
                }
                
            }
            $machine->lastrecord = $lastTime;
            $machine->save();
            flock($file, LOCK_UN);
        }
        fclose($file);
        // unlink($pathFile);
        return 1;
        // return $c;
    }
    
}