<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();


// Route::middleware('auth')->group(function(){
    Route::get('logout', 'Auth\LoginController@logout');
Route::get('/', 'HomeController@index')->name('home.index');

Route::prefix('self-enrol')->middleware('auth')->name('self-enrol.')->group(function () {
    Route::get('/', 'SelfEnrolController@index')->name('index');
    Route::post('enrol', 'SelfEnrolController@enrol')->name('enrol');
});
// Route::namespace('Admin')->middleware('auth')->group(function(){
//     Route::prefix('activity')->name('activity.')->group(function () {
//         Route::get('attendance/{id}/{section}', 'ActivityController@attendance')->name('attendance');
//         Route::get('attendance/{id?}/result-by-detail/{activityCalendarDetailId}', 'ActivityController@resultAttendanceByDetail')->name('result-attendance-by-detail');
//         Route::post('attendance/overridden/{activityCalendarDetailId}', 'ActivityController@overridenAttendance')->name('attendance-overridden');
//         Route::post('attendance/{id}/calendar', 'ActivityController@updateCalendar')->name('updatecalendar');
//         Route::post('attendance/{id}/export', 'ActivityController@exportResultAttendance')->name('attendance.export');        
//         Route::get('participant/{id}', 'ActivityController@participant')->name('participant');
//         Route::get('participant/potential-users/{id}', 'ActivityController@getPotentialUser')->name('get-potential-user');
//         Route::post('participant/enrol-users/{method?}', 'ActivityController@enrolUser')->name('enrol-user');
//         Route::post('participant/unenrol/{activityid}', 'ActivityController@unenrolUser')->name('unenrol-user');       
//         Route::get('statistic/{id}', 'ActivityController@statistic')->name('statistic');
//     });
// });
Route::namespace('Admin')->middleware('auth')->prefix('admin')->name('admin.')->group(function () {
    Route::prefix('machine')->name('machine.')->group(function () {
        Route::get('/', 'MachineController@index')->name('index');
        Route::get('index', 'MachineController@index')->name('index');
        Route::get('create', 'MachineController@create')->name('create');
        Route::post('store', 'MachineController@store')->name('store');
        Route::get('edit/{id}', 'MachineController@edit')->name('edit');
        Route::post('update', 'MachineController@update')->name('update');
        Route::get('update-identity-user/{id}', 'MachineController@showIdentityUser')->name('update-identity-user');
        Route::get('update-identity-user/{id}/get-potential-user', 'MachineController@getPotentialUser')->name('get-potential-user');
        Route::post('update-identity-user/{id}/store-identity-user', 'MachineController@storeIdentityUser')->name('store-identity-user');
        Route::post('delete-identity-user', 'MachineController@deleteIdentityUser')->name('delete-identity-user');
        Route::post('delete', 'MachineController@destroy')->name('delete');
    });

    Route::prefix('room')->name('room.')->group(function () {
        Route::get('/', 'RoomController@index')->name('index');
        Route::get('index', 'RoomController@index')->name('index');
        Route::get('create', 'RoomController@create')->name('create');
        Route::post('store', 'RoomController@store')->name('store');
        Route::get('edit/{id}', 'RoomController@edit')->name('edit');
        Route::get('assign-machine/{id}', 'RoomController@assignMachine')->name('assignmachine');
        Route::post('store-assign-machine', 'RoomController@storeAssignMachine')->name('storeassign');
        Route::post('update', 'RoomController@update')->name('update');
        Route::post('delete', 'RoomController@destroy')->name('delete');
    });
    
    Route::prefix('roomgroup')->name('roomgroup.')->group(function () {
        Route::get('create', 'RoomGroupController@create')->name('create');
        Route::post('store', 'RoomGroupController@store')->name('store');
        Route::get('edit/{id}', 'RoomGroupController@edit')->name('edit');
        Route::post('update', 'RoomGroupController@update')->name('update');
        Route::post('delete', 'RoomGroupController@destroy')->name('delete');
    });
    Route::prefix('holiday')->name('holiday.')->group(function () {
        Route::get('/', 'HolidayController@index')->name('index');
        Route::get('index', 'HolidayController@index')->name('index');
        Route::get('create', 'HolidayController@create')->name('create');
        Route::post('store', 'HolidayController@store')->name('store');
        Route::get('edit/{id}', 'HolidayController@edit')->name('edit');
        Route::post('update', 'HolidayController@update')->name('update');
        Route::post('delete', 'HolidayController@destroy')->name('delete');
    });


     Route::prefix('activitycategory')->name('activitycategory.')->group(function () {
        Route::get('create', 'ActivityCategoryController@create')->name('create');
        Route::post('store', 'ActivityCategoryController@store')->name('store');
        Route::get('edit/{id}', 'ActivityCategoryController@edit')->name('edit');
        Route::post('update', 'ActivityCategoryController@update')->name('update');
        Route::post('delete', 'ActivityCategoryController@destroy')->name('delete');
    });

    Route::prefix('activity')->name('activity.')->group(function () {
        Route::get('/', 'ActivityController@index')->name('index');
        Route::get('index', 'ActivityController@index')->name('index');
        Route::get('create', 'ActivityController@create')->name('create');
        Route::post('store', 'ActivityController@store')->name('store');
        Route::get('edit/{id}', 'ActivityController@edit')->name('edit');
        Route::post('update/{id}', 'ActivityController@update')->name('update');
        Route::get('subview-table-frequency/{frequency}/{shift?}', 'ActivityController@subViewTableFrequency')->name('subviewtablefrequency');
        Route::get('attendance/{id}/{section}', 'ActivityController@attendance')->name('attendance');
        Route::get('attendance/{id?}/result-by-detail/{activityCalendarDetailId}', 'ActivityController@resultAttendanceByDetail')->name('result-attendance-by-detail');
        Route::post('attendance/overridden/{activityCalendarDetailId}', 'ActivityController@overridenAttendance')->name('attendance-overridden');
        Route::post('attendance/{id}/calendar', 'ActivityController@updateCalendar')->name('updatecalendar');
        Route::post('attendance/{id}/export', 'ActivityController@exportResultAttendance')->name('attendance.export');        
        Route::get('participant/{id}', 'ActivityController@participant')->name('participant');
        Route::get('participant/potential-users/{id}', 'ActivityController@getPotentialUser')->name('get-potential-user');
        Route::post('participant/enrol-users/{method?}', 'ActivityController@enrolUser')->name('enrol-user');
        Route::post('participant/unenrol/{activityid}', 'ActivityController@unenrolUser')->name('unenrol-user');
       
        Route::get('statistic/{id}', 'ActivityController@statistic')->name('statistic');
        Route::post('delete', 'ActivityController@destroy')->name('delete');
    });
    Route::prefix('user')->name('user.')->group(function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::get('index', 'UserController@index')->name('index');
        Route::get('create', 'UserController@create')->name('create');
        Route::post('store', 'UserController@store')->name('store');
        Route::get('edit/{id}', 'UserController@edit')->name('edit');
        Route::post('update', 'UserController@update')->name('update');
        Route::post('delete', 'UserController@destroy')->name('delete');
    });
    
    Route::prefix('cohort')->name('cohort.')->group(function () {
        Route::get('/', 'CohortController@index')->name('index');
        Route::get('index', 'CohortController@index')->name('index');
        Route::get('create', 'CohortController@create')->name('create');
        Route::post('store', 'CohortController@store')->name('store');
        Route::get('edit/{id}', 'CohortController@edit')->name('edit');
        Route::get('assign-user/{id}', 'CohortController@assignMachine')->name('assignuser');
        Route::post('store-assign-user', 'CohortController@storeAssignMachine')->name('storeassign');
        Route::post('update', 'CohortController@update')->name('update');
        Route::post('delete', 'CohortController@destroy')->name('delete');
    });

    Route::prefix('shift')->name('shift.')->group(function () {
        Route::get('/', 'ShiftController@index')->name('index');
        Route::get('index/{id?}', 'ShiftController@index')->name('index');
        Route::get('create', 'ShiftController@create')->name('create');
        Route::post('store', 'ShiftController@store')->name('store');
        Route::get('edit/{id}', 'ShiftController@edit')->name('edit');
        Route::post('update', 'ShiftController@update')->name('update');
        Route::post('delete', 'ShiftController@destroy')->name('delete');
    });

    Route::prefix('config')->name('config.')->group(function () {
        Route::get('/{section?}', 'ConfigController@index')->name('index');
        Route::post('update/{section?}', 'ConfigController@update')->name('update');

        Route::get('role/create', 'RoleController@create')->name('create-role');
        Route::post('role/store', 'RoleController@store')->name('store-role');
        Route::get('role/edit/{id}', 'RoleController@edit')->name('edit-role');
        Route::post('role/update/{id}', 'RoleController@update')->name('update-role');
        Route::post('role/delete', 'RoleController@destroy')->name('delete-role');
    });
});